# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [11.0.9](https://gitlab.com/cublueprint/beneficent/server/compare/v11.0.8...v11.0.9) (2022-03-19)

### [11.0.8](https://gitlab.com/cublueprint/beneficent/server/compare/v11.0.7...v11.0.8) (2022-03-18)

### [11.0.7](https://gitlab.com/cublueprint/beneficent/server/compare/v11.0.6...v11.0.7) (2022-03-17)

### [11.0.6](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/server/compare/v11.0.5...v11.0.6) (2022-03-16)

### [11.0.5](https://git-us-east1-d.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/server/compare/v11.0.4...v11.0.5) (2022-03-15)


### Bug Fixes

* set password email template (EN-572) ([43c7330](https://git-us-east1-d.ci-gateway.int.gprd.gitlab.net:8989/cublueprint/beneficent/server/commit/43c7330b3642cbe85923e1c2755fdec9efcef354))

### [11.0.4](https://gitlab.com/cublueprint/beneficent/server/compare/v11.0.3...v11.0.4) (2022-03-15)

### [11.0.3](https://gitlab.com/cublueprint/beneficent/server/compare/v11.0.2...v11.0.3) (2022-03-14)

### [11.0.2](https://gitlab.com/cublueprint/beneficent/server/compare/v11.0.1...v11.0.2) (2022-03-12)


### Bug Fixes

* freeze user account on email change (EN-559) ([2597e5b](https://gitlab.com/cublueprint/beneficent/server/commit/2597e5bde08d8b349da1c1043aa18c487eec6f44))
* phone validation (EN-565) ([a01db7d](https://gitlab.com/cublueprint/beneficent/server/commit/a01db7d0149f7b5ef527fbe899a99e5dcea14de3))
* Update user name and email querying (EN-558) ([de518fc](https://gitlab.com/cublueprint/beneficent/server/commit/de518fc79287d303563458c3050d1803c036fad0))

### [11.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v11.0.0...v11.0.1) (2022-03-11)

## [11.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.8...v11.0.0) (2022-03-10)


### ⚠ BREAKING CHANGES

* email verification and other bugfixes (EN-556) (EN-557)

### Bug Fixes

* email verification and other bugfixes (EN-556) (EN-557) ([c370522](https://gitlab.com/cublueprint/beneficent/server/commit/c370522d051d0fc36a5a5164c16c006577227313))

### [10.1.8](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.7...v10.1.8) (2022-03-09)

### [10.1.7](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.6...v10.1.7) (2022-03-08)

### [10.1.6](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.5...v10.1.6) (2022-03-07)


### Bug Fixes

* account manager bugs (EN-550) ([17a68ea](https://gitlab.com/cublueprint/beneficent/server/commit/17a68ea03dc9c579a81ddc6e7e246ed6b0a8ba1b))

### [10.1.5](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.4...v10.1.5) (2022-03-07)

### [10.1.4](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.3...v10.1.4) (2022-03-03)

### [10.1.3](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.2...v10.1.3) (2022-03-02)

### [10.1.2](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.1...v10.1.2) (2022-03-01)

### [10.1.1](https://gitlab.com/cublueprint/beneficent/server/compare/v10.1.0...v10.1.1) (2022-02-28)

## [10.1.0](https://gitlab.com/cublueprint/beneficent/server/compare/v10.0.2...v10.1.0) (2022-02-27)


### Features

* account manager update and delete (EN-535, EN-536) ([1146e2a](https://gitlab.com/cublueprint/beneficent/server/commit/1146e2aae2ee28b21e44c8ff7878a3657cb03cfe))

### [10.0.2](https://gitlab.com/cublueprint/beneficent/server/compare/v10.0.1...v10.0.2) (2022-02-26)

### [10.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v10.0.0...v10.0.1) (2022-02-25)

## [10.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.43...v10.0.0) (2022-02-24)


### ⚠ BREAKING CHANGES

* new user roles and account verification (EN-531) (EN-532) (EN-533)

### Bug Fixes

* new user roles and account verification (EN-531) (EN-532) (EN-533) ([380b7f7](https://gitlab.com/cublueprint/beneficent/server/commit/380b7f7f41dbadd8d0f508c8aba2d2d3c958128b))

### [9.0.43](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.42...v9.0.43) (2022-02-23)

### [9.0.42](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.41...v9.0.42) (2022-02-22)

### [9.0.41](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.40...v9.0.41) (2022-02-20)

### [9.0.40](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.39...v9.0.40) (2022-02-19)

### [9.0.39](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.38...v9.0.39) (2022-02-18)


### Bug Fixes

* fix expected payment calculation on backend (EN-545) ([98eec1f](https://gitlab.com/cublueprint/beneficent/server/commit/98eec1fd5a3bf7658a90be531df0fc1eb7152860))

### [9.0.38](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.37...v9.0.38) (2022-02-17)

### [9.0.37](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.36...v9.0.37) (2022-02-16)

### [9.0.36](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.35...v9.0.36) (2022-02-15)

### [9.0.35](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.34...v9.0.35) (2022-02-14)

### [9.0.34](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.33...v9.0.34) (2022-02-13)

### [9.0.33](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.32...v9.0.33) (2022-02-12)


### Bug Fixes

* wave payments and calculations (EN-489, EN-462) ([12f801f](https://gitlab.com/cublueprint/beneficent/server/commit/12f801f52c6b0a480032c6ae6f216ba18d1956e3))

### [9.0.32](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.31...v9.0.32) (2022-02-11)

### [9.0.31](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.30...v9.0.31) (2022-02-10)

### [9.0.30](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.29...v9.0.30) (2022-02-09)

### [9.0.29](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.28...v9.0.29) (2022-02-08)

### [9.0.28](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.27...v9.0.28) (2022-02-07)

### [9.0.27](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.26...v9.0.27) (2022-02-06)

### [9.0.26](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.25...v9.0.26) (2022-02-05)

### [9.0.25](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.24...v9.0.25) (2022-02-04)

### [9.0.24](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.23...v9.0.24) (2022-02-03)

### [9.0.23](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.22...v9.0.23) (2022-02-02)

### [9.0.22](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.21...v9.0.22) (2022-02-01)


### Bug Fixes

* application model fields (EN-510) ([fb2738e](https://gitlab.com/cublueprint/beneficent/server/commit/fb2738ecf1cfb07771c9ba3627c9b77a91516fc1))

### [9.0.21](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.20...v9.0.21) (2022-01-31)

### [9.0.20](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.19...v9.0.20) (2022-01-30)

### [9.0.19](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.18...v9.0.19) (2022-01-29)

### [9.0.18](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.17...v9.0.18) (2022-01-28)

### [9.0.17](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.16...v9.0.17) (2022-01-27)

### [9.0.16](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.15...v9.0.16) (2022-01-26)

### [9.0.15](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.14...v9.0.15) (2022-01-24)

### [9.0.14](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.13...v9.0.14) (2022-01-23)

### [9.0.13](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.12...v9.0.13) (2022-01-22)

### [9.0.12](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.11...v9.0.12) (2022-01-22)

### [9.0.11](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.10...v9.0.11) (2022-01-22)


### Bug Fixes

* application upload script errors ([1f2a948](https://gitlab.com/cublueprint/beneficent/server/commit/1f2a94843e8ead9426d6217a1ba6360979272fdf))

### [9.0.10](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.9...v9.0.10) (2022-01-22)

### [9.0.9](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.8...v9.0.9) (2022-01-21)

### [9.0.8](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.7...v9.0.8) (2022-01-20)

### [9.0.7](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.6...v9.0.7) (2022-01-19)

### [9.0.6](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.5...v9.0.6) (2022-01-18)

### [9.0.5](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.4...v9.0.5) (2022-01-17)

### [9.0.4](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.3...v9.0.4) (2022-01-15)

### [9.0.3](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.2...v9.0.3) (2022-01-14)


### Bug Fixes

* update filter rules ([0c8d569](https://gitlab.com/cublueprint/beneficent/server/commit/0c8d5693bffabb6b435d7f3cb07a16fdd1e781cc))

### [9.0.2](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.1...v9.0.2) (2022-01-13)

### [9.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v9.0.0...v9.0.1) (2022-01-12)

## [9.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v8.0.5...v9.0.0) (2022-01-11)


### ⚠ BREAKING CHANGES

* move release pipeline into a template

### Features

* move release pipeline into a template ([98d61aa](https://gitlab.com/cublueprint/beneficent/server/commit/98d61aade2ad8635ef1173dc86d191d3fdcf0269))

### [8.0.5](https://gitlab.com/cublueprint/beneficent/server/compare/v8.0.4...v8.0.5) (2022-01-11)

### [8.0.4](https://gitlab.com/cublueprint/beneficent/server/compare/v8.0.3...v8.0.4) (2022-01-11)

### [8.0.3](https://gitlab.com/cublueprint/beneficent/server/compare/v8.0.2...v8.0.3) (2022-01-10)

### [8.0.2](https://gitlab.com/cublueprint/beneficent/server/compare/v8.0.1...v8.0.2) (2022-01-10)

### [8.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v8.0.0...v8.0.1) (2022-01-09)

## [8.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v7.0.5...v8.0.0) (2022-01-08)


### ⚠ BREAKING CHANGES

* add dateapplied field (EN-498)
* user model required fields (EN-501)

### Features

* add dateapplied field (EN-498) ([3a82903](https://gitlab.com/cublueprint/beneficent/server/commit/3a829038a049f6b1a6ba149cf3e9f68ad6342989))


### Bug Fixes

* user model required fields (EN-501) ([97c6231](https://gitlab.com/cublueprint/beneficent/server/commit/97c62311b80c30ee9db3ce428bd3512342c3de1e))

### [7.0.5](https://gitlab.com/cublueprint/beneficent/server/compare/v7.0.4...v7.0.5) (2022-01-07)

### [7.0.4](https://gitlab.com/cublueprint/beneficent/server/compare/v7.0.3...v7.0.4) (2022-01-06)


### Bug Fixes

* add date filters (EN-413) ([1bb9203](https://gitlab.com/cublueprint/beneficent/server/commit/1bb92033752994a0ca233315985c2446aab1e413))

### [7.0.3](https://gitlab.com/cublueprint/beneficent/server/compare/v7.0.2...v7.0.3) (2022-01-06)


### Bug Fixes

* update script logic (EN-496) ([c96b68a](https://gitlab.com/cublueprint/beneficent/server/commit/c96b68a607c3099512f2e582f8781884f9d80040))

### [7.0.2](https://gitlab.com/cublueprint/beneficent/server/compare/v7.0.1...v7.0.2) (2022-01-05)

### [7.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v7.0.0...v7.0.1) (2022-01-02)

## [7.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v6.0.1...v7.0.0) (2022-01-02)


### ⚠ BREAKING CHANGES

* application import validation (EN-490)

### Bug Fixes

* application import validation (EN-490) ([11d5467](https://gitlab.com/cublueprint/beneficent/server/commit/11d5467d56135f4b6820735cf375f5decf78f54a))
* contract payment values (EN-491) ([aaf3ffc](https://gitlab.com/cublueprint/beneficent/server/commit/aaf3ffc9d2dfd26781dfbab9e35391faad600105))

### [6.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v6.0.0...v6.0.1) (2021-12-30)

## [6.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v5.1.1...v6.0.0) (2021-12-28)


### ⚠ BREAKING CHANGES

* allow empty guarantor on patch (EN-471)

### Bug Fixes

* allow empty guarantor on patch (EN-471) ([6c7a766](https://gitlab.com/cublueprint/beneficent/server/commit/6c7a7662d329445592d6848bfced76071dfb4ce5))

### [5.1.1](https://gitlab.com/cublueprint/beneficent/server/compare/v5.1.0...v5.1.1) (2021-12-27)


### Bug Fixes

* Client detail view status change not working (EN-481) ([2d32e77](https://gitlab.com/cublueprint/beneficent/server/commit/2d32e77a652bf2d16b21fc3b645089bf49dac46e))

## [5.1.0](https://gitlab.com/cublueprint/beneficent/server/compare/v5.0.0...v5.1.0) (2021-12-26)


### Features

* applications upload script (EN-490) ([572116d](https://gitlab.com/cublueprint/beneficent/server/commit/572116dde756b1d14d97c8d0a703101bbcfbdfaa))

## [5.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v4.1.3...v5.0.0) (2021-12-23)


### ⚠ BREAKING CHANGES

* add logic to download active contract (EN-418)

### Bug Fixes

* add logic to download active contract (EN-418) ([90b1679](https://gitlab.com/cublueprint/beneficent/server/commit/90b1679523a3a1ca06c4d0aaf2cf2ea8574f9897))
* add validation for no payments (EN-424) ([6f404f9](https://gitlab.com/cublueprint/beneficent/server/commit/6f404f9c0fbb7a112734d4c8d1cbd44eb385f192))
* Fix contract bugs (EN-473) (EN-485 ([7345402](https://gitlab.com/cublueprint/beneficent/server/commit/73454027e4833a4a0c109929278eca6f0f2ae800))

### [4.1.3](https://gitlab.com/cublueprint/beneficent/server/compare/v4.1.2...v4.1.3) (2021-12-15)

### [4.1.2](https://gitlab.com/cublueprint/beneficent/server/compare/v4.1.1...v4.1.2) (2021-12-15)

### [4.1.1](https://gitlab.com/cublueprint/beneficent/server/compare/v4.1.0...v4.1.1) (2021-12-15)

## [4.1.0](https://gitlab.com/cublueprint/beneficent/server/compare/v4.0.2...v4.1.0) (2021-12-15)


### Features

* add cache controller (EN-424) ([814c526](https://gitlab.com/cublueprint/beneficent/server/commit/814c5263c804be759b33d2a191cf95de9db83ed1))

### [4.0.2](https://gitlab.com/cublueprint/beneficent/server/compare/v4.0.1...v4.0.2) (2021-12-15)


### Bug Fixes

* handle wave api failures gracefully (EN-424) ([c5f3971](https://gitlab.com/cublueprint/beneficent/server/commit/c5f3971632d9a1d1d924f31f4071d2681569e457))

### [4.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v4.0.0...v4.0.1) (2021-12-12)


### Bug Fixes

* add validation for moving app to active client status (EN-424) ([813037e](https://gitlab.com/cublueprint/beneficent/server/commit/813037e329d9caee419b94ac05aabebd35726393))

## [4.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v3.0.2...v4.0.0) (2021-12-10)


### ⚠ BREAKING CHANGES

* add payments api and integrate with wave (EN-424)

### Features

* add payments api and integrate with wave (EN-424) ([4ebde6d](https://gitlab.com/cublueprint/beneficent/server/commit/4ebde6d87e1cc85012fb185585c329a83948e9a0))


### Bug Fixes

* duplicate application emails and create application bugs (EN-440) (EN-EN-442) ([40b1649](https://gitlab.com/cublueprint/beneficent/server/commit/40b1649e70b8355b260bf1c2566ade81eab4f8f5))

### [3.0.2](https://gitlab.com/cublueprint/beneficent/server/compare/v3.0.1...v3.0.2) (2021-12-03)

### [3.0.1](https://gitlab.com/cublueprint/beneficent/server/compare/v3.0.0...v3.0.1) (2021-12-03)


### Bug Fixes

* backend status logic (EN-423) ([b800aa2](https://gitlab.com/cublueprint/beneficent/server/commit/b800aa2bc3dcd3d88a9f62e5b7a3eea0cbc43b30))
* update contract backend logic (EN-426) ([f291d18](https://gitlab.com/cublueprint/beneficent/server/commit/f291d187285664fc23316a6d4b184a54e88396cf))

## [3.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v2.0.0...v3.0.0) (2021-11-15)


### ⚠ BREAKING CHANGES

* fix contract model reference
* fix patch validation for guarantor object ()

### Features

* add postal code to application model (EN-402) ([34f3d1c](https://gitlab.com/cublueprint/beneficent/server/commit/34f3d1cad363615b83e57f103418315121245a82))
* query client by application ID (EN-399) ([e7e6cbb](https://gitlab.com/cublueprint/beneficent/server/commit/e7e6cbb8c7e7ee9abad28104537a8426c984e7c8))


### Bug Fixes

* fix contract model reference ([0415d36](https://gitlab.com/cublueprint/beneficent/server/commit/0415d364921ba2724968265d59b84c1890bb6b04))
* fix empty postal codes not being allowed (EN-402) ([5a4f664](https://gitlab.com/cublueprint/beneficent/server/commit/5a4f66405fb51b51377b6ca4c1302aa48ec22f54))
* fix patch validation for guarantor object () ([4419176](https://gitlab.com/cublueprint/beneficent/server/commit/4419176940f311172ebba25853cd434310724d91))
* seed and app validation (EN-416) ([69d0e56](https://gitlab.com/cublueprint/beneficent/server/commit/69d0e56a4e3d1a5320ebac2a99c172c853ed4c75))

## [2.0.0](https://gitlab.com/cublueprint/beneficent/server/compare/v1.0.0...v2.0.0) (2021-10-21)


### ⚠ BREAKING CHANGES

* add uploadedBy fields to document model (EN-384)

### Features

* add status field to client (EN-385) ([0e89b28](https://gitlab.com/cublueprint/beneficent/server/commit/0e89b28c47d72a98206cf0a6cd9291ab410d4bd5))
* add uploadedBy fields to document model (EN-384) ([ed01e1e](https://gitlab.com/cublueprint/beneficent/server/commit/ed01e1ebdb40665afcba50934368cb9d3a66ac7a))


### Bug Fixes

* coverage report path (EN-361) ([70f41a7](https://gitlab.com/cublueprint/beneficent/server/commit/70f41a72652937c375b24ca96f90c2f003650011))
* invalid paths in pipeline (EN-361) ([41ee687](https://gitlab.com/cublueprint/beneficent/server/commit/41ee687133d295798a6295bf27833d2636adc719))
* open handle warning in jest (EN-322) ([b151f46](https://gitlab.com/cublueprint/beneficent/server/commit/b151f46fb2d03e31370327f51fdd782f207d1618))
* update client validation (EN-385) ([86b043c](https://gitlab.com/cublueprint/beneficent/server/commit/86b043c63d5585fcef200d5505e31ed252475be5))
* use images from build stage (EN-361) ([76a49a2](https://gitlab.com/cublueprint/beneficent/server/commit/76a49a232b1a6bc3d719c0dc73677f58b12b2d77))

## 1.0.0 (2021-08-11)
