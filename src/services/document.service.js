const { Document } = require("../models");
const { validateApplicationId, validateUserId } = require("../utils/serviceUtils");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

/**
 * Create a document
 * @param {Object} documentBody
 * @param {Object} fields
 * @returns {Promise<Document>}
 */
const createDocument = async (documentBody, fields) => {
  await validateApplicationId(fields.application, "Application with this ID does not exist");
  await validateUserId(fields.uploadedBy, "User with this ID does not exist");
  const documentObj = { ...documentBody, ...fields };
  return Document.create(documentObj);
};

/**
 * Query for documents
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryDocuments = async (filters, options) => {
  const documents = await Document.paginate(filters, options);
  if (documents.results.length == 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No documents found");
  }
  return documents;
};

/**
 * Get document by id
 * @param {ObjectId} document
 * @returns {Promise<Document>}
 */
const getDocumentById = async (documentId) => {
  const document = await Document.findById(documentId);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, "Document not found");
  }
  return document;
};

/**
 * Update document by id
 * @param {ObjectId} documentId
 * @param {Object} updateBody
 * @returns {Promise<Client>}
 */
const updateDocumentById = async (documentId, updateBody) => {
  const document = await getDocumentById(documentId);
  await validateUserId(updateBody.updatedBy, "User with this ID does not exist");

  Object.assign(document, updateBody);
  await document.save();
  return document;
};

/**
 * Delete document by id
 * @param {ObjectId} documentId
 * @returns {Promise<Document>}
 */
const deleteDocumentById = async (documentId) => {
  const document = await getDocumentById(documentId);
  await document.remove();
  return document;
};

module.exports = {
  createDocument,
  queryDocuments,
  getDocumentById,
  updateDocumentById,
  deleteDocumentById
};
