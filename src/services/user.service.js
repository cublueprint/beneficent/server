const httpStatus = require("http-status");
const { User } = require("../models");
const ApiError = require("../utils/ApiError");
const crypto = require("crypto");
const { createOfficer, deleteOfficerByUser } = require("./officer.service");
const { generateResetPasswordToken, generateVerifyEmailToken } = require("./token.service");
const {
  sendResetPasswordEmail,
  sendVerificationEmail,
  sendNewUserEmail,
  sendUpdatedAccountEmail
} = require("./email.service");
/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const registerUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");
  }
  if (!userBody.password) {
    const randomBytes = await crypto.randomBytes(64);
    userBody.password = randomBytes.toString("hex");
    userBody.hasTempPassword = true;
  }
  const user = await User.create(userBody);
  if (userBody.isLoanOfficer) {
    const newOfficer = { ...user, user: user };
    await createOfficer(newOfficer);
  }
  const verifyEmailToken = await generateVerifyEmailToken(user.email);
  await sendNewUserEmail(user.email, verifyEmailToken);

  return user;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  let userFilter = { $and: [{ $or: [] }], $or: [] };

  if (filter.nameOrEmail) {
    filter.nameOrEmail
      .trim()
      .split(" ")
      .forEach((word) => {
        userFilter.$and[0].$or.push({ firstName: { $regex: word, $options: "i" } });
        userFilter.$and[0].$or.push({ lastName: { $regex: word, $options: "i" } });
        userFilter.$and[0].$or.push({ email: { $regex: word, $options: "i" } });
      });
  }

  if (filter.phoneNumber) userFilter.$and.push({ phoneNumber: filter.phoneNumber });
  if (filter.address) userFilter.$and.push({ address: filter.address });
  if (filter.country) userFilter.$and.push({ country: filter.country });
  if (filter.role) userFilter.$and.push({ role: filter.role });

  if (userFilter.$and[0].$or.length === 0) userFilter.$and.shift();
  if (userFilter.$and.length === 0) delete userFilter.$and;
  if (userFilter.$or.length === 0) delete userFilter.$or;

  const users = await User.paginate(userFilter, options);
  if (users.results.length == 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No users found");
  }
  return users;
};

/**
 * Get user by userId
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const getUserById = async (userId) => {
  return User.findById(userId);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  let newEmail = "";

  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");
  }
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  if (updateBody.isLoanOfficer == true && !user.isLoanOfficer) {
    const newOfficer = { ...user, user: user };
    await createOfficer(newOfficer);
  } else if (updateBody.isLoanOfficer == false && user.isLoanOfficer) {
    await deleteOfficerByUser(user);
  }
  if (updateBody.email && updateBody.email != user.email) {
    newEmail = updateBody.email;
  }

  Object.assign(user, updateBody);
  await user.save();

  if (newEmail != "") {
    await user.revertRoles();
    await user.save();

    const verifyEmailToken = await generateVerifyEmailToken(updateBody.email);
    await sendUpdatedAccountEmail(updateBody.email, verifyEmailToken);
  }

  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  if (user.isLoanOfficer) {
    await deleteOfficerByUser(user);
  }
  await user.remove();
  return user;
};

module.exports = {
  registerUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  deleteUserById
};
