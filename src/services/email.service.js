const nodemailer = require("nodemailer");
const config = require("../config/config");
const logger = require("../config/logger");
const moment = require("moment");

const transport = nodemailer.createTransport(config.email.smtp);
/* istanbul ignore next */
if (config.env !== "test") {
  transport
    .verify()
    .then(() => logger.info(`Connected to email server at ${config.email.from}`))
    .catch(() => logger.warn("Unable to connect to email server. Make sure you have configured the SMTP options in .env"));
}

/**
 * Send an email
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmail = async (to, subject, text) => {
  const msg = { from: config.email.from, to, subject, text };
  await transport.sendMail(msg);
};

/**
 * Send reset password email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendResetPasswordEmail = async (to, token) => {
  const subject = "Reset password";
  const resetPasswordUrl = `${config.clientUrl}/reset-password/${token}`;
  const text = `Dear user,
To reset your password, click on this link: ${resetPasswordUrl}
If you did not request any password resets, then ignore this email.`;
  await sendEmail(to, subject, text);
};

/**
 * Send set password email for new accounts
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendSetPasswordEmail = async (to, token) => {
  const subject = "Set password";
  const resetPasswordUrl = `${config.clientUrl}/reset-password/${token}`;
  const text = `Dear user,
To set your account password, click on this link: ${resetPasswordUrl}`;
  await sendEmail(to, subject, text);
};

/**
 * Send verification email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendVerificationEmail = async (to, token) => {
  const subject = "Email Verification";
  const verificationEmailUrl = `${config.clientUrl}/verify-email?token=${token}`;
  const text = `Dear user,
  To verify your email, click on this link: ${verificationEmailUrl}
  If you did not request any email verification, then ignore this email.`;
  await sendEmail(to, subject, text);
};

/**
 * Send verification instruction email to a newly created user account
 * @param {string} to
 * @param {string} token
 * @param {string} password
 * @returns {Promise}
 */
const sendNewUserEmail = async (to, token) => {
  const subject = "New Account Created";
  const verificationEmailUrl = `${config.clientUrl}/verify-email?token=${token}`;
  const text = `Dear user,
  You have just created a new account. Please follow the steps below in order to correctly verify your account: \n
  1. To verify your email, click on this link: ${verificationEmailUrl} \n
  2. After verifying your email, you may recieve an email prompting you to set your account password. Please set your password in order to login.`;

  await sendEmail(to, subject, text);
};

/**
 * Send verification instruction email to a user whose account was recently modified
 * @param {string} to
 * @param {string} token
 * @param {string} password
 * @returns {Promise}
 */
const sendUpdatedAccountEmail = async (to, token) => {
  const subject = "Account Email Changed";
  const verificationEmailUrl = `${config.clientUrl}/verify-email?token=${token}`;
  const text = `Dear user,
  Your account was recently modified with the new email address: ${to}. Please follow the steps below in order to correctly re-verify your account: \n
  1. To verify your new email, click on this link: ${verificationEmailUrl} \n
  2. If this is a new account, you will recieve an email prompting you to set your account password. Please set your password in order to login.`;

  await sendEmail(to, subject, text);
};

/**
 * Format application submission email
 * @param {Object} application application body
 * @returns {Promise}
 */
const submitApplicationEmail = async (application) => {
  let applicationBody = formatApplicationBody(application);

  const emailBody = `Hi ${application.firstName}, \n Your application has been successfully submitted.\n Here is the information you provided:\n
  --------------------\n${applicationBody}\n-------------------- 
  Sincerely,\n
  Beneficent`;

  const subject = "Beneficent - Loan Application Submitted Successfully";
  await sendEmail(application.email, subject, emailBody);
};

/**
 * Format application submission email
 * @param {Object} body application body
 * @returns {string}
 */
const formatApplicationBody = (body) => {
  let formattedBody = ``;
  let newKey = "";

  for (const [key, value] of Object.entries(body)) {
    // Split camel case keys into string separated by spaces, and capitalize each word
    // ex: firstName --> First Name
    newKey = key.replace(/([a-z])([A-Z])/g, "$1 $2").replace(/\b\w/g, (c) => c.toUpperCase());
    formattedBody += `${newKey}: `;

    if (key == "dateOfBirth" || key == "dateApplied") {
      formattedBody += `${moment(value).format("YYYY/MMM/DD")}\n`;
    }
    // For parsing keys and values of nested objects like Guarantor, Acknowledgments, Services,
    else if (typeof value == "object") {
      formattedBody += "\n";

      for (const [innerKey, innerValue] of Object.entries(value)) {
        newKey = innerKey.replace(/([a-z])([A-Z])/g, "$1 $2").replace(/\b\w/g, (c) => c.toUpperCase());
        formattedBody += `\t ${newKey}: ${innerValue}\n`;
      }
    } else {
      formattedBody += `${value}\n`;
    }
  }

  return formattedBody;
};

module.exports = {
  transport,
  sendEmail,
  sendResetPasswordEmail,
  sendVerificationEmail,
  submitApplicationEmail,
  sendNewUserEmail,
  sendUpdatedAccountEmail,
  sendSetPasswordEmail
};
