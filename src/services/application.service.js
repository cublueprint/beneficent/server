const httpStatus = require("http-status");
const logger = require("../config/logger");
const { Application, User, Contract, Client, Officer } = require("../models");
const ApiError = require("../utils/ApiError");
const { validateOfficerId, validateUserId } = require("../utils/serviceUtils");
const { addDataToDatabase } = require("../utils/migrateCSV");
const { submitApplicationEmail } = require("./email.service");
const clientService = require("./client.service");
const waveService = require("./wave.service");

/**
 * Create a application
 * @param {Object} applicationBody
 * @returns {Promise<Application>}
 */
const createApplication = async (applicationBody) => {
  if (applicationBody.user) {
    await validateUserId(applicationBody.user, "User with this ID does not exist");
    const user = await User.findById(applicationBody.user);
    if (
      user.email !== applicationBody.email ||
      user.firstName !== applicationBody.firstName ||
      user.lastName !== applicationBody.lastName
    ) {
      throw new ApiError(httpStatus.BAD_REQUEST, "Application name and/or email does not match with user name and/or email");
    }
  }
  if (applicationBody.officer) {
    await validateOfficerId(applicationBody.officer, "Officer with this ID does not exist");
  }
  const application = { ...applicationBody, dateApplied: applicationBody.dateApplied || Date.now() };
  const createdApp = await Application.create(application);
  await submitApplicationEmail(application);
  return createdApp;
};

/**
 *
 * @param {Buffer} file
 * @param {String} token
 * @returns {Object} - Successfully imported applications and array of warnings
 */
const importApplications = async (file, token) => {
  const { warnings, createdApps } = await addDataToDatabase(file, token);
  const result = {
    successful: createdApps.length,
    failed: warnings.length,
    warnings: warnings,
    created: createdApps
  };
  return result;
};

/**
 * Query for applications
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryApplications = async (filter, options) => {
  let applicationFilter = { $and: [{ $or: [] }], $or: [] };

  if (filter.nameOrEmail) {
    filter.nameOrEmail
      .trim()
      .split(" ")
      .forEach((word) => {
        applicationFilter.$and[0].$or.push({ firstName: { $regex: word, $options: "i" } });
        applicationFilter.$and[0].$or.push({ lastName: { $regex: word, $options: "i" } });
        applicationFilter.$and[0].$or.push({ email: { $regex: word, $options: "i" } });
      });
  }

  if (filter.status) applicationFilter.$or.push({ status: filter.status });
  if (!filter.displayAll) applicationFilter.$and.push({ status: { $nin: ["Active Client", "Archived"] } });
  if (filter.user) applicationFilter.$and.push({ user: filter.user });
  if (filter.officer) applicationFilter.$or.push({ officer: filter.officer });
  if (filter.startDate || filter.endDate) {
    applicationFilter.$and.push({
      dateApplied: {
        $gt: filter.startDate ? new Date(filter.startDate) : new Date("January 1, 1970 00:00:01"),
        $lt: filter.endDate ? new Date(filter.endDate) : Date.now()
      }
    });
  }
  if (applicationFilter.$and[0].$or.length === 0) applicationFilter.$and.shift();
  if (applicationFilter.$and.length === 0) delete applicationFilter.$and;
  if (applicationFilter.$or.length === 0) delete applicationFilter.$or;

  const applications = await Application.paginate(applicationFilter, options);
  if (applications.results.length == 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No applications found");
  }

  return applications;
};

/**
 * Get application by id
 * @param {ObjectId} applicationId
 * @returns {Promise<Application>}
 */
const getApplicationById = async (applicationId) => {
  const application = await Application.findById(applicationId);
  if (!application) {
    throw new ApiError(httpStatus.NOT_FOUND, "Application not found");
  }

  return application;
};

/**
 * Get application by user id
 * @param {ObjectId} userId
 * @returns {Promise<Application>}
 */
const getApplicationByUserId = async (userId) => {
  return Application.findOne({ user: userId });
};

/**
 * Update application by id
 * @param {ObjectId} applicationId
 * @param {Object} updateBody
 * @returns {Promise<Application>}
 */
const updateApplicationById = async (applicationId, updateBody) => {
  const application = await getApplicationById(applicationId);

  if (updateBody.user) {
    const user = await User.findById(updateBody.user);
    if (!user) {
      throw new ApiError(httpStatus.NOT_FOUND, "User not found");
    }
  }

  if (updateBody.officer) {
    const officer = await Officer.findById(updateBody.officer);
    if (!officer) {
      throw new ApiError(httpStatus.NOT_FOUND, "Officer not found");
    }
  }

  if (updateBody.status && !application.officer) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Cannot update a status with no assigned officer");
  }

  const client = await Client.findOne({ application: applicationId });

  if (updateBody.status && client) {
    clientService.updateClientById(client._id, { status: updateBody.status });
  }

  if (updateBody.status === "Active Client" && !client) {
    const contract = await Contract.findOne({ application: applicationId, status: "Active" });

    if (!contract) {
      throw new ApiError(httpStatus.BAD_REQUEST, "Cannot update status to Active Client without an active contract");
    }

    const waveCustomerId = await waveService.createCustomer({
      email: application.email,
      firstName: application.firstName,
      lastName: application.lastName,
      city: application.city,
      countryCode: "CA"
    });

    if (!waveCustomerId) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Failed to create customer in Wave");
    }

    const client = await clientService.createClient({
      dateSigned: Date.now(),
      wave: {
        customerId: waveCustomerId
      },
      application: application._id,
      officer: application.officer
    });

    if (!client) {
      throw new ApiError(httpStatus.BAD_REQUEST, "Failed to create a client for this application");
    }
    try {
      const productId = await waveService.createProduct({
        amount: `${contract.approvedLoanAmount}`,
        firstName: application.firstName,
        lastName: application.lastName
      });

      const { invoice, invoiceUrl } = await waveService.createInvoice({
        waveCustomerId: client.wave.customerId,
        dueDate: contract.finalPaymentDue.toISOString().slice(0, 10),
        invoiceDate: contract.contractStartDate.toISOString().slice(0, 10),
        product: productId
      });

      client.wave.invoiceId = invoice;
      client.wave.invoiceUrl = invoiceUrl;
      client.wave.productId = productId;

      await waveService.approveInvoice(invoice);
    } catch (error) {
      logger.error(error);
      await clientService.deleteClientById(client._id);
      throw error;
    }

    contract.client = client._id;
    await contract.save();
    await client.save();
  }

  Object.assign(application, updateBody);
  await application.save();
  const updatedApplication = Application.updateValidStatuses(applicationId);
  return updatedApplication;
};

/**
 * Delete application by id
 * @param {ObjectId} applicationId
 * @returns {Promise<Application>}
 */
const deleteApplicationById = async (applicationId) => {
  const application = await getApplicationById(applicationId);
  await application.remove();
  return application;
};

module.exports = {
  createApplication,
  queryApplications,
  getApplicationById,
  getApplicationByUserId,
  updateApplicationById,
  deleteApplicationById,
  importApplications
};
