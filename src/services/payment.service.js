const httpStatus = require("http-status");
const logger = require("../config/logger");
const { Payment, Contract, Client } = require("../models");
const ApiError = require("../utils/ApiError");
const waveService = require("./wave.service");

/**
 * Query for payments
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryPayments = async (filter, options) => {
  if (!filter.cache) {
    await updatePayments(filter.client);
  }
  delete filter.cache;
  const payments = await Payment.paginate(filter, options);
  if (payments.results.length === 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No payments found");
  }
  return payments;
};

/**
 * Process payments for a client
 * @param {String} clientId - client id
 */
const updatePayments = async (clientId) => {
  await Payment.deleteMany({ client: clientId });
  const client = await Client.findById(clientId);
  const contract = await Contract.findOne({ status: "Active", client: clientId });

  if (!contract) {
    logger.info(`No active contract found for client ${clientId}`);
    return;
  }

  //every time this form gets opened, check the data and

  const today = new Date();

  let nextPaymentDueDate = determineNextPayDueDateFromDate(contract.contractStartDate, today, contract.contractCycleLength);

  contract.wave = {
    totalAmountPaid: 0,
    totalAmountRemaining: contract.approvedLoanAmount,
    nextPaymentAmount: contract.monthlyPayment,
    expectedTotalAmountPaid: 0,
    nextPaymentAmount: contract.monthlyPayment,
    nextPaymentDueDate: nextPaymentDueDate,
    missedPayments: 0,
    credit: 0
  };

  await contract.save();

  const payments = await waveService.scrapePayments(client.wave.invoiceUrl, clientId);

  if (!payments) {
    return;
  }

  payments.sort((a, b) => {
    return a.datePaid - b.datePaid;
  });
  try {
    let cummulativePaidOff = 0;
    for (const payment of payments) {
      //determine cycle number at date paid
      //determine amount due with cycle number
      //subtract amount due from cummulative amount paid

      let cycleAtPayment = determineCycleCountAtDate(
        contract.contractStartDate,
        payment.datePaid,
        contract.contractCycleLength
      );

      let amountDue = contract.monthlyPayment * cycleAtPayment - cummulativePaidOff;
      let amountPaid = payment.amountPaid;
      let dateDue = determineNextPayDueDateFromDate(
        contract.contractStartDate,
        payment.datePaid,
        contract.contractCycleLength
      );
      let datePaid = payment.datePaid;

      cummulativePaidOff += payment.amountPaid;

      const paymentData = {
        amountDue,
        amountPaid,
        dateDue,
        datePaid,
        client: clientId,
        contract: contract._id
      };

      contract.wave.totalAmountPaid = cummulativePaidOff;

      await contract.save();
      await Payment.create(paymentData);
    }
  } catch (_) {
    logger.error("Failed to update payments for contract", { contractId: contract._id });
    return;
  }
};

/**
 * Determine next pay due date from a given data
 * @param {Date} contractStartDate
 * @param {Date} date
 * @param {Number} cycleLength
 * @returns
 */
const determineNextPayDueDateFromDate = (contractStartDate, date, cycleLength) => {
  const numCycles = determineCycleCountAtDate(contractStartDate, date, cycleLength);
  let nextPaymentDueDate = contractStartDate;

  //This can probably be optimized
  for (let i = 0; i < numCycles; i++) {
    nextPaymentDueDate = addXDays(nextPaymentDueDate, cycleLength);
  }

  return nextPaymentDueDate;
};

/**
 * Determine the current cycle number for a given date
 * @param {Date} contractStartDate
 * @param {Date} date
 * @param {Number} cycleLength
 * @returns
 */
const determineCycleCountAtDate = (contractStartDate, date, cycleLength) => {
  return Math.floor((date - contractStartDate) / cycleLength);
};

/**
 * Add cycleLength in days to date
 * @param {Date} date
 * @param {Number} cycleLength
 * @returns
 */
const addXDays = (date, cycleLength) => {
  const secondsInDay = 86400000;
  const daysToAdd = cycleLength / secondsInDay;
  const newDate = new Date(date);
  return new Date(newDate.setDate(newDate.getDate() + daysToAdd));
};

module.exports = {
  queryPayments
};
