const httpStatus = require("http-status");
const { GraphQLClient, gql } = require("graphql-request");
const fetch = require("node-fetch");
const JSSoup = require("jssoup").default;
const { normalizeText } = require("normalize-text");
const config = require("../config/config");
const logger = require("../config/logger");
const ApiError = require("../utils/ApiError");

const graphQLClient = new GraphQLClient(config.wave.apiEndpoint, {
  headers: {
    authorization: `Bearer ${config.wave.apiKey}`
  }
});

/**
 * Create a customer in Wave
 * @param {Object} customerBody
 * @param {String} customerBody.email
 * @param {String} customerBody.firstName
 * @param {String} customerBody.lastName
 * @param {String} customerBody.city
 * @param {String} customerBody.countryCode
 * @returns {Promise<String>} customerId
 */
const createCustomer = async (customerBody) => {
  const variables = {
    input: {
      businessId: `${config.wave.businessId}`,
      name: `${customerBody.firstName} ${customerBody.lastName}`,
      firstName: `${customerBody.firstName}`,
      lastName: `${customerBody.lastName}`,
      email: `${customerBody.email}`,
      address: {
        city: `${customerBody.city}`,
        countryCode: `${customerBody.countryCode}`
      },
      currency: "CAD"
    }
  };

  const mutation = gql`
    mutation ($input: CustomerCreateInput!) {
      customerCreate(input: $input) {
        didSucceed
        inputErrors {
          code
          message
          path
        }
        customer {
          id
          name
          firstName
          lastName
          email
          address {
            addressLine1
            addressLine2
            city
            province {
              code
              name
            }
            country {
              code
              name
            }
            postalCode
          }
          currency {
            code
          }
        }
      }
    }
  `;
  try {
    const res = await graphQLClient.request(mutation, variables);
    if (res.customerCreate?.didSucceed) {
      return res.customerCreate.customer.id;
    } else {
      throw new ApiError(httpStatus.BAD_REQUEST, "Failed to create customer in Wave");
    }
  } catch (err) {
    logger.error(err);
    throw new ApiError(httpStatus.BAD_REQUEST, "Invalid customerCreate input provided");
  }
};

/**
 * Create a product in Wave
 * @param {Object} productBody
 * @param {String} productBody.amount
 * @param {String} productBody.firstName
 * @param {String} productBody.lastName
 * @returns {Promise<String>} productId
 */
const createProduct = async (productBody) => {
  const variables = {
    input: {
      businessId: `${config.wave.businessId}`,
      name: `${productBody.firstName} ${productBody.lastName} - Loan`,
      unitPrice: `${productBody.amount}`,
      incomeAccountId: `${config.wave.incomeAccountId}`
    }
  };

  const mutation = gql`
    mutation ($input: ProductCreateInput!) {
      productCreate(input: $input) {
        didSucceed
        inputErrors {
          code
          message
          path
        }
        product {
          id
        }
      }
    }
  `;

  try {
    const res = await graphQLClient.request(mutation, variables);
    if (res.productCreate?.didSucceed) {
      return res.productCreate.product.id;
    } else {
      throw new ApiError(httpStatus.BAD_REQUEST, "Failed to create product in Wave");
    }
  } catch (err) {
    logger.error(err);
    throw new ApiError(httpStatus.BAD_REQUEST, "Invalid productCreate input provided");
  }
};

/**
 * Create a invoice in Wave
 * @param {Object} invoiceBody
 * @param {String} invoiceBody.waveCustomerId
 * @param {Date} invoiceBody.dueDate
 * @param {Date} invoiceBody.invoiceDate
 * @param {String} invoiceBody.product
 * @returns {Promise<Wave>} retuns a wave object
 */
const createInvoice = async (invoiceBody) => {
  const variables = {
    input: {
      businessId: config.wave.businessId,
      customerId: invoiceBody.waveCustomerId,
      dueDate: invoiceBody.dueDate,
      invoiceDate: invoiceBody.invoiceDate,
      items: [
        {
          productId: invoiceBody.product
        }
      ]
    }
  };

  const mutation = gql`
    mutation ($input: InvoiceCreateInput!) {
      invoiceCreate(input: $input) {
        didSucceed
        inputErrors {
          message
          code
          path
        }
        invoice {
          id
          viewUrl
        }
      }
    }
  `;

  try {
    const res = await graphQLClient.request(mutation, variables);
    if (res.invoiceCreate?.didSucceed) {
      return {
        invoice: res.invoiceCreate.invoice.id,
        invoiceUrl: res.invoiceCreate.invoice.viewUrl
      };
    } else {
      throw new ApiError(httpStatus.BAD_REQUEST, "Failed to create customer in Wave");
    }
  } catch (err) {
    logger.error(err);
    throw new ApiError(httpStatus.BAD_REQUEST, "Invalid invoiceCreate input provided");
  }
};

/**
 * Approve an invoice in Wave
 * @param {String} invoiceId
 * @returns {Promise<String>} invoiceId
 */
const approveInvoice = async (invoiceId) => {
  const variables = {
    input: {
      invoiceId: invoiceId
    }
  };

  const mutation = gql`
    mutation ($input: InvoiceApproveInput!) {
      invoiceApprove(input: $input) {
        didSucceed
        inputErrors {
          code
          message
          path
        }
        invoice {
          id
        }
      }
    }
  `;

  try {
    const res = await graphQLClient.request(mutation, variables);
    if (res.invoiceApprove?.didSucceed) {
      return res.invoiceApprove.invoice.id;
    } else {
      throw new ApiError(httpStatus.BAD_REQUEST, "Failed to create customer in Wave");
    }
  } catch (err) {
    logger.error(err);
    throw new ApiError(httpStatus.BAD_REQUEST, "Invalid invoiceApprove input provided");
  }
};

/**
 * Query payments for a customer in Wave
 * @param {String} waveUrl
 * @param {String} clientId
 * @returns {Promise<QueryResult>}
 */
const scrapePayments = async (waveUrl, clientId) => {
  try {
    const res = await fetch(waveUrl);
    const html = await res.text();
    const soup = new JSSoup(html);
    let table = soup.findAll("div", "contemporary-template__totals__amounts__line").map((row) => {
      return normalizeText(row.text);
    });
    table.pop();
    table.shift();
    const payments = table.map((row) => {
      const date = row.split("payment on ")[1].split(" using ")[0];
      const amount = row.split("payment on ")[1].split(" using ")[1].split("$")[1];
      return {
        amountPaid: parseFloat(amount.replace("$", "").replace(",", "")),
        datePaid: new Date(Date.parse(date)),
        client: clientId
      };
    });
    return payments;
  } catch (_) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Failed to scrape payments from Wave");
  }
};

module.exports = {
  createCustomer,
  createProduct,
  createInvoice,
  approveInvoice,
  scrapePayments
};
