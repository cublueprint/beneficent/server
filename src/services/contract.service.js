const httpStatus = require("http-status");
const { Application, Contract, Client } = require("../models");
const ApiError = require("../utils/ApiError");
const createContractPDF = require("../utils/createContractPDF");
const { validateUserId, validateApplicationId } = require("../utils/serviceUtils");
const mongoose = require("mongoose");

/**
 * Create a contract
 * @param {Object} contractBody
 * @returns {Promise<Contract>}
 */
const createContract = async (contractBody) => {
  await validateUserId(contractBody.createdBy, "User with this ID does not exist");
  await validateApplicationId(contractBody.application, "Application with this ID does not exist");
  const client = await Client.findOne({ application: contractBody.application });
  const fileName = await createContractFileName(contractBody.application);
  if (client) {
    return Contract.create({ ...contractBody, contractFileName: fileName, client: client._id });
  }
  return Contract.create({ ...contractBody, contractFileName: fileName });
};
/**
 * Query for contracts
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryContracts = async (filter, options) => {
  let contractFilter = { client: { $in: [] }, application: { $in: [] }, $or: [] };
  let validClientIds = [];
  let validApplicationIds = [];

  if (filter.application) validApplicationIds.push(filter.application);

  if (filter.client) validClientIds.push(filter.client);

  if (filter.officer) {
    const clients = await Client.find({ officer: filter.officer });
    clients.forEach((client) => validClientIds.push(client._id));
    if (clients.length === 0) {
      throw new ApiError(httpStatus.NOT_FOUND, "No contracts found"); //invalid officerId
    }
  }

  if (filter.status) contractFilter.$or.push({ status: filter.status });

  validClientIds.length > 0 ? (contractFilter.client.$in = validClientIds) : delete contractFilter.client;
  validApplicationIds.length > 0
    ? (contractFilter.application.$in = validApplicationIds)
    : delete contractFilter.application;

  if (contractFilter.$or.length === 0) delete contractFilter.$or;
  const contracts = await Contract.paginate(contractFilter, options);

  if (contracts.results.length === 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No contracts found");
  }
  return contracts;
};

/**
 * Creates file name for new contract
 * @param {ObjectId} applicationId id of application contract is tired to
 * @returns {String}
 */
const createContractFileName = async (applicationId) => {
  const application = await Application.findById(applicationId);
  return `${application.firstName}${application.lastName}-${Date.now()}.pdf`;
};

/**
 * Get contract by id
 * @param {ObjectId} contractId
 * @returns {Promise<Contract>}
 */
const getContractById = async (contractId) => {
  const contract = await Contract.findById(contractId);
  if (!contract) {
    throw new ApiError(httpStatus.NOT_FOUND, "Contract not found");
  }
  return contract;
};

/**
 * Update contract by id
 * @param {ObjectId} contractId
 * @param {Object} updateBody
 * @returns {Promise<Contract>}
 */
const updateContractById = async (contractId, updateBody) => {
  const contract = await getContractById(contractId);
  const approvedAmount = updateBody.approvedLoanAmount ? updateBody.approvedLoanAmount : contract.approvedLoanAmount;

  const finalPaymentTime = updateBody.finalPaymentDue
    ? updateBody.finalPaymentDue.getTime()
    : contract.finalPaymentDue.getTime();

  const firstPaymentTime = updateBody.firstPaymentDue
    ? updateBody.firstPaymentDue.getTime()
    : contract.firstPaymentDue.getTime();

  if (!updateBody.monthlyPayment) {
    updateBody.monthlyPayment = calculateNewMonthlyPayment(approvedAmount, finalPaymentTime, firstPaymentTime);
  }

  if (!updateBody.finalPayment) {
    updateBody.finalPayment = calculateNewFinalPayment(
      approvedAmount,
      updateBody.monthlyPayment,
      finalPaymentTime,
      firstPaymentTime
    );
  }

  Object.assign(contract, updateBody);
  await contract.save();
  return contract;
};

const secondsInMonth = 2628000000;

/**
 * @param {Number} approvedAmount
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns - Updated monthly payment rounded to the nearest 5
 */
const calculateNewMonthlyPayment = (approvedAmount, finalPaymentTime, firstPaymentTime) => {
  // approvedAmount, finalPaymentTime and firstPaymentTime are chosen based on whether or not
  // They were provided in the updateBody, otherwise, they are taken from the contract

  // Approved amount divided by number of months (rounded to the nearest month), rounded to nearest 5
  return Math.ceil(approvedAmount / Math.round((finalPaymentTime - firstPaymentTime) / secondsInMonth) / 5) * 5;
};

/**
 * @param {Number} approvedAmount
 * @param {Number} monthlyPayment
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns - Updated final payment after (# of months in term - 1) monthly payments have been made
 */
const calculateNewFinalPayment = (approvedAmount, monthlyPayment, finalPaymentTime, firstPaymentTime) => {
  // approvedAmount, finalPaymentTime and firstPaymentTime are chosen based on whether or not
  // They were provided in the updateBody, otherwise, they are taken from the contract
  // monthlyPayment will always either be sent with the updateBody or calculated

  // The remaining amount to pay after all but one month has been payed off at the monthly payment rate
  // ((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth) = # of months in payment term - 1 month

  const finalPayment =
    approvedAmount - monthlyPayment * Math.round((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth);

  // There may be cases where the new final payment calculated results in the loan being paid off earlier than required. In that case, return the final
  // amount that would be paid without exceeding the loan
  return finalPayment >= 0 ? finalPayment : monthlyPayment + finalPayment;
};

/**
 * Delete contract by id
 * @param {ObjectId} contractId
 * @returns {Promise<Contract>}
 */
const deleteContractById = async (contractId) => {
  const contract = await getContractById(contractId);
  await contract.remove();
  return contract;
};

/**
 * Create contract PDF by id
 * @param {ObjectId} contractId
 * @returns {String} - Name of contract PDF file
 */
const createContractPDFById = async (contractId) => {
  const contract = await getContractById(contractId);
  await contract
    .populate({
      path: "application",
      populate: {
        path: "officer user",
        populate: {
          path: "user"
        }
      }
    })
    .execPopulate();

  const fileBuffer = await createContractPDF(contract);
  await contract.save();
  return fileBuffer;
};

/**
 * Upload contract PDF
 * @param {ObjectId} contractId
 * @param {Buffer} contractFileBuffer
 * @returns {Promise<Contract>}
 */
const uploadContract = async (contractId, contractFileBuffer) => {
  const contract = await getContractById(contractId);

  archiveAllOtherContracts(contract);

  contract.status = "Archived";
  await contract.save();

  const newContract = {};
  Object.assign(newContract, contract._doc);
  newContract.status = "Active";
  newContract.contractFileBuffer = contractFileBuffer;
  newContract._id = new mongoose.Types.ObjectId();
  return await createContract(newContract);
};

/**
 * Archive all contracts under the same application when new contract is uploaded
 * @param {Object} contract
 */
const archiveAllOtherContracts = async (contract) => {
  const allContractsWithSameApp = await Contract.find({ application: contract.application });

  for (let i = 0; i < allContractsWithSameApp.length; i++) {
    if (allContractsWithSameApp[i].id != contract.id && allContractsWithSameApp[i].status == "Active") {
      await updateContractById(allContractsWithSameApp[i].id, { status: "Archived" });
    }
  }
};

module.exports = {
  createContract,
  queryContracts,
  getContractById,
  updateContractById,
  deleteContractById,
  createContractPDFById,
  uploadContract
};
