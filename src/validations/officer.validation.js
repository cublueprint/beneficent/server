const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createOfficer = {
  body: Joi.object().keys({
    user: Joi.string().required().custom(objectId)
  })
};

const getOfficers = {
  query: Joi.object().keys({
    firstName: Joi.string(),
    lastName: Joi.string(),
    email: Joi.string().email(),
    user: Joi.string().custom(objectId),
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getOfficer = {
  params: Joi.object().keys({
    officerId: Joi.required().custom(objectId)
  })
};

const updateOfficer = {
  params: Joi.object().keys({
    officerId: Joi.required().custom(objectId)
  }),
  body: Joi.object()
    .keys({
      user: Joi.string().custom(objectId)
    })
    .min(1)
};

const deleteOfficer = {
  params: Joi.object().keys({
    officerId: Joi.required().custom(objectId)
  })
};

module.exports = {
  createOfficer,
  getOfficers,
  getOfficer,
  updateOfficer,
  deleteOfficer
};
