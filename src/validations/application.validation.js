const Joi = require("joi");
const { objectId, postalCode, applicationCSVExtension, phoneNumber } = require("./custom.validation");

const validStatuses = [
  "Received",
  "Assigned",
  "Contacted",
  "Interview",
  "Requested Information",
  "Received Information",
  "Processing Information",
  "Committee Discussion",
  "Accepted",
  "Rejected",
  "Waitlist",
  "Contract Sent",
  "Contract Signed",
  "Active Client",
  "Updating Client Contract",
  "Archived"
];

const createApplication = {
  body: Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    phoneNumber: Joi.string().custom(phoneNumber).required(),
    address: Joi.string().required(),
    city: Joi.string().required(),
    province: Joi.string(),
    sex: Joi.string().required(),
    dateOfBirth: Joi.date().required(),
    maritalStatus: Joi.string().required(),
    citizenship: Joi.string().required(),
    preferredLanguage: Joi.string(),
    employmentStatus: Joi.string().required(),
    loanAmountRequested: Joi.number().required(),
    loanType: Joi.string().required(),
    debtCircumstances: Joi.string().required(),
    postalCode: Joi.string().custom(postalCode),
    guarantor: Joi.object({
      hasGuarantor: Joi.boolean(),
      fullName: Joi.string(),
      email: Joi.string().email(),
      phoneNumber: Joi.string()
    }),
    recommendationInfo: Joi.string().required(),
    acknowledgements: Joi.object({
      loanPurpose: Joi.boolean().required(),
      maxLoan: Joi.boolean().required(),
      repayment: Joi.boolean().required(),
      residence: Joi.boolean().required(),
      netPositiveIncome: Joi.boolean().required(),
      guarantorConsent: Joi.boolean().required()
    }),
    services: Joi.object({
      careerCoaching: Joi.boolean(),
      resumeCritique: Joi.boolean(),
      financialCoaching: Joi.boolean(),
      communityResources: Joi.boolean()
    }),
    emailOptIn: Joi.boolean(),
    status: Joi.string().valid(...validStatuses),
    officer: Joi.string().custom(objectId),
    user: Joi.string().custom(objectId),
    dateApplied: Joi.date()
  })
};

const importApplications = {
  file: Joi.object().keys({
    originalName: Joi.string().required().custom(applicationCSVExtension),
    fieldName: Joi.string().required(),
    encoding: Joi.string().required(),
    mimeType: Joi.string().required(),
    buffer: Joi.binary().required(),
    size: Joi.number()
  })
};

const getApplications = {
  query: Joi.object().keys({
    startDate: Joi.date(),
    endDate: Joi.date(),
    nameOrEmail: Joi.string(),
    status: [Joi.string().valid(...validStatuses, ""), Joi.array().items(Joi.string().valid(...validStatuses, ""))],
    displayAll: Joi.boolean(),
    officer: [Joi.string().custom(objectId), Joi.array().items(Joi.string().custom(objectId))],
    user: Joi.string().custom(objectId),
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getApplication = {
  params: Joi.object().keys({
    applicationId: Joi.required().custom(objectId)
  })
};

const updateApplication = {
  params: Joi.object().keys({
    applicationId: Joi.required().custom(objectId)
  }),
  body: Joi.object()
    .keys({
      firstName: Joi.string(),
      lastName: Joi.string(),
      email: Joi.string().email(),
      phoneNumber: Joi.string().custom(phoneNumber),
      address: Joi.string(),
      city: Joi.string(),
      province: Joi.string(),
      sex: Joi.string(),
      dateOfBirth: Joi.date(),
      maritalStatus: Joi.string(),
      citizenship: Joi.string(),
      preferredLanguage: Joi.string(),
      employmentStatus: Joi.string(),
      loanAmountRequested: Joi.number(),
      loanType: Joi.string(),
      debtCircumstances: Joi.string(),
      postalCode: Joi.string().custom(postalCode).optional().allow(""),
      guarantor: Joi.object({
        hasGuarantor: Joi.boolean(),
        fullName: Joi.string().allow(""),
        email: Joi.string().email().allow(""),
        phoneNumber: Joi.string().allow("")
      }),
      acknowledgements: Joi.object({
        loanPurpose: Joi.boolean(),
        maxLoan: Joi.boolean(),
        repayment: Joi.boolean(),
        residence: Joi.boolean(),
        netPositiveIncome: Joi.boolean(),
        guarantorConsent: Joi.boolean()
      }),
      services: Joi.object({
        careerCoaching: Joi.boolean(),
        resumeCritique: Joi.boolean(),
        financialCoaching: Joi.boolean(),
        communityResources: Joi.boolean()
      }),
      recommendationInfo: Joi.string(),
      emailOptIn: Joi.boolean(),
      status: Joi.string().valid(...validStatuses),
      officer: Joi.string().custom(objectId),
      user: Joi.string().custom(objectId)
    })
    .min(1)
};

const deleteApplication = {
  params: Joi.object().keys({
    applicationId: Joi.required().custom(objectId)
  })
};

module.exports = {
  createApplication,
  getApplications,
  getApplication,
  updateApplication,
  deleteApplication,
  importApplications
};
