const Joi = require("joi");
const { objectId } = require("./custom.validation");

const validStatuses = ["Active Client", "Updating Client Contract", "Archived"];

const createClient = {
  body: Joi.object().keys({
    dateSigned: Joi.date().required(),
    currentContract: Joi.string().custom(objectId),
    application: Joi.string().required().custom(objectId),
    officer: Joi.string().required().custom(objectId),
    status: Joi.string().valid(...validStatuses)
  })
};

const getClients = {
  query: Joi.object().keys({
    nameOrEmail: Joi.string(),
    status: [Joi.string().valid(...validStatuses, ""), Joi.array().items(Joi.string().valid(...validStatuses, ""))],
    startDate: Joi.date(),
    endDate: Joi.date(),
    application: Joi.string().custom(objectId),
    officer: [Joi.string().custom(objectId), Joi.array().items(Joi.string().custom(objectId))],
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getClient = {
  params: Joi.object().keys({
    clientId: Joi.required().custom(objectId)
  })
};

const updateClient = {
  params: Joi.object().keys({
    clientId: Joi.required().custom(objectId)
  }),
  body: Joi.object()
    .keys({
      contract: Joi.string().custom(objectId),
      officer: Joi.string().custom(objectId),
      status: Joi.string().valid(...validStatuses)
    })
    .min(1)
};

const deleteClient = {
  params: Joi.object().keys({
    clientId: Joi.required().custom(objectId)
  })
};

module.exports = {
  createClient,
  getClients,
  getClient,
  updateClient,
  deleteClient
};
