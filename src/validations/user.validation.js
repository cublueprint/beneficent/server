const Joi = require("joi");
const { password, objectId, phoneNumber } = require("./custom.validation");

const createUser = {
  body: Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    phoneNumber: Joi.string().custom(phoneNumber).allow(""),
    address: Joi.string().allow("").allow(null),
    country: Joi.string().allow("").allow(null),
    email: Joi.string().required().email(),
    password: Joi.string().custom(password).allow("").allow(null),
    role: Joi.string().valid("pendingUser", "pendingAdmin", "user", "admin"),
    isLoanOfficer: Joi.boolean().allow("")
  })
};

const getUsers = {
  query: Joi.object().keys({
    nameOrEmail: Joi.string(),
    phoneNumber: Joi.string(),
    address: Joi.string(),
    country: Joi.string(),
    role: Joi.string(),
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getUser = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId)
  })
};

const updateUser = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId)
  }),
  body: Joi.object()
    .keys({
      firstName: Joi.string(),
      lastName: Joi.string(),
      phoneNumber: Joi.string().custom(phoneNumber).allow(""),
      address: Joi.string().allow(""),
      country: Joi.string().allow(""),
      email: Joi.string().email(),
      password: Joi.string().custom(password),
      role: Joi.string().valid("pendingUser", "pendingAdmin", "user", "admin"),
      isLoanOfficer: Joi.boolean(),
      currentPassword: Joi.string().custom(password).when("password", {
        is: Joi.exist(),
        then: Joi.required(),
        otherwise: Joi.optional()
      })
    })
    .min(1)
};

const resetPassword = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId)
  }),
  body: Joi.object().keys({
    password: Joi.string().custom(password).required()
  })
};

const deleteUser = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId)
  })
};

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  resetPassword,
  deleteUser
};
