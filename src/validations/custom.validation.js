const path = require("path");
const validator = require("validator");

const objectId = (value, helpers) => {
  if (!value.match(/^[0-9a-fA-F]{24}$/)) {
    return helpers.message('"{{#label}}" must be a valid mongo id');
  }
  return value;
};

const password = (value, helpers) => {
  if (value.length < 8) {
    return helpers.message("password must be at least 8 characters");
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message("password must contain at least 1 letter and 1 number");
  }
  return value;
};

const fileExtension = (value, helpers) => {
  const validFileExts = [".pdf", ".jpeg", ".jpg", ".png"];
  const ext = path.extname(value);
  if (!validFileExts.includes(ext)) {
    return helpers.message("only .pdf, .jpeg, .jpg, and .png files are allowed");
  }
  return value;
};

const contractFileExtension = (value, helpers) => {
  const validFileExt = ".pdf";
  const ext = path.extname(value);
  if (ext !== validFileExt) {
    return helpers.message("only .pdf files are allowed");
  }
  return value;
};

const applicationCSVExtension = (value, helpers) => {
  const validFileExt = ".csv";
  const ext = path.extname(value);
  if (ext !== validFileExt) {
    return helpers.message("only .csv files are allowed");
  }
  return value;
};

const postalCode = (value, helpers) => {
  if (value !== "" && !validator.isPostalCode(value, "CA")) {
    return helpers.message("Postal code must be a valid Canadian postal code");
  }
  return value;
};

const phoneNumber = (value, helpers) => {
  if (!value.match(/^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)) {
    return helpers.message("Phone number number must be a valid phone number");
  }
  return value;
};

module.exports = {
  objectId,
  password,
  fileExtension,
  contractFileExtension,
  postalCode,
  applicationCSVExtension,
  phoneNumber
};
