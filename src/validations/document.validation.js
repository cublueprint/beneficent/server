const Joi = require("joi");
const { objectId, fileExtension } = require("./custom.validation");

const createDocument = {
  file: Joi.object().keys({
    originalName: Joi.string().required().custom(fileExtension),
    fieldName: Joi.string().required(),
    encoding: Joi.string().required(),
    mimeType: Joi.string().required(),
    buffer: Joi.binary().required(),
    size: Joi.number()
  }),
  body: Joi.object().keys({
    docType: Joi.string().valid("Identification", "Proof of Debt", "Proof of Income", "Other").required(),
    application: Joi.string().required().custom(objectId),
    uploadedBy: Joi.string().required().custom(objectId)
  })
};

const getDocuments = {
  query: Joi.object().keys({
    docType: Joi.string(),
    originalName: Joi.string(),
    application: Joi.string().custom(objectId),
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getDocumentById = {
  params: Joi.object().keys({
    documentId: Joi.required().custom(objectId)
  })
};

const updateDocument = {
  params: Joi.object().keys({
    documentId: Joi.required().custom(objectId)
  }),
  body: Joi.object().keys({
    docType: Joi.string().valid("Identification", "Proof of Debt", "Proof of Income", "Other").required(),
    updatedBy: Joi.string().required().custom(objectId)
  })
};

const deleteDocument = {
  params: Joi.object().keys({
    documentId: Joi.required().custom(objectId)
  })
};

module.exports = {
  createDocument,
  getDocuments,
  getDocumentById,
  updateDocument,
  deleteDocument
};
