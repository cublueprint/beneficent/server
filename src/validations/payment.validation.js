const Joi = require("joi");
const { objectId } = require("./custom.validation");

const getPayments = {
  query: Joi.object().keys({
    client: Joi.string().custom(objectId).required(),
    cache: Joi.boolean().default(false),
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

module.exports = {
  getPayments
};
