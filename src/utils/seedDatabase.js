const mongoose = require("mongoose");
const faker = require("faker");
const config = require("../config/config");
const { Application, Client, Contract, Officer, User, Payment } = require("../models");
const { applicationService, contractService } = require("../services");
const getRandomPhoneNumber = require("../../tests/fixtures/phoneNumber.fixture");

const validStatuses = [
  "Received",
  "Assigned",
  "Contacted",
  "Interview",
  "Requested Information",
  "Received Information",
  "Processing Information",
  "Committee Discussion",
  "Accepted",
  "Rejected",
  "Waitlist"
];

const validMaritalStatusSelections = ["Single", "Separated", "Married", "Widow/Widower", "Divorced", "Common Law"];

const validSexSelections = ["Male", "Female"];

const validProvinces = [
  "Alberta",
  "British Columbia",
  "Manitoba",
  "New Brunswick",
  "Newfoundland and Labrador",
  "Northwest Territories",
  "Nova Scotia",
  "Nunavut",
  "Ontario",
  "Prince Edward Island",
  "Quebec",
  "Saskatchewan",
  "Yukon"
];

const validEmploymentStatusSelections = ["Employed Full Time", "Employed Part Time", "Seasonal/Contract", "Unemployed"];

const validPreferredLanguagesSelections = ["English", "French", "Arabic", "Other"];

const validCitizenshipSelections = ["Canadian", "Permanent Resident", "Other"];

const date1 = new Date("2001-01-21");
const date2 = new Date("2002-01-21");
const date3 = new Date("2001-01-01");

const seedDatabase = async () => {
  await truncateDatabase();
  await createUsers();
  await createOfficersAndAdmin();
  await createApplications();
};

const truncateDatabase = async () => {
  await Application.deleteMany();
  await Client.deleteMany();
  await Contract.deleteMany();
  await Officer.deleteMany();
  await User.deleteMany();
};

const createUsers = async () => {
  await User.create({
    firstName: "fake first name",
    lastName: "fake last name",
    phoneNumber: "+16132332892",
    address: "'123 Melon Street, Ottawa, K1L6L3'",
    country: "Canada",
    email: "fake@example.com",
    password: "password1"
  });
  listOfUsers = [];
  for (let i = 0; i < 8; i++) {
    const currentUserInfo = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      phoneNumber: getRandomPhoneNumber(),
      address: faker.address.streetAddress(),
      country: faker.address.country(),
      email: faker.internet.email(),
      password: "dh3298y32hon32"
    };
    listOfUsers.push(currentUserInfo);
  }
  await User.create(listOfUsers);
};

const createOfficersAndAdmin = async () => {
  const users = await User.find();
  for (let i = 0; i < 3; i++) {
    const toUpdate = await User.findById(users[i]._id);
    if (i === 0) {
      toUpdate.role = "admin";
      await toUpdate.save();
    } else {
      toUpdate.role = "user";
      await toUpdate.save();
      await Officer.create([{ user: toUpdate._id }]);
    }
  }
};

const generateFakePostalCode = () => {
  const letters1 = "ABCEGHJKLMNPRSTVXYabceghjklmnprstvyx";
  const letters2 = "ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwyxz";
  const numbers = "1234567890";

  let postalCode = "";

  postalCode += letters1.charAt(Math.floor(Math.random() * letters1.length));
  for (let i = 1; i < 6; i++) {
    if (i % 2 == 0) {
      postalCode += letters2.charAt(Math.floor(Math.random() * letters2.length));
    } else {
      postalCode += numbers.charAt(Math.floor(Math.random() * numbers.length));
    }
  }

  return postalCode;
};

const createApplications = async () => {
  const users = await User.find();
  const officer1 = await Officer.findOne({ user: users[1]._id });
  const officer2 = await Officer.findOne({ user: users[2]._id });

  for (let i = 3; i < users.length; i++) {
    const newApplication = {
      firstName: users[i].firstName,
      lastName: users[i].lastName,
      email: users[i].email,
      phoneNumber: users[i].phoneNumber,
      address: users[i].address,
      city: faker.address.cityName(),
      province: validProvinces[Math.floor(Math.random() * validProvinces.length)],
      sex: validSexSelections[Math.floor(Math.random() * validSexSelections.length)],
      dateOfBirth: date2,
      maritalStatus: validMaritalStatusSelections[Math.floor(Math.random() * validMaritalStatusSelections.length)],
      citizenship: validCitizenshipSelections[Math.floor(Math.random() * validCitizenshipSelections.length)],
      preferredLanguage:
        validPreferredLanguagesSelections[Math.floor(Math.random() * validPreferredLanguagesSelections.length)],
      employmentStatus: validEmploymentStatusSelections[Math.floor(Math.random() * validEmploymentStatusSelections.length)],
      loanAmountRequested: faker.datatype.number(),
      loanType: "Other",
      debtCircumstances: faker.lorem.words(),
      recommendationInfo: faker.lorem.words(),
      postalCode: generateFakePostalCode(),
      acknowledgements: {
        loanPurpose: faker.datatype.boolean(),
        maxLoan: faker.datatype.boolean(),
        repayment: faker.datatype.boolean(),
        residence: faker.datatype.boolean(),
        netPositiveIncome: faker.datatype.boolean(),
        guarantorConsent: faker.datatype.boolean()
      },
      services: {
        careerCoaching: faker.datatype.boolean(),
        resumeCritique: faker.datatype.boolean(),
        financialCoaching: faker.datatype.boolean(),
        communityResources: faker.datatype.boolean()
      },
      emailOptIn: faker.datatype.boolean(),
      status: validStatuses[Math.floor(Math.random() * validStatuses.length)],
      officer: i < 6 ? officer1._id : officer2._id,
      guarantor: {
        hasGuarantor: true,
        fullName: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        phoneNumber: getRandomPhoneNumber()
      },
      dateApplied: Date.now(),
      user: users[i]._id
    };
    await applicationService.createApplication(newApplication);
  }
};

mongoose.connect(config.mongoose.url, config.mongoose.options).then(async (db) => {
  await seedDatabase();
  db.disconnect();
});

module.exports = seedDatabase;
