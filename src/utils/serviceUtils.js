const ApiError = require("./ApiError");
const { User, Application, Officer, Contract } = require("../models");
const httpStatus = require("http-status");

/**
 * Validates id from user collection
 * @param {ObjectId} userId - The user id
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if the ID does not exist in the mongoDB database
 * @returns void
 */
const validateUserId = async (userId, errorMessage) => {
  const user = await User.findById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, errorMessage);
  }
};

/**
 * Validates id from officer collection
 * @param {ObjectId} officerId - The officer id
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if the ID does not exist in the mongoDB database
 * @returns void
 */
const validateOfficerId = async (officerId, errorMessage) => {
  const user = await Officer.findById(officerId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, errorMessage);
  }
};

/**
 * Validates id from application collection
 * @param {ObjectId} applicationId - The application id
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if the ID does not exist in the mongoDB database
 * @returns void
 */
const validateApplicationId = async (applicationId, errorMessage) => {
  const app = await Application.findById(applicationId);
  if (!app) {
    throw new ApiError(httpStatus.NOT_FOUND, errorMessage);
  }
};

/**
 * Validates id from contract collection
 * @param {ObjectId} contractId - The contract id
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if the ID does not exist in the mongoDB database
 * @returns void
 */
const validateContractId = async (contractId, errorMessage) => {
  const contract = await Contract.findById(contractId);
  if (!contract) {
    throw new ApiError(httpStatus.NOT_FOUND, errorMessage);
  }
};

module.exports = {
  validateUserId,
  validateOfficerId,
  validateApplicationId,
  validateContractId
};
