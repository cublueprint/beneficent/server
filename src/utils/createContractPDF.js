const { PDFDocument, StandardFonts, rgb } = require("pdf-lib");
const moment = require("moment");

let dateFieldsCount = 0;

const paragraphTextOptions = {
  size: 11,
  opacity: 0.7
};

/**
 * Creates a PDF document using Contract information and saves it to the filesystem
 * @param {Contract} contract - Contract object
 * @returns {Buffer} pdfBuffer - Buffer from byte array of serialized generated pdf doc
 */
async function createContractPDF(contract) {
  // Create a new PDFDocument
  const pdfDoc = await PDFDocument.create();

  // Get the form so we can add fields to it
  const form = pdfDoc.getForm();

  // Embed the Helvetica fonts
  const Helvetica = await pdfDoc.embedFont(StandardFonts.Helvetica);
  const HelveticaBold = await pdfDoc.embedFont(StandardFonts.HelveticaBold);

  // Add a blank page to the document
  const page = pdfDoc.addPage();

  // Add Loan information title to beginning of page 1
  page.drawText("Loan information", {
    x: 40,
    y: 780,
    size: 18,
    font: HelveticaBold
  });

  page.drawLine({
    start: { x: 35, y: 760 },
    end: { x: 560, y: 760 },
    thickness: 1,
    opacity: 0.75
  });

  page.drawText("Office use only", {
    x: 40,
    y: 730,
    size: 14,
    font: HelveticaBold
  });

  // Add Assigned loan officer field
  page.drawText("Assigned loan officer", {
    x: 45,
    y: 700,
    size: 12,
    font: HelveticaBold
  });

  const assignedLoanOfficerField = form.createTextField("assigned.loan.officer");
  assignedLoanOfficerField.setText(
    contract.application.officer.user.firstName + " " + contract.application.officer.user.lastName
  );
  assignedLoanOfficerField.addToPage(page, {
    x: 45,
    y: 678,
    width: 300,
    height: 17
  });

  // Add Loan details subheading
  page.drawText("Loan details", {
    x: 45,
    y: 640,
    size: 12,
    font: HelveticaBold
  });

  // Add Approved loan amount field
  page.drawText("Approved loan amount", {
    x: 50,
    y: 602,
    size: 11,
    font: HelveticaBold,
    opacity: 0.7
  });

  const loanAmountField = form.createTextField("approved.loan.amount");
  loanAmountField.setText(`$${contract.approvedLoanAmount.toString()}`);
  loanAmountField.addToPage(page, {
    x: 445,
    y: 602,
    width: 100,
    height: 17
  });

  // Add Monthly installment amount field
  page.drawText("Monthly installment", {
    x: 50,
    y: 575,
    size: 11,
    font: HelveticaBold,
    opacity: 0.7
  });

  const monthlyInstallmentField = form.createTextField("monthly.installment");
  monthlyInstallmentField.setText(`$${contract.monthlyPayment.toString()}`);
  monthlyInstallmentField.addToPage(page, {
    x: 445,
    y: 575,
    width: 100,
    height: 17
  });

  // Add Final installment amount field
  page.drawText("Final installment (if applicable)", {
    x: 50,
    y: 548,
    size: 11,
    font: HelveticaBold,
    opacity: 0.7
  });

  const finalInstallmentField = form.createTextField("final.installment");
  finalInstallmentField.setText(`$${contract.finalPayment.toString()}`);
  finalInstallmentField.addToPage(page, {
    x: 445,
    y: 548,
    width: 100,
    height: 17
  });

  // Add First installment due date fields and display date from contract
  addDateFields(page, form, HelveticaBold, "First installment due", contract.firstPaymentDue, 45, 480);

  // Add Last installement due date fields and display date from contract
  addDateFields(page, form, HelveticaBold, "Last installment due", contract.finalPaymentDue, 250, 480);

  // Draw seperating line
  page.drawLine({
    start: { x: 35, y: 430 },
    end: { x: 560, y: 430 },
    thickness: 1,
    opacity: 0.75
  });

  // Add Payment details heading
  page.drawText("Payment details", {
    x: 40,
    y: 390,
    size: 18,
    font: HelveticaBold
  });

  page.drawText("We will use these details to register your debt as a payee and directly settle your debt for you", {
    x: 50,
    y: 360,
    size: 11,
    font: Helvetica,
    opacity: 0.7
  });

  // Add Type of debt radio group
  const typeOfDebtField = form.createRadioGroup("type.of.debt");
  typeOfDebtField.addOptionToPage("Credit card", page, { x: 50, y: 300, width: 10, height: 10 });
  typeOfDebtField.addOptionToPage("Bank loan", page, { x: 50, y: 280, width: 10, height: 10 });
  typeOfDebtField.addOptionToPage("Outstanding bill", page, { x: 50, y: 260, width: 10, height: 10 });
  typeOfDebtField.addOptionToPage("Other", page, { x: 50, y: 240, width: 10, height: 10 });

  page.drawText("Type of debt", {
    x: 50,
    y: 320,
    size: 12,
    font: HelveticaBold
  });

  page.drawText("Credit card", {
    x: 65,
    y: 300,
    size: 11,
    font: Helvetica,
    opacity: 0.7
  });

  page.drawText("Bank loan", {
    x: 65,
    y: 280,
    size: 11,
    font: Helvetica,
    opacity: 0.7
  });

  page.drawText("Outstanding bill", {
    x: 65,
    y: 260,
    size: 11,
    font: Helvetica,
    opacity: 0.7
  });

  const otherDebtField = form.createTextField("other.type.of.debt");
  otherDebtField.addToPage(page, {
    x: 65,
    y: 240,
    width: 100,
    height: 15
  });

  // Add Institution name/payee field
  page.drawText("Institution name or payee", {
    x: 50,
    y: 190,
    size: 12,
    font: HelveticaBold
  });

  const institutionNameOrPayeeField = form.createTextField("institution.name.or.payee");
  institutionNameOrPayeeField.addToPage(page, {
    x: 50,
    y: 170,
    width: 120,
    height: 15
  });

  page.drawText('E.g. "TD VISA" if credit card', {
    x: 50,
    y: 150,
    size: 9,
    opacity: 0.7
  });

  // Add Account number field
  page.drawText("Account number", {
    x: 250,
    y: 190,
    size: 12,
    font: HelveticaBold
  });

  const accountNumberField = form.createTextField("account.number");
  accountNumberField.addToPage(page, {
    x: 250,
    y: 170,
    width: 120,
    height: 15
  });

  // Add Expiry date fields
  addDateFields(page, form, HelveticaBold, "Expiry (if applicable)", null, 400, 170);

  // Add note to the end of page 2
  page.drawText(
    "Note: if we cannot register your debt as a payee, we will use an EMT to transfer the funds to you as a last",
    {
      x: 50,
      y: 100,
      size: 11,
      opacity: 0.8
    }
  );
  page.drawText("resort. Once the transfer is complete, please send a receipt to Benecient illustrating your debt payment", {
    x: 50,
    y: 85,
    size: 11,
    opacity: 0.8
  });

  // Add a second blank page to the document
  const page2 = pdfDoc.addPage();

  // Add Loan contract title to the beginning of page 2
  page2.drawText("Loan contract", {
    x: 40,
    y: 780,
    size: 18,
    font: HelveticaBold
  });

  // Add red note to the beginning of page 2
  page2.drawText(
    "Note: please do not fill out or sign this page until the loan has been approved and signed by a loan officer.",
    {
      x: 45,
      y: 750,
      size: 11,
      color: rgb(1, 0, 0)
    }
  );

  // Add Contract date fields and display date from contract
  addDateFields(page2, form, HelveticaBold, "Contract date", contract.contractStartDate, 45, 690);

  page2.drawText(`The terms of this agreement are between: \n"Beneficent" (Lender)\nAND\nThe Borrower (details below)`, {
    x: 45,
    y: 630,
    lineHeight: 15,
    ...paragraphTextOptions
  });

  // Add IN CONSIDERATION OF paragraph (line-by-line because)
  page2.drawText("IN CONSIDERATION OF", {
    x: 45,
    y: 550,
    font: HelveticaBold,
    ...paragraphTextOptions
  });
  page2.drawText('the lender lending certain monies (the "loan") to the', {
    x: 173,
    y: 550,
    ...paragraphTextOptions
  });
  page2.drawText(
    "borrower, and the borrower repaying the loan to the lender, both parties agree to keep, perform, and fulfill",
    {
      x: 45,
      y: 535,
      ...paragraphTextOptions
    }
  );
  page2.drawText("the promises and terms stipulated in this agreement.", {
    x: 45,
    y: 520,
    ...paragraphTextOptions
  });

  // Add Loan Amount heading and paragraph
  addParagraph(
    page2,
    "Loan Amount",
    [
      "The lender promises to loan the agreed upon amount (see below) to the borrower and the borrower",
      "promises to repay this principle amount to the lender, without interest payable on the unpaid principal."
    ],
    HelveticaBold,
    45,
    490
  );

  // Add Repayment heading and paragraph
  addParagraph(
    page2,
    "Repayment",
    [
      "This loan will  be repaid in full by the agreed upon date, commencing on the agreed upon date of first",
      "payment, and in lump sums of the agreed upon amount per month."
    ],
    HelveticaBold,
    45,
    430
  );

  // Add Default heading and paragraph
  addParagraph(
    page2,
    "Default",
    [
      "Notwithstanding anything to the contrary in this agreement, if the borrower is unable to meet any of their",
      "obligations under this agreement, then the lender may declare the principal amount owed under this",
      "agreement to be immediately due and payable by the guarantor listed below."
    ],
    HelveticaBold,
    45,
    370
  );

  // Add Confedentiality heading and paragraph
  addParagraph(
    page2,
    "Confedentiality",
    [
      "It is understood and agreed to that the below information may provide certain",
      "information that is and must be kept confidential, wherefore the parties acknowledge",
      "that they have read and understand this Agreement and voluntarily accept the duties",
      "and obligations set forth herein. To ensure the protection of such information, it is",
      "agreed that this information will remain confidential with the exception of cases where witholding this",
      "information is an obstruction of justice as carried out by the Canadian justice system."
    ],
    HelveticaBold,
    45,
    300
  );

  // Add name and signature fields to the bottom of page 2
  addNameAndSignatureFields(
    page2,
    form,
    HelveticaBold,
    "Borrower",
    contract.application.firstName + " " + contract.application.lastName,
    45,
    170
  );
  addNameAndSignatureFields(page2, form, HelveticaBold, "Guarantor", contract.application.guarantor.fullName, 210, 170);
  addNameAndSignatureFields(
    page2,
    form,
    HelveticaBold,
    "Loan officer",
    contract.application.officer.user.firstName + " " + contract.application.officer.user.lastName,
    375,
    170
  );

  var pdfBytes = await pdfDoc.save(); // Serialize pdf to array of bytes
  var pdfBuffer = Buffer.from(pdfBytes.buffer, "binary"); // Create PDF file buffer from byte array
  return pdfBuffer; // Return pdf file buffer
}

/**
 * Creates a name field and signature space and adds them to a page
 * @param {Object} page - PDFDocument page
 * @param {Object} form - PDFDocument form
 * @param {Object} font - PDFDocument font
 * @param {String} personType - Borrower/Guarantor/Loan officer
 * @param {String} personType - Name of Borrower/Guarantor/Loan officer
 * @param {Number} x - X coordinate to start text
 * @param {Number} y - Y coordinate to start text
 */
const addNameAndSignatureFields = (page, form, font, personType, personName, x, y) => {
  page.drawText(`${personType} name`, {
    x,
    y,
    size: 12,
    font
  });
  const nameField = form.createTextField(`${personType}.name`);
  nameField.setText(personName);
  nameField.addToPage(page, {
    x,
    y: y - 20,
    width: 150,
    height: 15
  });
  page.drawText(`${personType} signature`, {
    x: x - 5,
    y: y - 60,
    size: 12,
    font
  });
  page.drawLine({
    start: { x: x - 5, y: y - 120 },
    end: { x: x + 150, y: y - 120 },
    thickness: 2
  });
};

/**
 * Creates a paragraph with a heading and adds it to a page
 * @param {Object} page - PDFDocument page
 * @param {String} heading - Heading of the paragraph
 * @param {Array<String>} lines - Array of paragraph lines
 * @param {Object} font - PDFDocument font
 * @param {Number} x - X coordinate to start paragraph
 * @param {Number} y - Y coordinate to start paragraph
 */
const addParagraph = (page, heading, lines, font, x, y) => {
  page.drawText(heading, {
    x,
    y,
    font,
    ...paragraphTextOptions
  });
  lines.forEach((line) => {
    y -= 15;
    page.drawText(line, {
      x,
      y,
      ...paragraphTextOptions
    });
  });
};

/**
 * Creates month, day and year date fields and adds them to a page
 * @param {Object} page - PDFDocument page
 * @param {Object} form - PDFDocument form
 * @param {Object} font - PDFDocument font
 * @param {String} title - Title of the date
 * @param {Object} date - Date to be displayed
 * @param {Number} x - X coordinate to start date fields
 * @param {Number} y - Y coordinate to start date fields
 */
const addDateFields = (page, form, font, title, date, x, y) => {
  const utcDate = moment.utc(date);
  page.drawText(title, {
    x,
    y: y + 20,
    size: 12,
    font
  });
  const monthField = form.createTextField(`expiry.month.${dateFieldsCount++}`);
  monthField.setText(utcDate.format("MM") !== "Invalid date" ? utcDate.format("MM") : "N/A");
  monthField.addToPage(page, {
    x,
    y,
    width: 35,
    height: 15
  });
  const dayField = form.createTextField(`expiry.day.${dateFieldsCount++}`);
  dayField.setText(utcDate.format("DD") !== "Invalid date" ? utcDate.format("DD") : "N/A");
  dayField.addToPage(page, {
    x: x + 40,
    y,
    width: 35,
    height: 15
  });
  const yearField = form.createTextField(`expiry.year.${dateFieldsCount++}`);
  yearField.setText(utcDate.format("YYYY") !== "Invalid date" ? utcDate.format("YYYY") : "N/A");
  yearField.addToPage(page, {
    x: x + 80,
    y,
    width: 45,
    height: 15
  });
  page.drawText("Month       Day         Year", {
    x,
    y: y - 18,
    size: 9,
    opacity: 0.7
  });
};

module.exports = createContractPDF;
