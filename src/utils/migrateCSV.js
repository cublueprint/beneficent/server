const csv = require("csv-parser");
const validator = require("validator");
const config = require("../config/config");
const logger = require("../config/logger");
const fetch = require("node-fetch");
const { Readable } = require("stream");
const { capitalize } = require("lodash");

const url = `${config.serverUrl}/api/v1`;

const mapping = {
  "Email Opt-In": "emailOptIn",
  "Do you have a guarantor?": "guarantor",
  Phone: "phoneNumber",
  "How did you hear about Beneficent?": "recommendationInfo"
};

/**
 * returns the camel case version of the word ex: First Name --> firstName
 * @param {string} word word to convert to camel case
 * @returns {string} camel case word
 */
const convertToCamelCase = (word) => {
  const currentWordArray = word.toLowerCase().split(" ");
  let camelCaseWord = currentWordArray[0];
  currentWordArray.slice(1).map((word) => {
    camelCaseWord += word.charAt(0).toUpperCase() + word.slice(1);
  });
  return camelCaseWord;
};

/**
 * returns the capitalized version of the word ex: first name --> First Name
 * @param {string} word word to convert to camel case
 * @returns {string} capitalized word
 */
const convertToCapitalized = (word) => {
  const currentWordArray = word.split(" ");
  let capitalizeWord = "";
  currentWordArray.map((word) => {
    capitalizeWord += capitalize(word) + " ";
  });
  return capitalizeWord.trim();
};

/**
 * maps benefecient header to what we currently store as
 * @param {string} header word to convert to camel case
 * @returns {string} new headername
 */
const mapHeader = (header) => {
  if (header in mapping) {
    return mapping[header];
  } else {
    return convertToCamelCase(header);
  }
};

/**
 * formats the phone number into xxx-xxx-xxxx
 * @param {string} phoneNumber phoneNumber to format
 * @returns {string} formatted phone number
 */
const formatPhoneNumber = (phoneNumber) => {
  //get last 10 digits (ignore +1 etc)
  const number = phoneNumber.replace(/\)|\(|\+|\-|\s/gm, "").substr(-10);
  return number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
};

/**
 * strips dashes from word and returns camel case format
 * @param {string} word word to strip dashes from
 * @returns {string} formatted word
 */
const stripDashes = (word) => convertToCapitalized(word.replace(/-/g, " "));

/**
 * converts string number/currency to a numeric value
 * @param {string} stringNumber string to convert
 * @returns {number} numeric value if successful, NaN otherwise
 */
const formatNumber = (stringNumber) => {
  const int = parseInt(stringNumber.replace(/[^0-9\.]+/g, ""));
  if (isNaN(int)) {
    return 0;
  } else {
    return int;
  }
};

/**
 * converts date from format DD/MM/YYYY to MM/DD/YYYY
 * @param {String} data date to convert
 * @returns {string} formatted date, and 01/01/0001 otherwise
 */
const formatDate = (date) => {
  try {
    //format  of date is DD/MM/YYYY
    const splitDate = date.split("/");

    const formattedDate = new Date(
      parseInt(splitDate[2]),
      parseInt(splitDate[1]) - 1,
      parseInt(splitDate[0])
    ).toLocaleDateString();

    if (formattedDate === "Invalid Date") {
      return new Date(date).toISOString();
    }
    return formattedDate;
  } catch (e) {
    return date; //will throw error appropriately
  }
};

/**
 * converts string to boolean
 * @param {string} data data to convert
 * @returns {boolean} boolean value, true if yes or 1, false otherwise
 */
const formatBoolean = (data) => {
  if (data === "yes" || data === "1") return true;
  return false;
};

/**
 * formats acknowledgements as defined in the database
 * @param {String} data data to format
 * @returns {Object} object matching the stored version on the database
 */
const formatAcknowledgements = (data) => {
  const loanMapping = {
    "loan-purpose-is-for-credit-card-emergency-or-similar-high-interest-bearing-debt": "loanPurpose",
    "maximum-loan-value-of-usd4-000": "maxLoan",
    "repayment-within-12-months": "repayment",
    "residence-in-canada": "residence",
    "positive-net-income": "netPositiveIncome",
    "guarantor-consent": "guarantorConsent"
  };
  const acknowledgementsObject = {
    loanPurpose: false,
    repayment: false,
    maxLoan: false,
    residence: false,
    netPositiveIncome: false,
    guarantorConsent: false
  };
  const acknowledgements = data.split(",");
  for (const acknowledgement of acknowledgements) {
    if (acknowledgement in loanMapping) {
      acknowledgementsObject[loanMapping[acknowledgement]] = true;
    }
  }
  return acknowledgementsObject;
};

/**
 * formats loan type proeprly
 * @param {*} data data to format
 * @returns {String} string with formatted loan type
 */
const formatLoanType = (data) => {
  const loanTypeMap = {
    "credit-card": "Credit",
    emergency: "Emergency",
    other: "Other"
  };
  return loanTypeMap[data];
};

/**
 * formats guarantor as defined in the database
 * @param {String} data data to format
 * @returns {Object} object matching the stored version on the database
 */
const formatGuarantor = (data) => {
  const guarantorObject = {};
  guarantorObject.hasGuarantor = formatBoolean(data);
  return guarantorObject;
};

/**
 * formats martial status property
 * @param {*} data data to format
 * @returns string with formatted marital status
 */
const formatMaritalStatus = (data) => {
  let status = capitalize(data);
  if (status === "Widow-widower") {
    status = "Widow/Widower";
  }
  return status;
};

/**
 * formats province property
 * @param {*} data data to format
 * @returns string with formatted marital status
 */
const formatProvince = (data) => {
  const provinceMap = {
    AB: "Alberta",
    BC: "British Columbia",
    MB: "Manitoba",
    NB: "New Brunswick",
    NL: "Newfoundland and Labrador",
    NT: "Northwest Territories",
    NS: "Nova Scotia",
    NV: "Nunavut",
    ON: "Ontario",
    PE: "Prince Edward Island",
    QC: "Quebec",
    SK: "Saskatchewan",
    YK: "Yukon"
  };
  const provinceCode = data.toUpperCase();
  return provinceMap[provinceCode];
};
/**
 * santizes data from beneficent to match what will be stored on the database
 * @param {string} key - key of the object to sanitize
 * @param {string} data - date associated with the key to sanitize
 * @returns {string | number} - appropriately santized data
 */
const sanitizeData = (key, data) => {
  const formatMapping = {
    address: "uppercase",
    city: "uppercase",
    province: "province",
    sex: "capitalize",
    maritalStatus: "maritalStatus",
    preferredLanguage: "capitalize",
    citizenship: "stripDashes",
    phoneNumber: "phoneNumber",
    dateOfBirth: "date",
    dateSubmitted: "date",
    loanAmountRequested: "number",
    loanType: "loanType",
    employmentStatus: "stripDashes",
    guarantor: "guarantor",
    emailOptIn: "boolean",
    acknowledgements: "acknowledgements"
  };
  //trim & get rid of new lines
  data = data.trim().replace(/(\r\n|\n|\r)/gm, "");
  if (!(key in formatMapping)) {
    if (data === "") {
      return "Data not provided.";
    }
    //do not edit first or last name as there is no set rule
    if (key === "firstName" || key === "lastName") {
      return data;
    }
    return data.toLowerCase();
  }
  const format = formatMapping[key];
  switch (format) {
    case "uppercase":
      if (data === "") {
        return "Data not provided.";
      }
      return data.toUpperCase();

    case "capitalize":
      return convertToCapitalized(data);

    case "date":
      return formatDate(data);

    case "phoneNumber":
      return formatPhoneNumber(data);

    case "number":
      return formatNumber(data);

    case "boolean":
      return formatBoolean(data);

    case "stripDashes":
      return stripDashes(data);

    case "acknowledgements":
      return formatAcknowledgements(data);
    case "loanType":
      return formatLoanType(data);

    case "maritalStatus":
      return formatMaritalStatus(data);

    case "province":
      return formatProvince(data);

    case "guarantor":
      return formatGuarantor(data);

    default:
      return data;
  }
};

/**
 * reads the CSV file and migrates data
 * @param {string} filePath - path to where the file is stored
 * @returns {Promise<Object>} - objects with sanitized data and array of validation warnings
 */
const readCSVFile = (file) =>
  new Promise((resolve) => {
    const pastApplications = [];
    const warnings = [];
    try {
      const readable = Readable.from(file);
      readable
        .pipe(csv({ mapHeaders: ({ header, index }) => mapHeader(header) }))
        .on("data", (data) => {
          if (data.email || data.phoneNumber) {
            //if application doesn't have a phone or email, no way to contact
            const santizedData = {};
            for (const [key, value] of Object.entries(data)) {
              santizedData[key] = sanitizeData(key, value);
            }
            //if not canadian number or invalid email, then we do not add this data
            if (validator.isEmail(santizedData.email)) {
              pastApplications.push(santizedData);
            } else {
              warnings.push({
                message: `Failed to import application for: ${santizedData.firstName} ${santizedData.lastName}`,
                error: `Invalid email: ${santizedData.email}`
              });
            }
          }
        })
        .on("finish", () => {
          const listOfApplications = [];
          for (const application of pastApplications) {
            if (application.preferredLanguage == "") {
              delete application.preferredLanguage;
            }
            listOfApplications.push({
              firstName: application.firstName,
              lastName: application.lastName,
              email: application.email,
              phoneNumber: application.phoneNumber,
              address: application.address,
              city: application.city,
              province: application.province,
              sex: application.sex,
              dateOfBirth: application.dateOfBirth,
              maritalStatus: application.maritalStatus,
              citizenship: application.citizenship,
              employmentStatus: application.employmentStatus,
              loanAmountRequested: application.loanAmountRequested,
              loanType: application.loanType,
              debtCircumstances: application.debtCircumstances,
              preferredLanguage: application.preferredLanguage,
              emailOptIn: application.emailOptIn,
              guarantor: application.guarantor,
              recommendationInfo: application.recommendationInfo,
              status: "Received",
              acknowledgements: application.acknowledgements,
              dateApplied: application.dateSubmitted
            });
          }
          resolve({ applications: listOfApplications, warnings: warnings });
        });
    } catch (e) {
      logger.error(`Error parsing CSV file: ${e}`);
      throw new Error(e);
    }
  });

/**
 * gets data from CSV files and adds to database using applications API
 * @param {Buffer} file
 * @param {String} token
 * @returns {Object} - Object of successfully imported applications and array of failed import warnings
 */
const addDataToDatabase = async (file, token) => {
  const createdApps = [];
  let warningResult = [];
  const applicationAPI = url + "/applications";
  try {
    const { applications, warnings } = await readCSVFile(file.buffer);
    if (warnings.length > 0) {
      warningResult = warnings;
    }

    for (let i = 0; i < applications.length; i++) {
      // set timeout around the fetch() call, return fetch() response as a promise
      const appResponse = await new Promise((resolve) => {
        setTimeout(async () => {
          const resp = await fetch(applicationAPI, {
            method: "post",
            body: JSON.stringify(applications[i]),
            headers: { "Content-Type": "application/json", Authorization: token }
          });
          resolve(resp);
        }, 1000);
      });

      const createdApp = await appResponse.json();
      if (createdApp.code) {
        warningResult.push({
          message: `Failed to import application for: ${applications[i].firstName} ${applications[i].lastName}`,
          error: createdApp.message
        });
      } else {
        createdApps.push(createdApp);
      }
    }
    logger.info("Migration was successful.");
  } catch (error) {
    logger.error(`Error migrating: ${error}`);
    process.exit(1);
  }
  return { createdApps: createdApps, warnings: warningResult };
};

module.exports = {
  addDataToDatabase,
  readCSVFile
};
