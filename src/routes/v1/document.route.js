const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { documentValidation } = require("../../validations");
const { documentController } = require("../../controllers");
const { upload } = require("../../middlewares/upload");

const router = express.Router();

router
  .route("/")
  .post(auth("manageDocuments"), upload, validate(documentValidation.createDocument), documentController.createDocument)
  .get(auth("manageDocuments"), validate(documentValidation.getDocuments), documentController.getDocuments);

router
  .route("/:documentId")
  .patch(auth("manageDocuments"), validate(documentValidation.updateDocument), documentController.updateDocument)
  .delete(auth("manageDocuments"), validate(documentValidation.deleteDocument), documentController.deleteDocument);

router
  .route("/:documentId/download")
  .get(auth("manageDocuments"), validate(documentValidation.getDocumentById), documentController.getDocumentById);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Documents
 *   description: Document management and retrieval
 */

/**
 * @swagger
 *  /documents:
 *    post:
 *      summary: Upload a document
 *      description: Only admins can upload documents.
 *      tags: [Documents]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              type: object
 *              required:
 *                - file
 *                - docType
 *                - application
 *                - uploadedBy
 *              properties:
 *                file:
 *                  type: string
 *                  format: binary
 *                docType:
 *                  type: string
 *                  enum: [Identification, Proof of Debt, Proof of Income, Other]
 *                application:
 *                  type: string
 *                uploadedBy:
 *                  type: string
 *              example:
 *                file: fakeDocument.jpeg
 *                docType: Identification
 *                application: 4def40c86762e0fb12000456
 *                user: 4def40c86762e0fb13000452
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Document'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *    get:
 *      summary: Query documents
 *      description: Only admins can query documents.
 *      tags: [Documents]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: docType
 *          schema:
 *            type: string
 *          description: The type of document
 *        - in: query
 *          name: originalName
 *          schema:
 *            type: string
 *          description: The original file name of the document
 *        - in: query
 *          name: application
 *          schema:
 *            type: string
 *          description: The application ID of the document
 *        - in: query
 *          name: populate
 *          schema:
 *            type: string
 *          description: Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,) (ex. user)
 *        - in: query
 *          name: sortBy
 *          schema:
 *            type: string
 *          description: Sort by query in the form of field:desc/asc (ex. docType:asc)
 *        - in: query
 *          name: limit
 *          schema:
 *            type: integer
 *            minimum: 1
 *          default: 10
 *          description: Maximum number of documents
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *            minimum: 1
 *            default: 1
 *          description: Page number
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Document'
 *                  page:
 *                    type: integer
 *                    example: 1
 *                  limit:
 *                    type: integer
 *                    example: 10
 *                  totalPages:
 *                    type: integer
 *                    example: 1
 *                  totalResults:
 *                    type: integer
 *                    example: 1
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /documents/{id}/download:
 *    get:
 *      summary: Download a document
 *      description: Only admins can download documents.
 *      tags: [Documents]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Document id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/octet-stream:
 *              schema:
 *                 type: string
 *                 format: binary
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /documents/{id}:
 *    patch:
 *      summary: Update a document
 *      description: Only admins can update documents.
 *      tags: [Documents]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - docType
 *                - updatedBy
 *              properties:
 *                docType:
 *                  type: string
 *                  enum: [Identification, Proof of Debt, Proof of Income, Other]
 *                updatedBy:
 *                  type: string
 *              example:
 *                docType: Identification
 *                updatedBy: 4def40c86762e0fb13000452
 *      responses:
 *        "201":
 *          description: Updated
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Document'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /documents/{id}:
 *    delete:
 *      summary: Delete a document
 *      description: Only admins can delete documents
 *      tags: [Documents]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Document id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
