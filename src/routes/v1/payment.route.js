const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { paymentValidation } = require("../../validations");
const { paymentController } = require("../../controllers");

const router = express.Router();

router.route("/").get(auth("getPayments"), validate(paymentValidation.getPayments), paymentController.getPayments);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Payments
 *   description: Payment management and retrieval
 */

/**
 * @swagger
 *  /payments:
 *    get:
 *      summary: Get all payments
 *      description: Only admins can retrieve all payments.
 *      tags: [Payments]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: client
 *          required: true
 *          schema:
 *            type: string
 *        - in: query
 *          name: cache
 *          schema:
 *            type: boolean
 *          default: false
 *          description: Client ID
 *        - in: query
 *          name: populate
 *          schema:
 *            type: string
 *          description: Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,) (ex. user)
 *        - in: query
 *          name: sortBy
 *          schema:
 *            type: string
 *          description: Sort by query in the form of field:desc/asc (ex. name:asc)
 *        - in: query
 *          name: limit
 *          schema:
 *            type: integer
 *            minimum: 1
 *          default: 10
 *          description: Maximum number of payments
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *            minimum: 1
 *            default: 1
 *          description: Page number
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Payment'
 *                  page:
 *                    type: integer
 *                    example: 1
 *                  limit:
 *                    type: integer
 *                    example: 10
 *                  totalPages:
 *                    type: integer
 *                    example: 1
 *                  totalResults:
 *                    type: integer
 *                    example: 1
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
