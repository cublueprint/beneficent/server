const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { upload } = require("../../middlewares/upload");
const { contractValidation } = require("../../validations");
const { contractController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .post(auth("manageContracts"), validate(contractValidation.createContract), contractController.createContract)
  .get(auth("getContracts"), validate(contractValidation.getContracts), contractController.getContracts);

router
  .route("/:contractId")
  .get(auth("getContracts"), validate(contractValidation.getContract), contractController.getContract)
  .patch(auth("manageContracts"), validate(contractValidation.updateContract), contractController.updateContract)
  .delete(auth("manageContracts"), validate(contractValidation.deleteContract), contractController.deleteContract);

router
  .route("/:contractId/upload")
  .post(auth("manageContracts"), upload, validate(contractValidation.uploadContract), contractController.uploadContract);

router
  .route("/:contractId/download")
  .get(auth("manageContracts"), validate(contractValidation.downloadContract), contractController.downloadContract);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Contracts
 *   description: Contract management and retrieval
 */

/**
 * @swagger
 *  /contracts:
 *    post:
 *      summary: Create a contract
 *      description: Only admins can create other contracts.
 *      tags: [Contracts]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - guarantorName
 *                - approvedLoanAmount
 *                - firstPaymentDue
 *                - finalPaymentDue
 *                - monthlyPayment
 *                - finalPayment
 *                - status
 *                - contractStartDate
 *                - client
 *                - application
 *              properties:
 *                guarantorName:
 *                  type: string
 *                approvedLoanAmount:
 *                  type: number
 *                firstPaymentDue:
 *                  type: date
 *                finalPaymentDue:
 *                  type: date
 *                monthlyPayment:
 *                  type: number
 *                finalPayment:
 *                  type: number
 *                status:
 *                  type: string
 *                contractStartDate:
 *                  type: date
 *                client:
 *                  type: string
 *                  $ref: '#/components/schemas/Client'
 *                application:
 *                  type: string
 *                  $ref: '#/components/schemas/Application'
 *              example:
 *                guarantorName: Joe Green
 *                approvedLoanAmount: 1500
 *                firstPaymentDue: 2021/06/06
 *                finalPaymentDue: 2022/06/06
 *                monthlyPayment: 125
 *                finalPayment: 125
 *                status: Draft
 *                contractStartDate: 2021/06/01
 *                client: 6011e5410077e70030610e9e
 *                application: 6011e5410077e70030610e9e
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Contract'
 *        "400":
 *          $ref: '#/components/responses/DuplicateOfficer'
 *
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *
 *    get:
 *      summary: Get all contracts
 *      description: Only admins can retrieve all contracts.
 *      tags: [Contracts]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: application
 *          schema:
 *            type: string
 *          description: Application ID
 *        - in: query
 *          name: client
 *          schema:
 *            type: string
 *          description: Client ID
 *        - in: query
 *          name: officer
 *          schema:
 *            type: string
 *          description: Officer ID
 *        - in: query
 *          name: status
 *          schema:
 *            type: string
 *          description: Contract Status
 *        - in: query
 *          name: populate
 *          schema:
 *            type: string
 *          description: Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,) (ex. user)
 *        - in: query
 *          name: sortBy
 *          schema:
 *            type: string
 *          description: Sort by query in the form of field:desc/asc (ex. name:asc)
 *        - in: query
 *          name: limit
 *          schema:
 *            type: integer
 *            minimum: 1
 *          default: 10
 *          description: Maximum number of contracts
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *            minimum: 1
 *            default: 1
 *          description: Page number
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Contract'
 *                  page:
 *                    type: integer
 *                    example: 1
 *                  limit:
 *                    type: integer
 *                    example: 10
 *                  totalPages:
 *                    type: integer
 *                    example: 1
 *                  totalResults:
 *                    type: integer
 *                    example: 1
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /contracts/{id}:
 *    get:
 *      summary: Get a contract
 *      description: Only admins can fetch contracts.
 *      tags: [Contracts]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Contract id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Contract'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    patch:
 *      summary: Update a contract
 *      description: Only admins can update contracts.
 *      tags: [Contracts]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Contract id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                guarantorName:
 *                  type: string
 *                approvedLoanAmount:
 *                  type: number
 *                firstPaymentDue:
 *                  type: date
 *                finalPaymentDue:
 *                  type: date
 *                monthlyPayment:
 *                  type: number
 *                finalPayment:
 *                  type: number
 *                contractStartDate:
 *                  type: date
 *                client:
 *                  type: string
 *                  $ref: '#/components/schemas/Client'
 *              example:
 *                guarantorName: Joe Green
 *                approvedLoanAmount: 1500
 *                firstPaymentDue: 2021-06-06
 *                finalPaymentDue: 2022-06-06
 *                monthlyPayment: 125
 *                finalPayment: 125
 *                contractStartDate: 2021-06-01
 *                client: 6011e5410077e70030610e9e
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Contract'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    delete:
 *      summary: Delete a contract
 *      description: Only admins can delete contracts.
 *      tags: [Contracts]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Contract id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /contracts/{id}/download:
 *    get:
 *      summary: Download a contract
 *      description: Only admins and loan officers can download contracts.
 *      tags: [Contracts]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Contract id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/pdf:
 *              schema:
 *                 type: string
 *                 format: binary
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /contracts/{id}/upload:
 *    post:
 *      summary: Upload a contract
 *      description: Only admins and loan officers can download contracts.
 *      tags: [Contracts]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Contract id
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              type: object
 *              required:
 *                - file
 *              properties:
 *                file:
 *                  type: string
 *                  format: binary
 *              example:
 *                file: Loan-Contract-123456789.pdf
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/pdf:
 *              schema:
 *                 $ref: '#/components/schemas/Contract'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
