const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { upload } = require("../../middlewares/upload");
const { applicationValidation } = require("../../validations");
const { applicationController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .post(validate(applicationValidation.createApplication), applicationController.createApplication)
  .get(auth("getApplications"), validate(applicationValidation.getApplications), applicationController.getApplications);

router
  .route("/import")
  .post(
    auth("manageApplications"),
    upload,
    validate(applicationValidation.importApplications),
    applicationController.importApplications
  );

router
  .route("/:applicationId")
  .get(auth("getApplications"), validate(applicationValidation.getApplication), applicationController.getApplication)
  .patch(
    auth("manageApplications"),
    validate(applicationValidation.updateApplication),
    applicationController.updateApplication
  )
  .delete(
    auth("manageApplications"),
    validate(applicationValidation.deleteApplication),
    applicationController.deleteApplication
  );

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Applications
 *   description: Application management and retrieval
 */

/**
 * @swagger
 *  /applications:
 *    post:
 *      summary: Create a application
 *      description: Only admins can create other applications.
 *      tags: [Applications]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                -firstName
 *                -lastName
 *                -email
 *                -phoneNumber
 *                -address
 *                -city
 *                -province
 *                -sex
 *                -dateOfBirth
 *                -maritalStatus
 *                -citizenship
 *                -preferredLanguage
 *                -employmentStatus
 *                -loanAmountRequested
 *                -loanType
 *                -debtCircumstances
 *                -guarantor
 *                -recommendationInfo
 *                -acknowledgements
 *                -emailOptIn
 *              properties:
 *                firstName:
 *                  type: string
 *                lastName:
 *                  type: string
 *                email:
 *                  type: string
 *                  format: email
 *                phoneNumber:
 *                  type: string
 *                address:
 *                  type: string
 *                city:
 *                  type: string
 *                province:
 *                  type: string
 *                sex:
 *                  type: string
 *                dateOfBirth:
 *                  type: date
 *                maritalStatus:
 *                  type: string
 *                citizenship:
 *                  type: string
 *                preferredLanguage:
 *                  type: string
 *                employmentStatus:
 *                  type: string
 *                loanAmountRequested:
 *                  type: number
 *                loanType:
 *                  type: string
 *                debtCircumstances:
 *                  type: string
 *                postalCode:
 *                  type: string
 *                guarantor:
 *                  type: object
 *                  properties:
 *                    hasGuarantor:
 *                      type: boolean
 *                    fullName:
 *                      type: string
 *                    email:
 *                      type: string
 *                    phoneNumber:
 *                      type: string
 *                recommendationInfo:
 *                  type: string
 *                acknowledgements:
 *                  type: object
 *                  properties:
 *                    loanPurpose:
 *                      type: boolean
 *                    maxLoan:
 *                      type: boolean
 *                    repayment:
 *                      type: boolean
 *                    residence:
 *                      type: boolean
 *                    netPositiveIncome:
 *                      type: boolean
 *                    guarantorConsent:
 *                      type: boolean
 *                services:
 *                  type: object
 *                  properties:
 *                    careerCoaching:
 *                      type: boolean
 *                    resumeCritique:
 *                      type: boolean
 *                    financialCoaching:
 *                      type: boolean
 *                    communityResources:
 *                      type: boolean
 *                emailOptIn: true
 *                status:
 *                  type: string
 *                  enum: [Received, Assigned, Contacted, Interview, Requested Information, Received Information, Proccessing Information, Committee Discussion, Accepted, Rejected, Waitlist, Contract Sent, Contract Signed, Active Client, Updating Client Contract, Archived]
 *                officer:
 *                  type: string
 *                user:
 *                  type: string
 *                dateApplied:
 *                  type: date
 *              example:
 *                  firstName: John
 *                  lastName: Doe
 *                  email: johndoe@gmail.com
 *                  phoneNumber: "16132332892"
 *                  address: 123 Sesame Street
 *                  city: Ottawa
 *                  province: Ontario
 *                  sex: Male
 *                  dateOfBirth: 2002/11/06
 *                  maritalStatus: Single
 *                  citizenship: Canadian
 *                  preferredLanguage: English
 *                  employmentStatus: Employed Full Time
 *                  loanAmountRequested: 4000
 *                  loanType: Other
 *                  debtCircumstances: Gambled money on GME
 *                  postalCode: A1A1A1
 *                  guarantor:
 *                    hasGuarantor: true
 *                    fullName: Jimmy John
 *                    email: jimmyjohn@daboom.com
 *                    phoneNumber: "16131234567"
 *                  recommendationInfo: Friend told me to borrow money from here to buy more GME
 *                  acknowledgements:
 *                    loanPurpose: true
 *                    maxLoan: true
 *                    repayment: true
 *                    residence: true
 *                    netPositiveIncome: true
 *                    guarantorConsent: true
 *                  services:
 *                    careerCoaching: true
 *                    resumeCritique: false
 *                    financialCoaching: true
 *                    communityResources: false
 *                  emailOptIn: true
 *                  status: Received
 *                  officer: 4abc40c86762e0fb12000123
 *                  user: 4def40c86762e0fb12000456
 *                  dateApplied: 2002-11-06
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Application'
 *        "400":
 *          $ref: '#/components/responses/DuplicateApplication'
 *
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *
 *    get:
 *      summary: Get all applications
 *      description: Only admins can retrieve all applications.
 *      tags: [Applications]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: nameOrEmail
 *          schema:
 *            type: string
 *          description: Applicant's first or last name or email
 *        - in: query
 *          name: status
 *          schema:
 *            type: array
 *            items:
 *               type: string
 *               enum: [Received, Assigned, Contacted, Interview, Requested Information, Received Information, Proccessing Information, Committee Discussion, Accepted, Rejected, Waitlist, Contract Sent, Contract Signed, Active Client, Updating Client Contract, Archived]
 *          description: Status of the application
 *        - in: query
 *          name: displayAll
 *          schema:
 *            type: boolean
 *          description: (True) - Display all applications / (False) - List view and default behavior, don't display applications with active clients or that are archived
 *        - in: query
 *          name: user
 *          schema:
 *            type: string
 *          description: User's ID
 *        - in: query
 *          name: officer
 *          schema:
 *            type: array
 *            items:
 *              type: string
 *          description: Officer's ID
 *        - in: query
 *          name: populate
 *          schema:
 *            type: string
 *          description: Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,) (ex. user)
 *        - in: query
 *          name: sortBy
 *          schema:
 *            type: string
 *          description: Sort by query in the form of field:desc/asc (ex. name:asc)
 *        - in: query
 *          name: limit
 *          schema:
 *            type: integer
 *            minimum: 1
 *          default: 10
 *          description: Maximum number of applications
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *            minimum: 1
 *            default: 1
 *          description: Page number
 *        - in: query
 *          name: startDate
 *          schema:
 *            type: Date
 *          description: Applications submitted after given date
 *        - in: query
 *          name: endDate
 *          schema:
 *            type: Date
 *          description: Applications submitted before given date
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Application'
 *                  page:
 *                    type: integer
 *                    example: 1
 *                  limit:
 *                    type: integer
 *                    example: 10
 *                  totalPages:
 *                    type: integer
 *                    example: 1
 *                  totalResults:
 *                    type: integer
 *                    example: 1
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /applications/import:
 *    post:
 *      summary: Import applications via CSV file
 *      description: Only admins can upload applications
 *      tags: [Applications]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              type: object
 *              required:
 *                - file
 *              properties:
 *                file:
 *                  type: string
 *                  format: binary
 *              example:
 *                file: applications.csv
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ApplicationImport'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 *  /applications/{id}:
 *    get:
 *      summary: Get a application
 *      description: Logged in users can fetch only their own application information. Only admins can fetch other applications.
 *      tags: [Applications]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Application id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Application'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    patch:
 *      summary: Update a application
 *      description: Only admins can update applications.
 *      tags: [Applications]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Application id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                firstName:
 *                  type: string
 *                lastName:
 *                  type: string
 *                email:
 *                  type: string
 *                  format: email
 *                phoneNumber:
 *                  type: string
 *                address:
 *                  type: string
 *                city:
 *                  type: string
 *                province:
 *                  type: string
 *                sex:
 *                  type: string
 *                dateOfBirth:
 *                  type: date
 *                maritalStatus:
 *                  type: string
 *                citizenship:
 *                  type: string
 *                employmentStatus:
 *                  type: string
 *                loanAmountRequested:
 *                  type: number
 *                loanType:
 *                  type: string
 *                debtCircumstances:
 *                  type: string
 *                postalCode:
 *                  type: string
 *                guarantor:
 *                  type: object
 *                  properties:
 *                    hasGuarantor:
 *                      type: boolean
 *                    fullName:
 *                      type: string
 *                    email:
 *                      type: string
 *                    phoneNumber:
 *                      type: string
 *                recommendationInfo:
 *                  type: string
 *                acknowledgements:
 *                  type: object
 *                  properties:
 *                    loanPurpose:
 *                      type: boolean
 *                    maxLoan:
 *                      type: boolean
 *                    repayment:
 *                      type: boolean
 *                    residence:
 *                      type: boolean
 *                    netPositiveIncome:
 *                      type: boolean
 *                    guarantorConsent:
 *                      type: boolean
 *                services:
 *                  type: object
 *                  properties:
 *                    careerCoaching:
 *                      type: boolean
 *                    resumeCritique:
 *                      type: boolean
 *                    financialCoaching:
 *                      type: boolean
 *                    communityResources:
 *                      type: boolean
 *                emailOptIn: true
 *                status:
 *                  type: string
 *                  enum: [Received, Assigned, Contacted, Interview, Requested Information, Received Information, Proccessing Information, Committee Discussion, Accepted, Rejected, Waitlist, Contract Sent, Contract Signed, Active Client, Updating Client Contract, Archived]
 *                officer:
 *                  type: string
 *                user:
 *                  type: string
 *              example:
 *                firstName: John
 *                lastName: Doe
 *                email: johndoe@gmail.com
 *                phoneNumber: "16132332892"
 *                address: 123 Sesame Street
 *                city: Ottawa
 *                province: Ontario
 *                sex: Male
 *                dateOfBirth: 2002-11-06
 *                maritalStatus: Single
 *                citizenship: Canadian
 *                employmentStatus: Employed Full Time
 *                loanAmountRequested: 4000
 *                loanType: Other
 *                debtCircumstances: Gambled money on GME
 *                postalCode: A1A1A1
 *                guarantor:
 *                    hasGuarantor: true
 *                    fullName: Jimmy John
 *                    email: jimmyjohn@daboom.com
 *                    phoneNumber: "16131234567"
 *                recommendationInfo: Friend told me to borrow money from here to buy more GME
 *                acknowledgements:
 *                  loanPurpose: true
 *                  maxLoan: true
 *                  repayment: true
 *                  residence: true
 *                  netPositiveIncome: true
 *                  guarantorConsent: true
 *                services:
 *                  careerCoaching: true
 *                  resumeCritique: false
 *                  financialCoaching: true
 *                  communityResources: false
 *                status: Received
 *                officer: 4abc40c86762e0fb12000123
 *                user: 4def40c86762e0fb12000456
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Application'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    delete:
 *      summary: Delete a application
 *      description: Only admins can delete applications.
 *      tags: [Applications]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Application id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
