const express = require("express");
const authRoute = require("./auth.route");
const applicationRoute = require("./application.route");
const clientRoute = require("./client.route");
const contractRoute = require("./contract.route");
const officerRoute = require("./officer.route");
const userRoute = require("./user.route");
const documentRoute = require("./document.route");
const paymentRoute = require("./payment.route");
const docsRoute = require("./docs.route");
const config = require("../../config/config");

const router = express.Router();

const defaultRoutes = [
  {
    path: "/auth",
    route: authRoute
  },
  {
    path: "/officers",
    route: officerRoute
  },
  {
    path: "/users",
    route: userRoute
  },
  {
    path: "/clients",
    route: clientRoute
  },
  {
    path: "/contracts",
    route: contractRoute
  },
  {
    path: "/applications",
    route: applicationRoute
  },
  {
    path: "/documents",
    route: documentRoute
  },
  {
    path: "/payments",
    route: paymentRoute
  },
  {
    path: "/docs",
    route: docsRoute
  }
];

const devRoutes = [
  // routes available only in development mode
  // {
  //   path: "/docs",
  //   route: docsRoute
  // }
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === "development") {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
