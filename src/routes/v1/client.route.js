const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { clientValidation } = require("../../validations");
const { clientController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .post(auth("manageClients"), validate(clientValidation.createClient), clientController.createClient)
  .get(auth("getClients"), validate(clientValidation.getClients), clientController.getClients);

router
  .route("/:clientId")
  .get(auth("getClients"), validate(clientValidation.getClient), clientController.getClient)
  .patch(auth("manageClients"), validate(clientValidation.updateClient), clientController.updateClient)
  .delete(auth("manageClients"), validate(clientValidation.deleteClient), clientController.deleteClient);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Clients
 *   description: Client management and retrieval
 */

/**
 * @swagger
 *  /clients:
 *    post:
 *      summary: Create a client
 *      description: Only admins can create other clients.
 *      tags: [Clients]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - officer
 *                - application
 *              properties:
 *                dateSigned:
 *                  type: date
 *                officer:
 *                  type: string
 *                  $ref: '#/components/schemas/User'
 *                application:
 *                  type: string
 *                  $ref: '#/components/schemas/Application'
 *              example:
 *                dateSigned: 2020-05-12
 *                officer: 4abc40c86762e0fb12000123
 *                application: 4def40c86762e0fb12000456
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Client'
 *        "400":
 *          $ref: '#/components/responses/DuplicateClient'
 *
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *
 *    get:
 *      summary: Get all clients
 *      description: Only admins can retrieve all clients.
 *      tags: [Clients]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: nameOrEmail
 *          schema:
 *            type: string
 *          description: Client's first or last name or email
 *        - in: query
 *          name: status
 *          schema:
 *            type: string
 *            enum: [Active Client, Updating Client Contract, Archived]
 *        - in: query
 *          name: application
 *          schema:
 *            type: string
 *          description: Application ID
 *        - in: query
 *          name: officer
 *          schema:
 *            type: array
 *            items:
 *              type: string
 *          description: Officer ID
 *        - in: query
 *          name: startDate
 *          schema:
 *            type: Date
 *          description: Start date of query
 *        - in: query
 *          name: endDate
 *          schema:
 *            type: Date
 *          description: End date of query
 *        - in: query
 *          name: populate
 *          schema:
 *            type: string
 *          description: Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,) (ex. user)
 *        - in: query
 *          name: sortBy
 *          schema:
 *            type: string
 *          description: Sort by query in the form of field:desc/asc (ex. name:asc)
 *        - in: query
 *          name: limit
 *          schema:
 *            type: integer
 *            minimum: 1
 *          default: 10
 *          description: Maximum number of clients
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *            minimum: 1
 *            default: 1
 *          description: Page number
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Client'
 *                  page:
 *                    type: integer
 *                    example: 1
 *                  limit:
 *                    type: integer
 *                    example: 10
 *                  totalPages:
 *                    type: integer
 *                    example: 1
 *                  totalResults:
 *                    type: integer
 *                    example: 1
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /clients/{id}:
 *    get:
 *      summary: Get a client
 *      description: Logged in users can fetch only their own client information. Only admins can fetch other clients.
 *      tags: [Clients]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Client id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Client'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    patch:
 *      summary: Update a client
 *      description: Only admins can update clients.
 *      tags: [Clients]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Client id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                officer:
 *                  type: string
 *                  description: Officer's user id
 *              example:
 *                officer: 4abc40c86762e0fb12000123
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Client'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    delete:
 *      summary: Delete a client
 *      description: Only admins can delete clients.
 *      tags: [Clients]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Client id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
