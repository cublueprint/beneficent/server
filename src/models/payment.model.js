const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const paymentSchema = mongoose.Schema(
  {
    amountDue: { type: Number, required: true },
    amountPaid: { type: Number, required: true },
    dateDue: { type: Date, required: true },
    datePaid: { type: Date, required: true },
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Client",
      required: true
    },
    contract: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Contract",
      required: true
    }
  },
  {
    timestamps: true
  }
);

paymentSchema.plugin(toJSON);
paymentSchema.plugin(paginate);

/**
 * @typedef Payment
 */
const Payment = mongoose.model("Payment", paymentSchema);

module.exports = Payment;
