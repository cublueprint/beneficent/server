const mongoose = require("mongoose");
const validator = require("validator");
const { toJSON, paginate } = require("./plugins");
const { ClientStatuses, ApplicationStatuses } = require("../utils/statusUtils");

const applicationSchema = mongoose.Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Invalid email");
        }
      }
    },
    phoneNumber: {
      type: String,
      required: true,
      validate(value) {
        let re = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
        if (!re.test(value)) {
          throw new Error("Invalid phone number");
        }
      }
    },
    address: { type: String, required: true },
    city: { type: String, required: true },
    province: { type: String, required: false },
    sex: { type: String, required: true },
    dateOfBirth: { type: Date, required: true },
    dateApplied: { type: Date, required: true },
    maritalStatus: { type: String, required: true },
    citizenship: { type: String, required: true },
    preferredLanguage: { type: String, default: "English" },
    employmentStatus: { type: String, required: true },
    loanAmountRequested: { type: Number, required: true },
    loanType: { type: String, required: true },
    debtCircumstances: { type: String, required: true },
    postalCode: { type: String, required: false },
    guarantor: {
      hasGuarantor: Boolean,
      fullName: { type: String, required: false },
      email: {
        type: String,
        trim: true,
        lowercase: true,
        required: false,
        validate(value) {
          if (value != "" && !validator.isEmail(value)) {
            throw new Error("Invalid email");
          }
        }
      },
      phoneNumber: {
        type: String,
        required: false,
        validate(value) {
          if (value != "" && !validator.isMobilePhone(value)) {
            throw new Error("Invalid phone number");
          }
        }
      }
    },
    recommendationInfo: { type: String, required: true },
    acknowledgements: {
      loanPurpose: { type: Boolean, required: true },
      maxLoan: { type: Boolean, required: true },
      repayment: { type: Boolean, required: true },
      residence: { type: Boolean, required: true },
      netPositiveIncome: { type: Boolean, required: true },
      guarantorConsent: { type: Boolean, required: true }
    },
    services: {
      careerCoaching: { type: Boolean, required: false },
      resumeCritique: { type: Boolean, required: false },
      financialCoaching: { type: Boolean, required: false },
      communityResources: { type: Boolean, required: false }
    },
    emailOptIn: { type: Boolean, required: false },
    status: { type: String, required: true, default: "Received" },
    validStatuses: {
      type: Array,
      default: function () {
        return Object.values(ApplicationStatuses);
      }
    },
    officer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Officer",
      required: false
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: false }
  },
  {
    timestamps: true
  }
);

applicationSchema.plugin(toJSON);
applicationSchema.plugin(paginate);

applicationSchema.statics.updateValidStatuses = async function (id) {
  const application = await this.findOne({ _id: id });
  if (Object.values(ClientStatuses).includes(application.status)) {
    application.validStatuses = Object.values(ClientStatuses);
    await application.save();
  }
  return application;
};

/**
 * Update workload for officers
 */
applicationSchema.post("save", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * Update workload for officers
 */
applicationSchema.post("remove", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * @typedef Application
 */
const Application = mongoose.model("Application", applicationSchema);

module.exports = Application;
