const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");
const { ClientStatuses } = require("../utils/statusUtils");

const clientSchema = mongoose.Schema(
  {
    dateSigned: { type: Date, required: true },
    currentContract: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Contract",
      required: false
    },
    status: { type: String, required: true, default: "Active Client" },
    wave: {
      customerId: { type: String },
      invoiceId: { type: String },
      invoiceUrl: { type: String },
      productId: { type: String }
    },
    officer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Officer",
      required: false
    },
    application: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Application",
      required: true
    },
    validStatuses: {
      type: Array,
      default: function () {
        return Object.values(ClientStatuses);
      }
    }
  },
  {
    timestamps: true
  }
);

clientSchema.plugin(toJSON);
clientSchema.plugin(paginate);

/**
 * Check if user ID or application ID is already assigned to a client
 * @param {ObjectId} userId - The user id
 * @param {ObjectId} applicationId - The application id
 * @returns {Promise<boolean>}
 */
clientSchema.statics.isDuplicate = async function (applicationId) {
  const application = await this.findOne({ application: applicationId });
  return application;
};

/**
 * Update workload for officers
 */
clientSchema.post("save", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * Update workload for officers
 */
clientSchema.post("remove", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * @typedef Client
 */
const Client = mongoose.model("Client", clientSchema);

module.exports = Client;
