const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const secondsInMonth = 2628000000;

const contractSchema = mongoose.Schema(
  {
    approvedLoanAmount: { type: Number, required: true },
    firstPaymentDue: { type: Date, required: true },
    finalPaymentDue: { type: Date, required: true },
    monthlyPayment: {
      type: Number,
      required: true
    },
    finalPayment: {
      type: Number,
      default: 0
    },
    status: { type: String, required: true },
    contractFileName: { type: String, required: true },
    createdBy: { type: mongoose.SchemaTypes.ObjectId, ref: "User", required: true },
    contractFileBuffer: { type: Buffer, private: true },
    contractStartDate: { type: Date, required: true },
    contractCycleLength: { type: Number },
    wave: {
      totalAmountPaid: { type: Number, default: 0 },
      totalAmountRemaining: { type: Number, default: this.approvedLoanAmount },
      expectedTotalAmountPaid: {
        type: Number,
        default: function () {
          return (
            this.monthlyPayment *
              Math.round((this.finalPaymentDue.getTime() - this.firstPaymentDue.getTime()) / secondsInMonth) +
            this.finalPayment
          );
        }
      },
      nextPaymentAmount: { type: Number, default: this.monthlyPayment },
      nextPaymentDueDate: { type: Date, default: this.firstPaymentDue },
      credit: { type: Number, default: 0 },
      missedPayments: { type: Number, default: 0, private: true }
    },
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Client",
      required: false
    },
    application: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Application",
      required: true
    }
  },
  {
    timestamps: true
  }
);

contractSchema.pre("save", function (next) {
  this.contractCycleLength = this.firstPaymentDue.getTime() - this.contractStartDate.getTime();
  next();
});

contractSchema.plugin(toJSON);
contractSchema.plugin(paginate);

/**
 * @typedef Contract
 */
const Contract = mongoose.model("Contract", contractSchema);

module.exports = Contract;
