const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const documentSchema = mongoose.Schema(
  {
    docType: { type: String, required: true },
    originalName: { type: String, required: true },
    mimeType: { type: String, required: true },
    size: { type: String, required: true },
    buffer: { type: Buffer, required: true, private: true },
    uploadedBy: { type: mongoose.SchemaTypes.ObjectId, ref: "User", required: false },
    updatedBy: { type: mongoose.SchemaTypes.ObjectId, ref: "User", required: false },
    application: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Application",
      required: true
    }
  },
  {
    timestamps: true
  }
);

documentSchema.plugin(toJSON);
documentSchema.plugin(paginate);

/**
 * @typedef Document
 */
const Document = mongoose.model("Document", documentSchema);

module.exports = Document;
