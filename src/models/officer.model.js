const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const officerSchema = mongoose.Schema(
  {
    user: { type: mongoose.SchemaTypes.ObjectId, ref: "User", required: true },
    workload: { type: Number, default: 0 }
  },
  {
    timestamps: true
  }
);

officerSchema.plugin(toJSON);
officerSchema.plugin(paginate);

const getArchivedApplications = async (params) => {
  const Client = mongoose.model("Client");
  const Application = mongoose.model("Application");

  let archivedApplications = 0;
  for await (const client of Client.find(params)) {
    let currentApplication = await Application.findById(client.application);
    if (currentApplication.status === "Archived") {
      archivedApplications++;
    }
  }

  return archivedApplications;
};

/**
 * Update workload for all officers
 */
officerSchema.statics.updateWorkload = async function () {
  const Client = mongoose.model("Client");
  const Application = mongoose.model("Application");

  // Calculate total number of clients where the associated application is not archived
  let totalNumClients = await Client.countDocuments();
  const numClientsWithArchivedApplications = await getArchivedApplications({});

  totalNumClients -= numClientsWithArchivedApplications;

  // Calculate total number of applications that aren't archived and don't have an associcated client
  const totalNumApplications = await Application.find({
    status: { $nin: ["Archived", "Updating Client Contract", "Active Client"] }
  }).countDocuments();

  //totalNumClients + totalNumApplications = total number of applications that have not become clients and clients whose application have not been archived

  for await (const officer of Officer.find()) {
    let numClientsForOfficer = await Client.find({ officer: officer._id }).countDocuments();
    const numArchivedClientsForOfficer = await getArchivedApplications({ officer: officer._id });

    numClientsForOfficer -= numArchivedClientsForOfficer;

    const numApplicationsForOfficer = await Application.find({
      officer: officer._id,
      status: { $nin: ["Archived", "Updating Client Contract", "Active Client"] }
    }).countDocuments();

    let ratio = ((numClientsForOfficer + numApplicationsForOfficer) / (totalNumClients + totalNumApplications)) * 100 || 0;
    officer.workload = Math.floor(ratio);
    await Officer.findByIdAndUpdate(officer._id, { workload: ratio });
  }
};

/**
 * Check if user ID is already assigned to a officer
 * @param {ObjectId} userId - The user id
 * @returns {Promise<boolean>}
 */
officerSchema.statics.isDuplicate = async function (userId) {
  const officer = await this.findOne({ user: userId });
  return !!officer;
};

/**
 * @typedef Officer
 */
const Officer = mongoose.model("Officer", officerSchema);

module.exports = Officer;
