const dotenv = require("dotenv");
const path = require("path");
const Joi = require("joi");

dotenv.config({ path: path.join(__dirname, "../../.env") });

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string().valid("production", "development", "test").required(),
    PORT: Joi.number(),
    MONGODB_URL: Joi.string().required().description("Mongo DB url"),
    MONGODB_TEST_URL: Joi.string().description("Mongo DB test url"),
    JWT_SECRET: Joi.string().required().description("JWT secret key"),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number().description("minutes after which access tokens expire"),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number().description("days after which refresh tokens expire"),
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: Joi.number().description("minutes after which reset password token expires"),
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: Joi.number().description("minutes after which verify email token expires"),
    SMTP_REJECT_SELFSIGNED_CERTS: Joi.boolean().description("accept or reject self-signed certificates."),
    SMTP_HOST: Joi.string().description("server that will send the emails"),
    SMTP_PORT: Joi.number().description("port to connect to the email server"),
    SMTP_USERNAME: Joi.string().description("username for email server"),
    SMTP_PASSWORD: Joi.string().description("password for email server"),
    EMAIL_FROM: Joi.string().description("the from field in the emails sent by the app"),
    SERVER_URL: Joi.string().description("Server url"),
    CLIENT_URL: Joi.string().description("Client url"),
    WAVE_API_KEY: Joi.string().description("Wave API key"),
    WAVE_BUSINESS_ID: Joi.string().description("Wave business id"),
    WAVE_INCOME_ACCOUNT_ID: Joi.string().description("Wave income id"),
    WAVE_API_ENDPOINT: Joi.string().description("Wave API endpoint")
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: "key" } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongoose: {
    testUrl: envVars.MONGODB_TEST_URL,
    url: envVars.MONGODB_URL,
    options: {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    }
  },
  jwt: {
    secret: envVars.JWT_SECRET,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: envVars.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes: envVars.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES
  },
  email: {
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      auth: {
        user: envVars.SMTP_USERNAME,
        pass: envVars.SMTP_PASSWORD
      },
      rejectUnauthorized: envVars.SMTP_REJECT_SELFSIGNED_CERTS
    },
    from: envVars.EMAIL_FROM
  },
  serverUrl: envVars.SERVER_URL,
  clientUrl: envVars.CLIENT_URL,
  wave: {
    apiKey: envVars.WAVE_API_KEY,
    businessId: envVars.WAVE_BUSINESS_ID,
    incomeAccountId: envVars.WAVE_INCOME_ACCOUNT_ID,
    apiEndpoint: envVars.WAVE_API_ENDPOINT
  }
};
