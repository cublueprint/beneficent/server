const permissions = {
  pendingUser: ["verifyEmail"],
  pendingAdmin: ["verifyEmail"],
  admin: [
    "getUsers",
    "manageUsers",
    "getClients",
    "manageClients",
    "getApplications",
    "manageApplications",
    "getOfficers",
    "manageOfficers",
    "getContracts",
    "manageContracts",
    "getDocuments",
    "manageDocuments",
    "getPayments",
    "managePayments",
    "verifyEmail",
    "sendVerification"
  ],
  user: [
    "getUsers",
    "getClients",
    "manageClients",
    "getApplications",
    "manageApplications",
    "getContracts",
    "getOfficers",
    "manageContracts",
    "getDocuments",
    "manageDocuments",
    "getPayments",
    "managePayments",
    "verifyEmail"
  ]
};

const roles = Object.keys(permissions);
const roleRights = new Map(Object.entries(permissions));

module.exports = {
  roles,
  roleRights
};
