const httpStatus = require("http-status");
const pick = require("../utils/pick");
const ApiError = require("../utils/ApiError");
const catchAsync = require("../utils/catchAsync");
const { contractService } = require("../services");

const createContract = catchAsync(async (req, res) => {
  const contract = await contractService.createContract(req.body);
  res.status(httpStatus.CREATED).send(contract);
});

const getContracts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ["application", "client", "officer", "status"]);
  const options = pick(req.query, ["populate", "sortBy", "limit", "page"]);
  const result = await contractService.queryContracts(filter, options);
  res.send(result);
});

const getContract = catchAsync(async (req, res) => {
  const contract = await contractService.getContractById(req.params.contractId);
  res.send(contract);
});

const updateContract = catchAsync(async (req, res) => {
  const contract = await contractService.updateContractById(req.params.contractId, req.body);
  res.send(contract);
});

const deleteContract = catchAsync(async (req, res) => {
  await contractService.deleteContractById(req.params.contractId);
  res.status(httpStatus.NO_CONTENT).send();
});

const downloadContract = catchAsync(async (req, res) => {
  let contractFileBuffer = "";
  const contract = await contractService.getContractById(req.params.contractId);
  if (contract.status === "Active") {
    contractFileBuffer = contract.contractFileBuffer;
  } else {
    contractFileBuffer = await contractService.createContractPDFById(req.params.contractId);
  }
  res.writeHead(httpStatus.CREATED, {
    "Content-Disposition": `attachment; filename="${contract.contractFileName}"`,
    "Content-Type": "application/pdf"
  });
  res.end(contractFileBuffer);
});

const uploadContract = catchAsync(async (req, res) => {
  const contractFileBuffer = req.file.buffer;
  const contract = await contractService.uploadContract(req.params.contractId, contractFileBuffer);
  res.status(httpStatus.CREATED).send(contract);
});

module.exports = {
  createContract,
  getContracts,
  getContract,
  updateContract,
  deleteContract,
  downloadContract,
  uploadContract
};
