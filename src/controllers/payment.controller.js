const pick = require("../utils/pick");
const catchAsync = require("../utils/catchAsync");
const { paymentService } = require("../services");

const getPayments = catchAsync(async (req, res) => {
  const filter = pick(req.query, ["client", "cache"]);
  const options = pick(req.query, ["populate", "sortBy", "limit", "page"]);
  const result = await paymentService.queryPayments(filter, options);
  res.send(result);
});

module.exports = {
  getPayments
};
