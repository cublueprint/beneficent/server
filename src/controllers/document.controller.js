const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { documentService } = require("../services");
const pick = require("../utils/pick");

const createDocument = catchAsync(async (req, res) => {
  const documentBody = pick(req.file, ["originalName", "mimeType", "path", "size", "buffer"]);
  const fields = pick(req.body, ["docType", "application", "uploadedBy"]);
  const document = await documentService.createDocument(documentBody, fields);
  res.status(httpStatus.CREATED).send(document);
});

const getDocuments = catchAsync(async (req, res) => {
  const filter = pick(req.query, ["docType", "originalName", "application"]);
  const options = pick(req.query, ["populate", "sortBy", "limit", "page"]);
  const result = await documentService.queryDocuments(filter, options);
  res.send(result);
});

const getDocumentById = catchAsync(async (req, res) => {
  const document = await documentService.getDocumentById(req.params.documentId);
  res.set("Content-disposition", "attachment; filename=" + document.originalName);
  res.send(document.buffer);
});

const updateDocument = catchAsync(async (req, res) => {
  const document = await documentService.updateDocumentById(req.params.documentId, req.body);
  res.status(httpStatus.CREATED).send(document);
});

const deleteDocument = catchAsync(async (req, res) => {
  await documentService.deleteDocumentById(req.params.documentId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createDocument,
  getDocuments,
  getDocumentById,
  updateDocument,
  deleteDocument
};
