module.exports.applicationController = require("./application.controller");
module.exports.authController = require("./auth.controller");
module.exports.clientController = require("./client.controller");
module.exports.contractController = require("./contract.controller");
module.exports.officerController = require("./officer.controller");
module.exports.userController = require("./user.controller");
module.exports.documentController = require("./document.controller");
module.exports.paymentController = require("./payment.controller");
