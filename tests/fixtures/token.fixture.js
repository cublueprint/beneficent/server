const moment = require("moment");
const config = require("../../src/config/config");
const { tokenTypes } = require("../../src/config/tokens");
const tokenService = require("../../src/services/token.service");
const { userOne, userTwo, officerUserOne, officerUserTwo, admin, pendingUser } = require("./user.fixture");
const { clientOne, clientTwo } = require("./client.fixture");

const accessTokenExpires = moment().add(config.jwt.accessExpirationMinutes, "minutes");
const userOneAccessToken = tokenService.generateToken(userOne._id, accessTokenExpires, tokenTypes.ACCESS);
const userTwoAccessToken = tokenService.generateToken(userTwo._id, accessTokenExpires, tokenTypes.ACCESS);
const pendingUserAccessToken = tokenService.generateToken(pendingUser._id, accessTokenExpires, tokenTypes.ACCESS);
const clientOneAccessToken = tokenService.generateToken(clientOne._id, accessTokenExpires, tokenTypes.ACCESS);
const clientTwoAccessToken = tokenService.generateToken(clientTwo._id, accessTokenExpires, tokenTypes.ACCESS);
const officerOneAccessToken = tokenService.generateToken(officerUserOne._id, accessTokenExpires, tokenTypes.ACCESS);
const officerTwoAccessToken = tokenService.generateToken(officerUserTwo._id, accessTokenExpires, tokenTypes.ACCESS);
const adminAccessToken = tokenService.generateToken(admin._id, accessTokenExpires, tokenTypes.ACCESS);

module.exports = {
  userOneAccessToken,
  userTwoAccessToken,
  clientOneAccessToken,
  clientTwoAccessToken,
  officerOneAccessToken,
  officerTwoAccessToken,
  adminAccessToken,
  pendingUserAccessToken
};
