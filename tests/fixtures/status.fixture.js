const ClientStatuses = {
  ACTIVE: "Active Client",
  UPDATING_CONTRACT: "Updating Client Contract",
  ARCHIVED: "Archived"
};

const ApplicationStatuses = {
  RECIEVED: "Received",
  ASSIGNED: "Assigned",
  CONTACTED: "Contacted",
  INTERVIEW: "Interview",
  REQUESTED_INFORMATION: "Requested Information",
  RECIEVED_INFORMATION: "Received Information",
  PROCESSING_INFORMATION: "Processing Information",
  COMMITTEE_DISCUSSION: "Committee Discussion",
  ACCEPTED: "Accepted",
  CONTRACT_SENT: "Contract Sent",
  CONTRACT_SIGNED: "Contract Signed",
  ACTIVE_CLIENT: "Active Client",
  REJECTED: "Rejected",
  WAITLIST: "Waitlist",
  ARCHIVED: "Archived"
};

module.exports = {
  ClientStatuses,
  ApplicationStatuses
};
