const mongoose = require("mongoose");
const Client = require("../../src/models/client.model");
const { officerOne, officerTwo } = require("./officer.fixture");
const { applicationOne, applicationTwo } = require("./application.fixture");
const { ClientStatuses } = require("./status.fixture");

const dateOne = new Date("2021-01-31");
const dateTwo = new Date("2021-03-31");

const clientOne = {
  _id: mongoose.Types.ObjectId(),
  dateSigned: dateOne,
  status: "Active Client",
  officer: officerOne._id,
  application: applicationOne._id,
  validStatuses: Object.values(ClientStatuses)
};

const clientTwo = {
  _id: mongoose.Types.ObjectId(),
  dateSigned: dateTwo,
  status: "Archived",
  officer: officerTwo._id,
  application: applicationTwo._id,
  validStatuses: Object.values(ClientStatuses)
};

const insertClients = async (clients) => {
  await Client.insertMany(clients.map((client) => ({ ...client })));
};

module.exports = {
  dateOne,
  dateTwo,
  clientOne,
  clientTwo,
  insertClients
};
