const mongoose = require("mongoose");
const faker = require("faker");
const fs = require("fs");
const path = require("path");

const { Contract } = require("../../src/models");
const { clientOne, clientTwo } = require("./client.fixture");
const { applicationOne, applicationTwo } = require("./application.fixture");
const { userOne } = require("./user.fixture");

const dateOne = new Date("2021-01-01");
const dateTwo = new Date("2022-01-01");
const dateThree = new Date("2001-01-01");

const filePath = path.join(__dirname, "./files/Loan_Contract-123.pdf");
const activeContract = fs.readFileSync(filePath);

const contractOne = {
  _id: mongoose.Types.ObjectId(),
  approvedLoanAmount: 1500,
  monthlyPayment: 125,
  finalPayment: 0,
  firstPaymentDue: dateOne,
  finalPaymentDue: dateTwo,
  status: "Draft",
  contractStartDate: dateThree,
  client: clientOne._id,
  contractFileName: `${applicationOne.firstName}${applicationOne.lastName}-1637876311`,
  createdBy: userOne._id,
  application: applicationOne._id
};

const contractTwo = {
  _id: mongoose.Types.ObjectId(),
  approvedLoanAmount: 3000,
  monthlyPayment: 250,
  finalPayment: 0,
  firstPaymentDue: dateOne,
  finalPaymentDue: dateTwo,
  status: "Draft",
  contractStartDate: dateThree,
  client: clientTwo._id,
  contractFileName: `${applicationTwo.firstName}${applicationTwo.lastName}-1637876311`,
  createdBy: userOne._id,
  application: applicationTwo._id
};

const activeContractOne = {
  _id: mongoose.Types.ObjectId(),
  approvedLoanAmount: 1500,
  monthlyPayment: 100,
  finalPayment: 400,
  firstPaymentDue: dateOne,
  finalPaymentDue: dateTwo,
  status: "Active",
  contractStartDate: dateThree,
  client: clientOne._id,
  contractFileName: `${applicationOne.firstName}${applicationOne.lastName}-1637876311`,
  contractFileBuffer: activeContract,
  createdBy: userOne._id,
  application: applicationOne._id
};

const insertContracts = async (contracts) => {
  await Contract.insertMany(contracts);
};

const secondsInMonth = 2628000000;

/**
 * @param {Number} approvedAmount
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns {Number} - Monthly payment rounded to the nearest 5
 */
const calculateMonthlyPayment = (approvedAmount, finalPaymentTime, firstPaymentTime) => {
  // Approved amount divided by number of months (rounded to the nearest month), rounded to nearest 5
  return Math.ceil(approvedAmount / Math.round((finalPaymentTime - firstPaymentTime) / secondsInMonth) / 5) * 5;
};

/**
 * @param {Number} approvedAmount
 * @param {Number} monthlyPayment
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns {Number} - Final payment after (# of months in term - 1) monthly payments have been made
 */
const calculateFinalPayment = (approvedAmount, monthlyPayment, finalPaymentTime, firstPaymentTime) => {
  // The remaining amount to pay after all but one month has been payed off at the monthly payment rate
  // ((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth) = # of months in payment term - 1 month
  return (
    approvedAmount - monthlyPayment * Math.round((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth)
  );
};

/**
 * @param {Number} approvedAmount
 * @param {Number} monthlyPayment
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns - Updated final payment after (# of months in term - 1) monthly payments have been made
 */
const calculateNewFinalPayment = (approvedAmount, monthlyPayment, finalPaymentTime, firstPaymentTime) => {
  // approvedAmount, finalPaymentTime and firstPaymentTime are chosen based on whether or not
  // They were provided in the updateBody, otherwise, they are taken from the contract
  // monthlyPayment will always either be sent with the updateBody or calculated

  // The remaining amount to pay after all but one month has been payed off at the monthly payment rate
  // ((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth) = # of months in payment term - 1 month

  const finalPayment =
    approvedAmount - monthlyPayment * Math.round((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth);

  // There may be cases where the new final payment calculated results in the loan being paid off earlier than required. In that case, return the final
  // amount that would be paid without exceeding the loan
  return finalPayment >= 0 ? finalPayment : monthlyPayment + finalPayment;
};

module.exports = {
  dateOne,
  dateTwo,
  dateThree,
  contractOne,
  contractTwo,
  activeContractOne,
  insertContracts,
  calculateMonthlyPayment,
  calculateFinalPayment,
  calculateNewFinalPayment
};
