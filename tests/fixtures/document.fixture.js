const mongoose = require("mongoose");
const faker = require("faker");
const fs = require("fs");
const path = require("path");

const { Document } = require("../../src/models");
const { admin } = require("./user.fixture");
const { applicationOne, applicationTwo } = require("./application.fixture");

let fileBuffer;
const filePath = path.join(__dirname, "./files/validFile.pdf");
fileBuffer = fs.readFileSync(filePath);

const documentOne = {
  _id: mongoose.Types.ObjectId(),
  docType: "Other",
  originalName: "validFile.pdf",
  application: applicationOne._id,
  uploadedBy: admin._id,
  buffer: fileBuffer,
  size: fileBuffer.length,
  mimeType: "application/pdf"
};

const documentTwo = {
  _id: mongoose.Types.ObjectId(),
  docType: "Identification",
  originalName: "validFile.pdf",
  application: applicationTwo._id,
  uploadedBy: admin._id,
  buffer: fileBuffer,
  size: fileBuffer.length,
  mimeType: "application/pdf"
};

const insertDocuments = async (documents) => {
  await Document.insertMany(documents);
};

module.exports = {
  documentOne,
  documentTwo,
  insertDocuments
};
