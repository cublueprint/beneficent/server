const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const faker = require("faker");
const User = require("../../src/models/user.model");
const getRandomPhoneNumber = require("./phoneNumber.fixture");

const password = "password1";
const salt = bcrypt.genSaltSync(8);
const hashedPassword = bcrypt.hashSync(password, salt);

const userOne = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  country: "Canada",
  email: faker.internet.email().toLowerCase(),
  password,
  role: "user",
  isEmailVerified: false
};

const userTwo = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  country: "Canada",
  email: faker.internet.email().toLowerCase(),
  password,
  role: "user",
  isEmailVerified: false
};

const pendingUser = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  country: "Canada",
  email: faker.internet.email().toLowerCase(),
  password,
  role: "pendingUser",
  isEmailVerified: false
};

const officerUserOne = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  country: "Canada",
  email: faker.internet.email().toLowerCase(),
  password,
  isLoanOfficer: true,
  role: "user",
  isEmailVerified: false
};

const officerUserTwo = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  country: "Canada",
  email: faker.internet.email().toLowerCase(),
  password,
  isLoanOfficer: true,
  role: "user",
  isEmailVerified: false
};

const admin = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  country: "Canada",
  email: faker.internet.email().toLowerCase(),
  password,
  role: "admin",
  isEmailVerified: false
};

const insertUsers = async (users) => {
  await User.insertMany(users.map((user) => ({ ...user, password: hashedPassword })));
};

module.exports = {
  userOne,
  userTwo,
  officerUserOne,
  officerUserTwo,
  admin,
  pendingUser,
  insertUsers
};
