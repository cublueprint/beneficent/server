const mongoose = require("mongoose");
const faker = require("faker");
const Application = require("../../src/models/application.model");
const getRandomPhoneNumber = require("./phoneNumber.fixture");
const { admin, userOne, userTwo } = require("./user.fixture");
const { officerOne, officerTwo } = require("./officer.fixture");
const { ApplicationStatuses } = require("./status.fixture");

const date = new Date("2001-01-21");

const applicationOne = {
  _id: mongoose.Types.ObjectId(),
  firstName: userOne.firstName,
  lastName: userOne.firstName,
  email: userOne.email,
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  city: faker.address.city(),
  province: faker.address.state(),
  sex: faker.name.gender(),
  dateOfBirth: date,
  maritalStatus: "Single",
  citizenship: faker.address.country(),
  preferredLanguage: "English",
  employmentStatus: "Employed Full Time",
  loanAmountRequested: faker.datatype.number(),
  loanType: "Other",
  debtCircumstances: faker.lorem.words(),
  postalCode: "A1A1A1",
  guarantor: {
    hasGuarantor: true,
    fullName: faker.name.findName(),
    email: faker.internet.email().toLowerCase(),
    phoneNumber: getRandomPhoneNumber()
  },
  recommendationInfo: faker.lorem.words(),
  acknowledgements: {
    loanPurpose: true,
    maxLoan: true,
    repayment: true,
    residence: true,
    netPositiveIncome: true,
    guarantorConsent: true
  },
  emailOptIn: true,
  status: "Received",
  validStatuses: Object.values(ApplicationStatuses),
  officer: officerOne._id,
  user: userOne._id,
  dateApplied: new Date("2020-09-17")

};

const applicationTwo = {
  _id: mongoose.Types.ObjectId(),
  firstName: userTwo.firstName,
  lastName: userTwo.lastName,
  email: userTwo.email,
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  city: faker.address.city(),
  province: faker.address.state(),
  sex: faker.name.gender(),
  dateOfBirth: date,
  maritalStatus: "Single",
  citizenship: faker.address.country(),
  preferredLanguage: "English",
  employmentStatus: "Employed Full Time",
  loanAmountRequested: faker.datatype.number(),
  loanType: "Other",
  debtCircumstances: faker.lorem.words(),
  postalCode: "B2B2B2",
  guarantor: {
    hasGuarantor: true,
    fullName: faker.name.findName(),
    email: faker.internet.email().toLowerCase(),
    phoneNumber: getRandomPhoneNumber()
  },
  recommendationInfo: faker.lorem.words(),
  acknowledgements: {
    loanPurpose: true,
    maxLoan: true,
    repayment: true,
    residence: true,
    netPositiveIncome: true,
    guarantorConsent: true
  },
  emailOptIn: true,
  status: "Accepted",
  validStatuses: Object.values(ApplicationStatuses),
  officer: officerTwo._id,
  user: userTwo._id,
  dateApplied: new Date("2021-09-17")
};

const unmatchedNameApplication = {
  firstName: "Test",
  lastName: "Duplicate",
  email: "testduplicate@example.com",
  phoneNumber: getRandomPhoneNumber(),
  address: faker.address.streetAddress(),
  city: faker.address.city(),
  province: faker.address.state(),
  sex: faker.name.gender(),
  dateOfBirth: date,
  maritalStatus: "Single",
  citizenship: faker.address.country(),
  preferredLanguage: "English",
  employmentStatus: "Employed Full Time",
  loanAmountRequested: faker.datatype.number({
    min: 1000,
    max: 9999
  }),
  loanType: "Other",
  debtCircumstances: faker.lorem.words(),
  postalCode: "C3C3C3",
  guarantor: {
    hasGuarantor: true,
    fullName: faker.name.findName(),
    email: faker.internet.email().toLowerCase(),
    phoneNumber: getRandomPhoneNumber()
  },
  recommendationInfo: faker.lorem.words(),
  acknowledgements: {
    loanPurpose: true,
    maxLoan: true,
    repayment: true,
    residence: true,
    netPositiveIncome: true,
    guarantorConsent: true
  },
  emailOptIn: true,
  status: "Received",
  officer: admin._id,
  user: userOne._id,
  dateApplied: new Date("2021-09-17")

};

const insertApplications = async (applications) => {
  await Application.insertMany(applications);
};

const expectedApplication = (application) => {
  const expectedApp = {
    ...application,
    id: application._id.toHexString(),
    dateOfBirth: date.toISOString(),
    dateApplied: application.dateApplied.toISOString(),
    officer: application.officer.toHexString(),
    user: application.user.toHexString(),
    createdAt: expect.anything(),
    updatedAt: expect.anything()
  };
  delete expectedApp._id;
  return expectedApp;
};

module.exports = {
  date,
  applicationOne,
  applicationTwo,
  unmatchedNameApplication,
  insertApplications,
  expectedApplication
};
