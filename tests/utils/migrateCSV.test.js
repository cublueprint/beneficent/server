const fs = require("fs");
const path = require("path");
const { readCSVFile } = require("../../src/utils/migrateCSV");

const importSuccess = "../fixtures/files/application-import-files/applicationImport-success.csv";
const importWarnings = "../fixtures/files/application-import-files/applicationImport-warnings.csv";

describe("Import Applications", () => {
  describe("Test readCSVFile", () => {
    test("Should return successful applications without warnings", async () => {
      let fileBuffer;
      const filePath = path.join(__dirname, importSuccess);
      fileBuffer = fs.readFileSync(filePath);
      const { applications, warnings } = await readCSVFile(fileBuffer);

      expect(warnings.length).toBe(0);
      expect(applications.length).toBe(1);
    });

    test("Should return successful applications with warnings", async () => {
      let fileBuffer;
      const filePath = path.join(__dirname, importWarnings);
      fileBuffer = fs.readFileSync(filePath);
      const { applications, warnings } = await readCSVFile(fileBuffer);

      expect(warnings.length).toBe(1);
      expect(applications.length).toBe(3);
    });
  });
});
