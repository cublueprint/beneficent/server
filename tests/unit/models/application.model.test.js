const faker = require("faker");
const mongoose = require("mongoose");
const { Application } = require("../../../src/models");
const { date } = require("../../fixtures/application.fixture");
const getRandomPhoneNumber = require("../../fixtures/phoneNumber.fixture");

describe("Application model", () => {
  describe("Application validation", () => {
    let newApplication;
    beforeEach(() => {
      newApplication = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        phoneNumber: getRandomPhoneNumber(),
        address: faker.address.streetAddress(),
        city: faker.address.city(),
        province: faker.address.state(),
        sex: faker.name.gender(),
        dateOfBirth: date,
        maritalStatus: "Single",
        citizenship: faker.address.country(),
        preferredLanguage: "English",
        employmentStatus: "Employed Full Time",
        loanAmountRequested: faker.datatype.number({
          min: 1000,
          max: 9999
        }),
        loanType: "Other",
        debtCircumstances: faker.lorem.words(),
        postalCode: "A1A1A1",
        guarantor: {
          hasGuarantor: true,
          fullName: faker.name.findName(),
          email: faker.internet.email().toLowerCase(),
          phoneNumber: getRandomPhoneNumber()
        },
        recommendationInfo: faker.lorem.words(),
        acknowledgements: {
          loanPurpose: true,
          maxLoan: true,
          repayment: true,
          residence: true,
          netPositiveIncome: true,
          guarantorConsent: true
        },
        emailOptIn: true,
        status: "Received",
        officer: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        dateApplied: Date.now()
      };
    });

    test("should correctly validate a valid application", async () => {
      await expect(new Application(newApplication).validate()).resolves.toBeUndefined();
    });

    test("should throw a validation error if date of birth is invalid", async () => {
      newApplication.dateOfBirth = "invalidDate";
      await expect(new Application(newApplication).validate()).rejects.toThrow();
    });

    test("should throw a validation error if loan officer is invalid", async () => {
      newApplication.officer = "invalidLoanOfficer";
      await expect(new Application(newApplication).validate()).rejects.toThrow();
    });

    test("should throw a validation error if user is invalid", async () => {
      newApplication.user = "invalidUser";
      await expect(new Application(newApplication).validate()).rejects.toThrow();
    });

    test("should throw a validation error if email is invalid", async () => {
      newApplication.email = "invalidEmail";
      await expect(new Application(newApplication).validate()).rejects.toThrow();
    });

    test("should throw a validation error if phone number is invalid", async () => {
      newApplication.phoneNumber = "invalidPhoneNumber";
      await expect(new Application(newApplication).validate()).rejects.toThrow();
    });

    test("should throw a validation error if guarantor email is invalid", async () => {
      newApplication.guarantor.email = "invalidGuarantorEmail";
      await expect(new Application(newApplication).validate()).rejects.toThrow();
    });

    test("should throw a validation error if guarantor phone number is invalid", async () => {
      newApplication.guarantor.phoneNumber = "invalidGuarantorPhoneNumber";
      await expect(new Application(newApplication).validate()).rejects.toThrow();
    });
  });
});
