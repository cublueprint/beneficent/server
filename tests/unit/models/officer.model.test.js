const mongoose = require("mongoose");
const setupTestDB = require("../../utils/setupTestDB");
const { Officer } = require("../../../src/models");
const { userOne } = require("../../fixtures/user.fixture");
const { applicationOne, applicationTwo, insertApplications } = require("../../fixtures/application.fixture");
const { contractOne, contractTwo, insertContracts } = require("../../fixtures/contract.fixture");
const { officerOne, officerTwo } = require("../../fixtures/officer.fixture");
const { insertOfficers } = require("../../fixtures/officer.fixture");
const { updateApplicationById } = require("../../../src/services/application.service");

setupTestDB();

describe("Officer model", () => {
  describe("Officer validation", () => {
    let newOfficer;
    beforeEach(() => {
      newOfficer = {
        user: userOne._id
      };
    });

    test("should correctly update the workload", async () => {
      contractOne.status = "Active";
      contractTwo.status = "Active";
      await insertContracts([contractOne, contractTwo]);
      await insertOfficers([officerOne, officerTwo]);
      const officerBeforeUpdate = await Officer.findById(officerOne._id);
      expect(officerBeforeUpdate.workload).toEqual(0);

      // Add a single application to a single officer for 100% workload
      applicationOne.officer = officerOne._id;
      await insertApplications([applicationOne]);
      await mongoose.model("Officer").updateWorkload();
      const officerAfterUpdate = await Officer.findById(officerOne._id);
      await expect(officerAfterUpdate.workload).toEqual(100);

      // Add a single application to another officer for 50% workload
      applicationTwo.officer = officerTwo._id;
      await insertApplications([applicationTwo]);
      await mongoose.model("Officer").updateWorkload();
      const officerTwoAfterUpdate = await Officer.findById(officerTwo._id);
      await expect(officerTwoAfterUpdate.workload).toEqual(50);

      // Create two clients, workload should remain the same
      await updateApplicationById(applicationOne._id, { status: "Active Client" });
      await updateApplicationById(applicationTwo._id, { status: "Active Client" });
      const officerOneAfterClientCreation = await Officer.findById(officerOne._id);
      const officerTwoAfterClientCreation = await Officer.findById(officerTwo._id);
      await expect(officerOneAfterClientCreation.workload).toEqual(50);
      await expect(officerTwoAfterClientCreation.workload).toEqual(50);

      // Set a single applications status to archived to remove from workload
      await updateApplicationById(applicationTwo._id, { status: "Archived" });
      const officerOneAfterArchive = await Officer.findById(officerOne._id);
      const officerTwoAfterArchive = await Officer.findById(officerTwo._id);
      await expect(officerOneAfterArchive.workload).toEqual(100);
      await expect(officerTwoAfterArchive.workload).toEqual(0);
    });

    test("should correctly validate a valid officer", async () => {
      await expect(new Officer(newOfficer).validate()).resolves.toBeUndefined();
    });

    test("should correctly throw error on an invalid officer user", async () => {
      let newOfficer;
      await expect(new Officer(newOfficer).validate()).rejects.toThrow();
    });
  });
});
