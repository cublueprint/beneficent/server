const request = require("supertest");
const faker = require("faker");
const httpStatus = require("http-status");
const app = require("../../src/app");
const path = require("path");
const setupTestDB = require("../utils/setupTestDB");
const { Contract } = require("../../src/models");
const {
  dateOne,
  dateTwo,
  dateThree,
  contractOne,
  contractTwo,
  activeContractOne,
  insertContracts,
  calculateMonthlyPayment,
  calculateFinalPayment,
  calculateNewFinalPayment
} = require("../fixtures/contract.fixture");
const {
  userOneAccessToken,
  adminAccessToken,
  officerOneAccessToken,
  pendingUserAccessToken
} = require("../fixtures/token.fixture");
const { userOne, officerUserOne, admin, insertUsers, pendingUser } = require("../fixtures/user.fixture");
const { officerOne, insertOfficers } = require("../fixtures/officer.fixture");
const { applicationOne, insertApplications, applicationTwo } = require("../fixtures/application.fixture");
const { clientOne, insertClients } = require("../fixtures/client.fixture");

setupTestDB();

describe("CONTRACT routes", () => {
  describe("POST /api/v1/contracts", () => {
    let newContract;
    beforeEach(() => {
      newContract = {
        approvedLoanAmount: 1234,
        monthlyPayment: 100,
        finalPayment: 134,
        firstPaymentDue: dateOne,
        finalPaymentDue: dateTwo,
        status: "Draft",
        contractStartDate: dateThree,
        application: applicationOne._id,
        createdBy: contractOne.createdBy.toHexString()
      };
    });

    test("should return 201 and successfully create new contract if data is ok", async () => {
      await insertUsers([admin, userOne]);
      await insertApplications([applicationOne]);
      const res = await request(app)
        .post("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newContract);
      expect(res.body).toEqual({
        id: expect.anything(),
        approvedLoanAmount: newContract.approvedLoanAmount,
        firstPaymentDue: dateOne.toISOString(),
        finalPaymentDue: dateTwo.toISOString(),
        monthlyPayment: newContract.monthlyPayment,
        finalPayment: newContract.finalPayment,
        application: newContract.application.toHexString(),
        status: "Draft",
        contractFileName: expect.stringContaining(`${applicationOne.firstName}${applicationOne.lastName}`),
        contractStartDate: dateThree.toISOString(),
        contractCycleLength: dateOne - dateThree,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        wave: expect.anything()
      });

      const dbContract = await Contract.findById(res.body.id);
      expect(dbContract).toBeDefined();
      expect(dbContract.toJSON()).toMatchObject({
        approvedLoanAmount: newContract.approvedLoanAmount,
        firstPaymentDue: newContract.firstPaymentDue,
        finalPaymentDue: newContract.finalPaymentDue,
        monthlyPayment: newContract.monthlyPayment,
        finalPayment: newContract.finalPayment,
        application: newContract.application,
        status: "Draft",
        contractFileName: expect.stringContaining(`${applicationOne.firstName}${applicationOne.lastName}`),
        contractStartDate: newContract.contractStartDate
      });
    });

    test("should return 401 if access token is missing", async () => {
      await request(app).post("/api/v1/contracts").send(newContract).expect(httpStatus.UNAUTHORIZED);
    });
    test("should not create contract if application field is invalid", async () => {
      const badContract = { ...newContract, application: applicationTwo._id };
      await insertUsers([admin, userOne]);
      const res = await request(app)
        .post("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badContract)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({
        message: "Application with this ID does not exist"
      });
    });
    test("should not create contract if createdBy field is invalid", async () => {
      const badContract = { ...newContract, createdBy: userOne._id };
      await insertUsers([admin]);
      const res = await request(app)
        .post("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badContract)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({
        message: "User with this ID does not exist"
      });
    });
  });

  describe("GET /api/v1/contracts", () => {
    test("should return 200 and apply the default query options", async () => {
      await insertContracts([contractOne, contractTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });

      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0]).toEqual({
        id: contractOne._id.toHexString(),

        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: contractOne.monthlyPayment,
        finalPayment: contractOne.finalPayment,
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
      expect(res.body.results[1]).toEqual({
        id: contractTwo._id.toHexString(),
        approvedLoanAmount: contractTwo.approvedLoanAmount,
        firstPaymentDue: contractTwo.firstPaymentDue.toISOString(),
        finalPaymentDue: contractTwo.finalPaymentDue.toISOString(),
        monthlyPayment: contractTwo.monthlyPayment,
        finalPayment: contractTwo.finalPayment,
        client: contractTwo.client.toHexString(),
        application: contractTwo.application.toHexString(),
        status: "Draft",
        contractFileName: contractTwo.contractFileName,
        contractStartDate: contractTwo.contractStartDate.toISOString(),
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 200 and apply filter on application field", async () => {
      await insertContracts([contractOne, contractTwo]);
      await insertClients([clientOne]);
      await insertApplications([applicationOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ application: `${applicationOne._id}` })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });

      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0]).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: contractOne.monthlyPayment,
        finalPayment: contractOne.finalPayment,
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 200 and apply filter on client field", async () => {
      await insertContracts([contractOne, contractTwo]);
      await insertClients([clientOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ client: `${clientOne._id}` })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });

      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0]).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: contractOne.monthlyPayment,
        finalPayment: contractOne.finalPayment,
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 200 and apply filter on officer field", async () => {
      await insertContracts([contractOne, contractTwo]);
      await insertClients([clientOne]);
      await insertOfficers([officerOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ officer: `${officerOne._id}` })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });

      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0]).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: contractOne.monthlyPayment,
        finalPayment: contractOne.finalPayment,
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 200 and apply filter on status field", async () => {
      await insertContracts([activeContractOne]);
      await insertClients([clientOne]);
      await insertOfficers([officerOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ status: "Active" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });

      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0]).toEqual({
        id: activeContractOne._id.toHexString(),
        approvedLoanAmount: activeContractOne.approvedLoanAmount,
        firstPaymentDue: activeContractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: activeContractOne.finalPaymentDue.toISOString(),
        monthlyPayment: activeContractOne.monthlyPayment,
        finalPayment: activeContractOne.finalPayment,
        client: activeContractOne.client.toHexString(),
        application: activeContractOne.application.toHexString(),
        status: "Active",
        contractFileName: activeContractOne.contractFileName,
        contractStartDate: activeContractOne.contractStartDate.toISOString(),
        createdAt: expect.anything(),
        createdBy: activeContractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 404 if accessing contracts for officer without clients", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerOne]);
      await insertContracts([contractOne]);
      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .query({ officer: `${officerOne._id}` })
        .expect(httpStatus.NOT_FOUND);
      expect(res.body).toMatchObject({ message: "No contracts found" });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);
      await request(app).get("/api/v1/contracts").send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to access all contracts", async () => {
      await insertUsers([pendingUser]);
      await insertContracts([contractOne]);

      await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 404 if no contracts are found", async () => {
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "No contracts found" });
    });

    test("should return the correct page if page and limit params are specified", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne, contractTwo]);

      const res = await request(app)
        .get("/api/v1/contracts")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ page: 2, limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 2,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(contractTwo._id.toHexString());
    });
  });

  describe("GET /api/v1/contracts/:contractId", () => {
    test("should return 403 if user is accessing contract information", async () => {
      await insertUsers([pendingUser]);
      await insertContracts([contractOne]);
      const res = await request(app)
        .get(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 and the contract object if data is ok", async () => {
      await insertUsers([officerUserOne, admin]);
      await insertContracts([contractOne]);
      await insertOfficers([officerOne]);

      const res = await request(app)
        .get(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: contractOne.monthlyPayment,
        finalPayment: contractOne.finalPayment,
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertContracts([contractOne]);
      await request(app).get(`/api/v1/contracts/${contractOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 400 if contractID is not a valid mongo id", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);

      await request(app)
        .get("/api/v1/contracts/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if contract is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("GET /api/v1/contracts/:contractId/download", () => {
    test("should return 200 and create a contract PDF if data is ok", async () => {
      await insertUsers([officerUserOne, userOne, admin]);
      await insertApplications([applicationOne]);
      await insertOfficers([officerOne]);
      await insertContracts([contractOne]);
      await insertClients([clientOne]);

      const res = await request(app)
        .get(`/api/v1/contracts/${contractOne._id}/download`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.CREATED);
      expect(res.headers["content-disposition"]).toBe(`attachment; filename=\"${contractOne.contractFileName}\"`);
      expect(res.headers["content-type"]).toBe("application/pdf");

      const dbContract = await Contract.findById(contractOne);
      expect(dbContract.contractFileName).toBe(contractOne.contractFileName);
    });

    test("should return 200 and the active contract PDF if data is ok", async () => {
      await insertContracts([activeContractOne]);
      await insertUsers([officerUserOne, userOne, admin]);
      await insertApplications([applicationOne]);
      await insertOfficers([officerOne]);
      await insertClients([clientOne]);

      const res = await request(app)
        .get(`/api/v1/contracts/${activeContractOne._id}/download`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.CREATED);
      expect(res.headers["content-disposition"]).toBe(`attachment; filename=\"${activeContractOne.contractFileName}\"`);
      expect(res.headers["content-type"]).toBe("application/pdf");
    });

    test("should return 401 if access token is missing", async () => {
      await insertContracts([contractOne]);
      await request(app).get(`/api/v1/contracts/${contractOne._id}/download`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is trying to download a contract", async () => {
      await insertUsers([pendingUser]);
      await insertContracts([contractOne]);

      await request(app)
        .get(`/api/v1/contracts/${contractOne._id}/download`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 404 if contract is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/api/v1/contracts/${contractOne._id}/download`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("POST /api/v1/contracts/:contractId/upload", () => {
    test("should return 201 and the contract", async () => {
      await insertContracts([contractOne]);
      await insertOfficers([officerOne]);
      await insertUsers([officerUserOne, userOne]);
      await insertApplications([applicationOne]);

      const res = await request(app)
        .post(`/api/v1/contracts/${contractOne._id}/upload`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .attach("file", path.resolve(__dirname, "../fixtures/files/Loan_Contract-123.pdf"))
        .set("Connection", "keep-alive")
        .expect(httpStatus.CREATED);

      expect(res.body).toEqual({
        id: expect.anything(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: contractOne.monthlyPayment,
        finalPayment: contractOne.finalPayment,
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Active",
        contractStartDate: contractOne.contractStartDate.toISOString(),
        contractCycleLength: contractOne.firstPaymentDue - contractOne.contractStartDate,
        contractFileName: expect.anything(),
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });

      const dbOldContract = await Contract.findById(contractOne._id);
      expect(dbOldContract.status).toBe("Archived");

      const dbNewContract = await Contract.findById(res.body.id);
      expect(dbNewContract).toBeDefined();
      expect(dbNewContract.status).toBe("Active");
    });

    test("should return 400 if contract file extension is invalid", async () => {
      insertUsers([officerUserOne]);
      insertApplications([applicationOne]);

      const res = await request(app)
        .post(`/api/v1/contracts/${contractOne._id}/upload`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .attach("file", path.join(__dirname, "../fixtures/files/invalidExt.doc"))
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({
        message: "only .pdf files are allowed"
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertContracts([contractOne]);
      await insertOfficers([officerOne]);
      await insertApplications([applicationOne]);

      const res = await request(app)
        .post(`/api/v1/contracts/${contractOne._id}/upload`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .attach("file", path.resolve(__dirname, "../fixtures/files/Loan_Contract-123.pdf"))
        .set("Connection", "keep-alive")
        .expect(httpStatus.UNAUTHORIZED);

      expect(res.body).toMatchObject({
        message: "Please authenticate"
      });
    });

    test("should return 401 if a user trying to upload a contract", async () => {
      await insertUsers([pendingUser]);
      await insertContracts([contractOne]);
      const res = await request(app)
        .post(`/api/v1/contracts/${contractOne._id}/upload`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .attach("file", path.resolve(__dirname, "../fixtures/files/Loan_Contract-123.pdf"))
        .expect(httpStatus.FORBIDDEN);

      expect(res.body).toMatchObject({
        message: "Forbidden"
      });
    });

    test("should return 404 if contract is not found", async () => {
      await insertUsers([admin]);

      const res = await request(app)
        .post(`/api/v1/contracts/${contractOne._id}/upload`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .attach("file", path.resolve(__dirname, "../fixtures/files/Loan_Contract-123.pdf"))
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({
        message: "Contract not found"
      });
    });
  });

  describe("DELETE /api/v1/contracts/:contractId", () => {
    test("should return 204 if data is ok", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);

      await request(app)
        .delete(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbContract = await Contract.findById(contractOne._id);
      expect(dbContract).toBeNull();
    });

    test("should return 401 if access token is missing", async () => {
      await insertContracts([contractOne]);

      await request(app).delete(`/api/v1/contracts/${contractOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is trying to delete another contract", async () => {
      await insertUsers([pendingUser]);
      await insertContracts([contractOne]);

      await request(app)
        .delete(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 400 if contractId is not a valid mongo id", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete("/api/v1/contracts/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if contract is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("PATCH /api/v1/contracts/:contractId", () => {
    test("should return 200 and successfully update contract if data is ok with new approved loan amount", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);
      const updateBody = {
        approvedLoanAmount: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };

      const res = await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: updateBody.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: calculateMonthlyPayment(
          updateBody.approvedLoanAmount,
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        finalPayment: calculateNewFinalPayment(
          updateBody.approvedLoanAmount,
          calculateMonthlyPayment(
            updateBody.approvedLoanAmount,
            contractOne.finalPaymentDue.getTime(),
            contractOne.firstPaymentDue.getTime()
          ),
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        contractCycleLength: contractOne.firstPaymentDue - contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });

      const dbContract = await Contract.findById(contractOne._id);
      expect(dbContract).toBeDefined();
      expect(dbContract.toJSON()).toMatchObject({
        approvedLoanAmount: updateBody.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue,
        finalPaymentDue: contractOne.finalPaymentDue,
        monthlyPayment: calculateMonthlyPayment(
          updateBody.approvedLoanAmount,
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        finalPayment: calculateNewFinalPayment(
          updateBody.approvedLoanAmount,
          calculateMonthlyPayment(
            updateBody.approvedLoanAmount,
            contractOne.finalPaymentDue.getTime(),
            contractOne.firstPaymentDue.getTime()
          ),
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        client: contractOne.client,
        application: contractOne.application,
        status: "Draft",
        contractStartDate: contractOne.contractStartDate,
        contractFileName: contractOne.contractFileName,
        wave: expect.anything()
      });
    });

    test("should return 200 and successfully update contract if data is ok with new final payment date", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);

      let newDateTwo = contractOne.finalPaymentDue;
      newDateTwo.setFullYear(newDateTwo.getFullYear() + 1);

      const updateBody = {
        finalPaymentDue: newDateTwo
      };

      const res = await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: updateBody.finalPaymentDue.toISOString(),
        monthlyPayment: calculateMonthlyPayment(
          contractOne.approvedLoanAmount,
          updateBody.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        finalPayment: calculateNewFinalPayment(
          contractOne.approvedLoanAmount,
          calculateMonthlyPayment(
            contractOne.approvedLoanAmount,
            updateBody.finalPaymentDue.getTime(),
            contractOne.firstPaymentDue.getTime()
          ),
          updateBody.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        contractCycleLength: contractOne.firstPaymentDue - contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });

      const dbContract = await Contract.findById(contractOne._id);
      expect(dbContract).toBeDefined();
      expect(dbContract.toJSON()).toMatchObject({
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue,
        finalPaymentDue: updateBody.finalPaymentDue,
        monthlyPayment: calculateMonthlyPayment(
          contractOne.approvedLoanAmount,
          updateBody.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        finalPayment: calculateNewFinalPayment(
          contractOne.approvedLoanAmount,
          calculateMonthlyPayment(
            contractOne.approvedLoanAmount,
            updateBody.finalPaymentDue.getTime(),
            contractOne.firstPaymentDue.getTime()
          ),
          updateBody.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        client: contractOne.client,
        application: contractOne.application,
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy,
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 200 and successfully update contract if data is ok with new first payment date", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);

      let newDateOne = contractOne.firstPaymentDue;
      newDateOne.setFullYear(newDateOne.getFullYear() - 1);

      const updateBody = {
        firstPaymentDue: newDateOne
      };

      const res = await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: updateBody.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: calculateMonthlyPayment(
          contractOne.approvedLoanAmount,
          contractOne.finalPaymentDue.getTime(),
          updateBody.firstPaymentDue.getTime()
        ),
        finalPayment: calculateNewFinalPayment(
          contractOne.approvedLoanAmount,
          calculateMonthlyPayment(
            contractOne.approvedLoanAmount,
            contractOne.finalPaymentDue.getTime(),
            updateBody.firstPaymentDue.getTime()
          ),
          contractOne.finalPaymentDue.getTime(),
          updateBody.firstPaymentDue.getTime()
        ),
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        contractCycleLength: updateBody.firstPaymentDue - contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });

      const dbContract = await Contract.findById(contractOne._id);
      expect(dbContract).toBeDefined();
      expect(dbContract.toJSON()).toMatchObject({
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: updateBody.firstPaymentDue,
        finalPaymentDue: contractOne.finalPaymentDue,
        monthlyPayment: calculateMonthlyPayment(
          contractOne.approvedLoanAmount,
          contractOne.finalPaymentDue.getTime(),
          updateBody.firstPaymentDue.getTime()
        ),
        finalPayment: calculateNewFinalPayment(
          contractOne.approvedLoanAmount,
          calculateMonthlyPayment(
            contractOne.approvedLoanAmount,
            contractOne.finalPaymentDue.getTime(),
            updateBody.firstPaymentDue.getTime()
          ),
          contractOne.finalPaymentDue.getTime(),
          updateBody.firstPaymentDue.getTime()
        ),
        client: contractOne.client,
        application: contractOne.application,
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy,
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 200 and successfully update contract if data is ok with new monthly payment amount", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);

      const updateBody = {
        monthlyPayment:
          calculateMonthlyPayment(
            contractOne.approvedLoanAmount,
            contractOne.finalPaymentDue.getTime(),
            contractOne.firstPaymentDue.getTime()
          ) - 25
      };

      const res = await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: updateBody.monthlyPayment,
        finalPayment: calculateNewFinalPayment(
          contractOne.approvedLoanAmount,
          updateBody.monthlyPayment,
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        contractCycleLength: contractOne.firstPaymentDue - contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });

      const dbContract = await Contract.findById(contractOne._id);
      expect(dbContract).toBeDefined();
      expect(dbContract.toJSON()).toMatchObject({
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue,
        finalPaymentDue: contractOne.finalPaymentDue,
        monthlyPayment: updateBody.monthlyPayment,
        finalPayment: calculateNewFinalPayment(
          contractOne.approvedLoanAmount,
          updateBody.monthlyPayment,
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        client: contractOne.client,
        application: contractOne.application,
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate,
        contractCycleLength: contractOne.firstPaymentDue - contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy,
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 200 and successfully update contract if data is ok with new final payment amount", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);

      const updateBody = {
        finalPayment: calculateFinalPayment(
          contractOne.approvedLoanAmount,
          calculateMonthlyPayment(
            contractOne.approvedLoanAmount,
            contractOne.finalPaymentDue.getTime(),
            contractOne.firstPaymentDue.getTime()
          ),
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        )
      };

      const res = await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: contractOne._id.toHexString(),
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue.toISOString(),
        finalPaymentDue: contractOne.finalPaymentDue.toISOString(),
        monthlyPayment: calculateMonthlyPayment(
          contractOne.approvedLoanAmount,
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        finalPayment: updateBody.finalPayment,
        client: contractOne.client.toHexString(),
        application: contractOne.application.toHexString(),
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate.toISOString(),
        contractCycleLength: contractOne.firstPaymentDue - contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy.toHexString(),
        updatedAt: expect.anything(),
        wave: expect.anything()
      });

      const dbContract = await Contract.findById(contractOne._id);
      expect(dbContract).toBeDefined();
      expect(dbContract.toJSON()).toMatchObject({
        approvedLoanAmount: contractOne.approvedLoanAmount,
        firstPaymentDue: contractOne.firstPaymentDue,
        finalPaymentDue: contractOne.finalPaymentDue,
        monthlyPayment: calculateMonthlyPayment(
          contractOne.approvedLoanAmount,
          contractOne.finalPaymentDue.getTime(),
          contractOne.firstPaymentDue.getTime()
        ),
        finalPayment: updateBody.finalPayment,
        client: contractOne.client,
        application: contractOne.application,
        status: "Draft",
        contractFileName: contractOne.contractFileName,
        contractStartDate: contractOne.contractStartDate,
        createdAt: expect.anything(),
        createdBy: contractOne.createdBy,
        updatedAt: expect.anything(),
        wave: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);
      const updateBody = {
        approvedLoanAmount: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };
      await request(app).patch(`/api/v1/contracts/${contractOne._id}`).send(updateBody).expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is updating own contract information", async () => {
      await insertUsers([pendingUser]);
      await insertContracts([contractOne]);
      const updateBody = {
        finalPayment: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };
      await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 403 if user is updating another contract", async () => {
      await insertUsers([pendingUser]);
      await insertContracts([contractTwo]);
      const updateBody = {
        finalPayment: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };
      await request(app)
        .patch(`/api/v1/contracts/${contractTwo._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 and successfully update contract if admin is updating another contract", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);
      const updateBody = {
        approvedLoanAmount: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };
      await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);
    });

    test("should return 404 if admin is updating another contract that is not found", async () => {
      await insertUsers([admin]);
      const updateBody = {
        approvedLoanAmount: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };
      await request(app)
        .patch(`/api/v1/contract/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);
    });

    test("should return 400 if contractId is not a valid mongo id", async () => {
      await insertUsers([admin]);
      const updateBody = {
        finalPayment: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };
      await request(app)
        .patch(`/api/v1/contracts/invalidId`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if officer is invalid", async () => {
      await insertUsers([admin]);
      await insertContracts([contractOne]);
      const updateBody = { officer: "invalidOfficer" };
      await request(app)
        .patch(`/api/v1/contracts/${contractOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if contract is not found", async () => {
      await insertUsers([admin]);
      const updateBody = { finalPayment: 100 };

      const res = await request(app)
        .patch("/api/v1/contracts/60577adb72009a0024d2f7c2")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Contract not found" });
    });
  });
});
