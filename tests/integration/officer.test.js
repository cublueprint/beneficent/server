const request = require("supertest");
const httpStatus = require("http-status");
const app = require("../../src/app");
const setupTestDB = require("../utils/setupTestDB");
const { Officer } = require("../../src/models");
const {
  userOneAccessToken,
  pendingUserAccessToken,
  officerOneAccessToken,
  adminAccessToken
} = require("../fixtures/token.fixture");
const {
  userOne,
  userTwo,
  pendingUser,
  officerUserOne,
  officerUserTwo,
  admin,
  insertUsers
} = require("../fixtures/user.fixture");
const { officerOne, officerTwo, insertOfficers } = require("../fixtures/officer.fixture");

setupTestDB();

describe("OFFICER routes", () => {
  describe("POST /api/v1/officers", () => {
    let newOfficer;
    beforeEach(() => {
      newOfficer = {
        user: userOne._id
      };
    });

    test("should return 201 and successfully create new officer if data is ok", async () => {
      await insertUsers([admin]);
      await insertUsers([userOne]);
      const res = await request(app)
        .post("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newOfficer)
        .expect(httpStatus.CREATED);
      expect(res.body).toEqual({
        id: expect.anything(),
        user: newOfficer.user.toHexString(),
        workload: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
      const dbOfficer = await Officer.findById(res.body.id);
      expect(dbOfficer).toBeDefined();
      expect(dbOfficer.toJSON()).toMatchObject({
        user: newOfficer.user,
        workload: 0
      });
    });

    test("should return 404 if the user id does not exist", async () => {
      await insertUsers([admin]);
      const res = await request(app)
        .post("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newOfficer)
        .expect(httpStatus.NOT_FOUND);
      expect(res.body).toMatchObject({ message: "User with this user id does not exist" });
    });

    test("should return 400 if the officer already exists", async () => {
      await insertUsers([admin, userOne]);
      await insertOfficers([newOfficer]);
      const res = await request(app)
        .post("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newOfficer)
        .expect(httpStatus.BAD_REQUEST);
      expect(res.body).toMatchObject({ message: "Officer with this user ID already exists" });
    });

    test("should return 401 if access token is missing", async () => {
      await request(app).post("/api/v1/officers").send(newOfficer).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe("GET /api/v1/officers", () => {
    test("should return 200 and apply the default query options", async () => {
      await insertUsers([admin, officerUserOne]);
      await insertOfficers([officerOne]);

      const res = await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });

      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0]).toEqual({
        id: officerOne._id.toHexString(),
        user: officerOne.user.toHexString(),
        workload: 0,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerOne]);
      await request(app).get("/api/v1/officers").send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to access all officers", async () => {
      await insertUsers([pendingUser]);
      await insertOfficers([officerOne]);

      await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should correctly apply filter on user field", async () => {
      await insertUsers([officerUserOne, admin]);
      await insertOfficers([officerOne]);

      const res = await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ user: officerUserOne._id.toHexString() })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(officerOne._id.toHexString());
    });

    test("should correctly apply filter on name field", async () => {
      await insertUsers([officerUserOne, admin]);
      await insertOfficers([officerOne, officerTwo]);

      const res = await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ firstName: officerUserOne.firstName, lastName: officerUserOne.lastName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(officerOne._id.toHexString());
    });

    test("should correctly apply filter on email field", async () => {
      await insertUsers([officerUserOne, admin]);
      await insertOfficers([officerOne, officerTwo]);

      const res = await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ email: officerUserOne.email })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(officerOne._id.toHexString());
    });

    test("should return 404 if no officers are found", async () => {
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "No officers found" });
    });

    test("should limit returned array if limit param is specified", async () => {
      await insertUsers([admin, officerUserOne, officerUserTwo]);
      await insertOfficers([officerOne, officerTwo]);

      const res = await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(officerOne._id.toHexString());
    });

    test("should return the correct page if page and limit params are specified", async () => {
      await insertUsers([admin, officerUserOne, officerUserTwo]);
      await insertOfficers([officerOne, officerTwo]);

      const res = await request(app)
        .get("/api/v1/officers")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ page: 2, limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 2,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(officerTwo._id.toHexString());
    });
  });

  describe("GET /api/v1/officers/:officerId", () => {
    test("should return 200 and the officer if user is accessing own officer information", async () => {
      await insertUsers([officerUserOne]);
      await insertOfficers([officerOne]);
      const res = await request(app)
        .get(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: expect.anything(),
        user: officerOne.user.toHexString(),
        workload: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 403 if user is not accessing own officer information", async () => {
      await insertUsers([pendingUser]);
      await insertOfficers([officerOne]);
      const res = await request(app)
        .get(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 and the officer object if data is ok", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerOne]);
      const res = await request(app)
        .get(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: expect.anything(),
        user: officerOne.user.toHexString(),
        workload: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertOfficers([officerOne]);
      await request(app).get(`/api/v1/officers/${officerOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 400 if officerID is not a valid mongo id", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerOne]);

      await request(app)
        .get("/api/v1/officers/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if officer is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("DELETE /api/v1/officers/:officerId", () => {
    test("should return 204 if data is ok", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerOne]);

      await request(app)
        .delete(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbOfficer = await Officer.findById(officerOne._id);
      expect(dbOfficer).toBeNull();
    });

    test("should return 401 if access token is missing", async () => {
      await insertOfficers([officerOne]);

      await request(app).delete(`/api/v1/officers/${officerOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is trying to delete another loan officer", async () => {
      await insertUsers([userOne]);
      await insertOfficers([officerOne]);

      await request(app)
        .delete(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 400 if officerId is not a valid mongo id", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete("/api/v1/officers/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if officer is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("PATCH /api/v1/officers/:officerId", () => {
    test("should return 200 and successfully update officer if data is ok", async () => {
      await insertUsers([userTwo, admin]);
      await insertOfficers([officerOne]);
      const updateBody = {
        user: userTwo._id
      };

      const res = await request(app)
        .patch(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: officerOne._id.toHexString(),
        user: userTwo._id.toHexString(),
        workload: 0,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbOfficer = await Officer.findById(officerOne._id);
      expect(dbOfficer).toBeDefined();
      expect(dbOfficer.toJSON()).toMatchObject({
        user: userTwo._id,
        workload: 0
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([userTwo, admin]);
      await insertOfficers([officerOne]);

      const updateBody = {
        user: userTwo._id
      };

      await request(app).patch(`/api/v1/officers/${officerOne._id}`).send(updateBody).expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is updating own officer information", async () => {
      await insertUsers([userOne, userTwo]);
      await insertOfficers([officerOne]);

      const updateBody = {
        user: userTwo._id
      };

      await request(app)
        .patch(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 403 if user is updating another officer", async () => {
      await insertUsers([userOne, userTwo]);
      await insertOfficers([officerTwo]);

      const updateBody = {
        user: userOne._id
      };

      await request(app)
        .patch(`/api/v1/officers/${officerTwo._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 if the user is not found", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerTwo]);

      const updateBody = {
        user: userOne._id
      };

      const res = await request(app)
        .patch(`/api/v1/officers/${officerTwo._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body.message).toEqual("User not found");
    });

    test("should return 404 if the user is not found", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerTwo]);

      const updateBody = {
        user: userOne._id
      };

      const res = await request(app)
        .patch(`/api/v1/officers/${officerTwo._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body.message).toEqual("User not found");
    });

    test("should return 400 if officerId is not a valid mongo id", async () => {
      await insertUsers([userOne, admin]);
      const updateBody = {
        user: userOne._id
      };

      await request(app)
        .patch(`/api/v1/officers/invalidId`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if user is invalid", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerOne]);
      const updateBody = { user: "invalidUser" };

      await request(app)
        .patch(`/api/v1/clients/${officerOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if officer does not exist", async () => {
      await insertUsers([admin]);

      const updateBody = {
        user: userOne._id
      };

      const res = await request(app)
        .patch(`/api/v1/officers/${officerTwo._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Officer not found" });
    });

    test("should return 404 if user does not exist", async () => {
      await insertUsers([admin]);
      await insertOfficers([officerOne]);

      const updateBody = {
        user: userOne._id
      };

      const res = await request(app)
        .patch(`/api/v1/officers/${officerOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "User not found" });
    });
  });
});
