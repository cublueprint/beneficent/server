const request = require("supertest");
const faker = require("faker");
const httpStatus = require("http-status");
const app = require("../../src/app");
const setupTestDB = require("../utils/setupTestDB");
const { Client } = require("../../src/models");
const { dateOne, dateTwo, clientOne, clientTwo, insertClients } = require("../fixtures/client.fixture");
const { applicationOne, applicationTwo, insertApplications } = require("../fixtures/application.fixture");
const {
  userOneAccessToken,
  userTwoAccessToken,
  pendingUserAccessToken,
  adminAccessToken
} = require("../fixtures/token.fixture");
const { userOne, userTwo, pendingUser, admin, officerUserOne, insertUsers } = require("../fixtures/user.fixture");
const { officerOne, insertOfficers, officerTwo } = require("../fixtures/officer.fixture");
const { getApplicationById } = require("../../src/services/application.service");
const { ClientStatuses } = require("../fixtures/status.fixture");

const testDate = new Date("2021-02-01");
const testDate2 = new Date("2021-05-31");

setupTestDB();

describe("CLIENT routes", () => {
  describe("POST /api/v1/clients", () => {
    let newClient;
    beforeEach(() => {
      newClient = {
        dateSigned: dateOne,
        officer: officerOne._id,
        application: applicationOne._id
      };
    });

    test("should return 201 and successfully create new client if data is ok", async () => {
      await insertUsers([admin, userOne, officerUserOne]);
      await insertApplications([applicationOne]);
      await insertOfficers([officerOne]);

      const res = await request(app)
        .post("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newClient)
        .expect(httpStatus.CREATED);

      expect(res.body).toEqual({
        id: expect.anything(),
        dateSigned: dateOne.toISOString(),
        officer: newClient.officer.toHexString(),
        application: newClient.application.toHexString(),
        status: "Active Client",
        validStatuses: Object.values(ClientStatuses),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbClient = await Client.findById(res.body.id);
      expect(dbClient).toBeDefined();
      expect(dbClient.toJSON()).toMatchObject({
        dateSigned: newClient.dateSigned,
        officer: newClient.officer,
        application: newClient.application,
        validStatuses: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await request(app).post("/api/v1/clients").send(newClient).expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 400 if application ID already exists", async () => {
      await insertUsers([admin, officerUserOne]);
      await insertOfficers([officerOne]);
      await insertApplications([applicationOne]);
      await insertClients([newClient]);
      const res = await request(app)
        .post("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newClient)
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({ message: "Client with this application ID already exists" });
    });

    test("should return 404 if invalid Officer ID provided", async () => {
      await insertUsers([admin, userOne]);
      await insertApplications([applicationOne]);

      let badClient = { ...newClient, officer: userTwo._id };
      const res = await request(app)
        .post("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badClient)
        .expect(httpStatus.NOT_FOUND);
      expect(res.body).toMatchObject({ message: "Officer with this ID does not exist" });
    });

    test("should return 404 if invalid Application ID provided", async () => {
      await insertUsers([admin, userOne, officerUserOne]);
      await insertApplications([applicationOne]);
      await insertOfficers([officerOne]);

      let badClient = { ...newClient, application: applicationTwo._id };
      const res = await request(app)
        .post("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badClient)
        .expect(httpStatus.NOT_FOUND);
      expect(res.body).toMatchObject({ message: "Application with this ID does not exist" });
    });
  });

  describe("GET /api/v1/clients", () => {
    test("should return 200 and apply the default query options", async () => {
      await insertUsers([userOne, admin]);
      await insertApplications([applicationOne]);
      await insertClients([clientOne]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });

      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0]).toEqual({
        id: clientOne._id.toHexString(),
        dateSigned: dateOne.toISOString(),
        officer: clientOne.officer.toHexString(),
        application: clientOne.application.toHexString(),
        status: clientOne.status,
        validStatuses: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);
      await request(app).get("/api/v1/clients").send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to access all clients", async () => {
      await insertUsers([pendingUser]);
      await insertClients([clientOne]);

      await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should correctly apply filter on nameOrEmail field with firstName", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: userOne.firstName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        application = await getApplicationById(result.application);
        expect(application.firstName).toEqual(userOne.firstName);
      }
    });

    test("should correctly apply filter on nameOrEmail field with lastName", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertClients([clientOne, clientTwo]);
      await insertApplications([applicationOne, applicationTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: applicationOne.lastName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        application = await getApplicationById(result.application);
        expect(application.lastName).toEqual(applicationOne.lastName);
      }
    });

    test("should correctly apply filter on nameOrEmail field with email", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: userOne.email })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        application = await getApplicationById(result.application);
        expect(application.email).toEqual(userOne.email);
      }
    });

    test("should correctly apply filter on status field as Active Client", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ status: "Active Client" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        application = await getApplicationById(result.application);
        expect(application.email).toEqual(userOne.email);
      }
    });

    test("should correctly apply filter on status field as Archived", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ status: "Archived" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        application = await getApplicationById(result.application);
        expect(application.email).toEqual(userTwo.email);
      }
    });

    test("should correctly apply filter on application field", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ application: applicationOne._id.toHexString() })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results[0].application).toEqual(applicationOne._id.toHexString());
    });

    test("should correctly apply filter on officer field for only one officer", async () => {
      clientTwo.officer = officerTwo._id;
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);
      await insertOfficers([officerOne]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ officer: [`${officerOne._id}`] })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(clientOne._id.toHexString());
    });

    test("should correctly apply filter on officer field for more than one officer", async () => {
      clientTwo.officer = officerOne._id;
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);
      await insertOfficers([officerOne]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ officer: [`${admin._id}`, `${officerOne._id}`] })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(clientOne._id.toHexString());
      expect(res.body.results[1].id).toBe(clientTwo._id.toHexString());
    });
    test("should return 404 if no clients are found", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: "doesNotExist" })
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "No clients found" });
    });

    test("should limit returned array if limit param is specified", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(clientOne._id.toHexString());
    });

    test("should return the correct page if page and limit params are specified", async () => {
      await insertUsers([userOne, userTwo, admin]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertClients([clientOne, clientTwo]);

      const res = await request(app)
        .get("/api/v1/clients")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ page: 2, limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 2,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(clientTwo._id.toHexString());
    });
  });

  describe("GET /api/v1/clients/:clientId", () => {
    test("should return 200 and the client if user is accessing own client information", async () => {
      await insertUsers([admin, userTwo]);
      await insertApplications([applicationTwo]);
      await insertClients([clientTwo]);
      const res = await request(app)
        .get(`/api/v1/clients/${clientTwo._id}`)
        .set("Authorization", `Bearer ${userTwoAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: expect.anything(),
        dateSigned: dateTwo.toISOString(),
        officer: clientTwo.officer.toHexString(),
        application: clientTwo.application.toHexString(),
        status: clientTwo.status,
        validStatuses: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 403 if user is not accessing own client information", async () => {
      await insertUsers([pendingUser]);
      await insertApplications([applicationTwo]);
      await insertClients([clientTwo]);
      const res = await request(app)
        .get(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 and the client object if data is ok", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);
      const res = await request(app)
        .get(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: expect.anything(),
        dateSigned: dateOne.toISOString(),
        officer: clientOne.officer.toHexString(),
        application: clientOne.application.toHexString(),
        status: clientOne.status,
        validStatuses: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertClients([clientOne]);
      await request(app).get(`/api/v1/clients/${clientOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 400 if clientID is not a valid mongo id", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);

      await request(app)
        .get("/api/v1/clients/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if client is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("DELETE /api/v1/clients/:clientId", () => {
    test("should return 204 if data is ok", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);

      await request(app)
        .delete(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbClient = await Client.findById(clientOne._id);
      expect(dbClient).toBeNull();
    });

    test("should return 401 if access token is missing", async () => {
      await insertClients([clientOne]);

      await request(app).delete(`/api/v1/clients/${clientOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is trying to delete another client", async () => {
      await insertUsers([pendingUser]);
      await insertApplications([applicationOne]);
      await insertClients([clientOne]);

      await request(app)
        .delete(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 400 if clientId is not a valid mongo id", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete("/api/v1/clients/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if client is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("PATCH /api/v1/clients/:clientId", () => {
    test("should return 200 and successfully update client if data is ok", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);
      await insertApplications([applicationOne]);
      await insertOfficers([officerOne, officerTwo]);

      const updateBody = {
        officer: officerTwo._id,
        status: "Archived"
      };

      const res = await request(app)
        .patch(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: clientOne._id.toHexString(),
        dateSigned: dateOne.toISOString(),
        officer: officerTwo._id.toHexString(),
        application: clientOne.application.toHexString(),
        status: "Archived",
        validStatuses: Object.values(ClientStatuses),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbClient = await Client.findById(clientOne._id);
      expect(dbClient).toBeDefined();
      expect(dbClient.toJSON()).toMatchObject({
        dateSigned: clientOne.dateSigned,
        officer: updateBody.officer,
        application: clientOne.application,
        validStatuses: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);

      const updateBody = {
        officer: officerTwo._id
      };

      await request(app).patch(`/api/v1/clients/${clientOne._id}`).send(updateBody).expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is updating own client information", async () => {
      await insertUsers([pendingUser]);
      await insertApplications([applicationOne]);
      await insertClients([clientOne]);

      const updateBody = {
        officer: officerTwo._id
      };

      await request(app)
        .patch(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 403 if user is updating another client", async () => {
      await insertUsers([pendingUser]);
      await insertApplications([applicationTwo]);
      await insertClients([clientTwo]);

      const updateBody = {
        officer: officerTwo._id
      };

      await request(app)
        .patch(`/api/v1/clients/${clientTwo._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 and successfully update client if admin is updating another client", async () => {
      await insertUsers([admin]);
      await insertClients([clientTwo]);
      await insertApplications([applicationTwo]);
      await insertOfficers([officerOne, officerTwo]);

      const updateBody = {
        officer: officerTwo._id
      };

      await request(app)
        .patch(`/api/v1/clients/${clientTwo._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);
    });

    test("should return 404 if admin is updating another client that is not found", async () => {
      await insertUsers([admin]);
      const updateBody = {
        officer: officerTwo._id
      };

      await request(app)
        .patch(`/api/v1/client/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);
    });

    test("should return 400 if clientId is not a valid mongo id", async () => {
      await insertUsers([admin]);
      const updateBody = {
        officer: officerTwo._id
      };

      await request(app)
        .patch(`/api/v1/clients/invalidId`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if officer is invalid", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);
      const updateBody = { officer: "invalidofficer" };

      await request(app)
        .patch(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if status is invalid", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);
      const updateBody = { status: "invalidstatus" };

      await request(app)
        .patch(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if contract is invalid", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);
      const updateBody = { contract: "invalidcontract" };

      await request(app)
        .patch(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if client is not found", async () => {
      await insertUsers([admin]);
      const updateBody = { officer: "60577adb72009a0024d2f7c2" };

      const res = await request(app)
        .patch("/api/v1/clients/60577adb72009a0024d2f7c2")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Client not found" });
    });

    test("should return 404 if officer is not found", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);

      const updateBody = { officer: admin._id };

      const res = await request(app)
        .patch(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Officer with this ID does not exist" });
    });

    test("should return 404 if contract is not found", async () => {
      await insertUsers([admin]);
      await insertClients([clientOne]);

      const updateBody = { contract: admin._id };

      const res = await request(app)
        .patch(`/api/v1/clients/${clientOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Contract with this ID does not exist" });
    });
  });
});
