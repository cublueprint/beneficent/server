const request = require("supertest");
const faker = require("faker");
const httpStatus = require("http-status");
const app = require("../../src/app");
const path = require("path");
const setupTestDB = require("../utils/setupTestDB");
const { Application, Client } = require("../../src/models");
const getRandomPhoneNumber = require("../fixtures/phoneNumber.fixture");
const { ApplicationStatuses, ClientStatuses } = require("../fixtures/status.fixture");

const {
  date,
  applicationOne,
  applicationTwo,
  unmatchedNameApplication,
  insertApplications,
  expectedApplication
} = require("../fixtures/application.fixture");

const { userOneAccessToken, adminAccessToken, pendingUserAccessToken } = require("../fixtures/token.fixture");

const {
  userOne,
  userTwo,
  admin,
  officerUserOne,
  officerUserTwo,
  insertUsers,
  pendingUser
} = require("../fixtures/user.fixture");

const { officerOne, officerTwo, insertOfficers } = require("../fixtures/officer.fixture");
const { contractOne, insertContracts } = require("../fixtures/contract.fixture");

setupTestDB();

describe("APPLICATION routes", () => {
  describe("POST /api/v1/applications", () => {
    let newApplication;
    beforeEach(() => {
      newApplication = {
        firstName: userOne.firstName,
        lastName: userOne.lastName,
        email: userOne.email,
        phoneNumber: getRandomPhoneNumber(),
        address: faker.address.streetAddress(),
        city: faker.address.city(),
        province: faker.address.state(),
        sex: faker.name.gender(),
        dateOfBirth: date,
        maritalStatus: "Single",
        citizenship: faker.address.country(),
        preferredLanguage: "English",
        employmentStatus: "Employed Full Time",
        loanAmountRequested: faker.datatype.number({
          min: 1000,
          max: 9999
        }),
        loanType: "Other",
        debtCircumstances: faker.lorem.words(),
        postalCode: "A1A1A1",
        guarantor: {
          hasGuarantor: true,
          fullName: faker.name.findName(),
          email: faker.internet.email().toLowerCase(),
          phoneNumber: getRandomPhoneNumber()
        },
        recommendationInfo: faker.lorem.words(),
        acknowledgements: {
          loanPurpose: true,
          maxLoan: true,
          repayment: true,
          residence: true,
          netPositiveIncome: true,
          guarantorConsent: true
        },
        emailOptIn: true,
        status: "Received",
        officer: officerOne._id,
        user: userOne._id
      };
    });

    test("should return 201 and successfuly create new application with all the data", async () => {
      await insertUsers([admin, userOne, officerUserOne]);
      await insertOfficers([officerOne]);

      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newApplication)
        .expect(httpStatus.CREATED);

      expect(res.body).toEqual({
        ...newApplication,
        id: expect.anything(),
        dateOfBirth: date.toISOString(),
        officer: newApplication.officer.toHexString(),
        user: newApplication.user.toHexString(),
        validStatuses: Object.values(ApplicationStatuses),
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
        dateApplied: expect.anything()
      });

      const dbApplication = await Application.findById(res.body.id);
      expect(dbApplication).toBeDefined();
      expect(dbApplication).toMatchObject({
        ...newApplication,
        dateOfBirth: date,
        validStatuses: expect.anything(),
        dateApplied: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 201 and successfuly create new application without a user and an officer", async () => {
      await insertUsers([admin]);

      const applicationWithoutUser = Object.assign({}, newApplication);
      delete applicationWithoutUser.user;
      delete applicationWithoutUser.officer;

      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(applicationWithoutUser)
        .expect(httpStatus.CREATED);

      expect(res.body).toEqual({
        ...applicationWithoutUser,
        id: expect.anything(),
        dateOfBirth: date.toISOString(),
        validStatuses: Object.values(ApplicationStatuses),
        dateApplied: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbApplication = await Application.findById(res.body.id);
      expect(dbApplication).toBeDefined();
      expect(dbApplication).toMatchObject({
        ...applicationWithoutUser,
        dateOfBirth: date,
        validStatuses: expect.anything(),
        dateApplied: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 201 and set dateApplied if passed", async () => {
      await insertUsers([admin, userOne, officerUserOne]);
      await insertOfficers([officerOne]);
      const dateApplied = new Date("2020-04-29");
      const applicationWithDate = { ...newApplication, dateApplied: dateApplied };
      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(applicationWithDate)
        .expect(httpStatus.CREATED);

      expect(res.body).toEqual({
        ...applicationWithDate,
        id: expect.anything(),
        dateOfBirth: date.toISOString(),
        dateApplied: dateApplied.toISOString(),
        officer: newApplication.officer.toHexString(),
        user: newApplication.user.toHexString(),
        validStatuses: Object.values(ApplicationStatuses),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbApplication = await Application.findById(res.body.id);
      expect(dbApplication).toBeDefined();
      expect(dbApplication).toMatchObject({
        ...newApplication,
        dateOfBirth: date,
        dateApplied: dateApplied,
        validStatuses: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 400 bad request if the name or email on the application does not match a user", async () => {
      await insertUsers([admin, userOne]);
      await insertApplications([unmatchedNameApplication]);

      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(unmatchedNameApplication)
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({
        message: "Application name and/or email does not match with user name and/or email"
      });
    });

    test("should return 404 if User ID does not exist", async () => {
      await insertUsers([admin]);

      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newApplication)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "User with this ID does not exist" });
    });

    test("should return 404 if Application Officer ID does not exist", async () => {
      await insertUsers([admin, userOne]);

      let badApplication = { ...newApplication, officer: userTwo._id };
      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badApplication)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Officer with this ID does not exist" });
    });

    test("should return 404 there is a name/email mismatch", async () => {
      await insertUsers([admin, userOne]);

      let badApplication = { ...newApplication, lastName: userTwo.lastName };
      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badApplication)
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({
        message: "Application name and/or email does not match with user name and/or email"
      });
    });

    test("should return 400 if a new application has an invalid status", async () => {
      await insertUsers([admin]);
      let badApplication = {
        ...newApplication,
        status: "randomStatus"
      };

      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badApplication)
        .expect(httpStatus.BAD_REQUEST);
      expect(res.body).toMatchObject({
        message:
          '"status" must be one of [Received, Assigned, Contacted, Interview, Requested Information, Received Information, Processing Information, Committee Discussion, Accepted, Rejected, Waitlist, Contract Sent, Contract Signed, Active Client, Updating Client Contract, Archived]'
      });
    });

    test("should return 400 if a new application has an invalid postal code", async () => {
      await insertUsers([admin]);
      let badApplication = {
        ...newApplication,
        postalCode: "1A1A1A"
      };

      const res = await request(app)
        .post("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(badApplication)
        .expect(httpStatus.BAD_REQUEST);
      expect(res.body).toMatchObject({
        message: "Postal code must be a valid Canadian postal code"
      });
    });
  });

  describe("POST /api/v1/applications/import", () => {
    test("should return 403 if a non-admin is trying to import applications", async () => {
      await insertUsers([pendingUser]);
      const importCSV = "../fixtures/files/application-import-files/applicationImport-success.csv";

      await request(app)
        .post("/api/v1/applications/import")
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .attach("file", path.join(__dirname, importCSV))
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 400 if csv extension is invalid", async () => {
      insertUsers([admin]);
      const importFile = "../fixtures/files/invalidExt.doc";

      const res = await request(app)
        .post("/api/v1/applications/import")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .attach("file", path.join(__dirname, importFile))
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({
        message: "only .csv files are allowed"
      });
    });
  });

  describe("GET /api/v1/applications", () => {
    test("should return 200 and apply the default query options", async () => {
      await insertApplications(applicationOne);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0]).toEqual(expectedApplication(applicationOne));
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne]);
      await request(app).get("/api/v1/applications").send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to access all applications", async () => {
      await insertUsers([pendingUser]);
      await insertApplications([applicationOne]);

      await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });
    test("should return 400 if querying by an invalid status", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne, applicationTwo]);
      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ status: ["randomStatus"] })
        .send()
        .expect(httpStatus.BAD_REQUEST);
      expect(res.body).toMatchObject({
        message:
          '"status" must be one of [Received, Assigned, Contacted, Interview, Requested Information, Received Information, Processing Information, Committee Discussion, Accepted, Rejected, Waitlist, Contract Sent, Contract Signed, Active Client, Updating Client Contract, Archived, , array]'
      });
    });

    test("should correctly apply filter on nameOrEmail field with first name", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: applicationOne.firstName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });

      for (const result of res.body.results) {
        expect(result.firstName).toEqual(applicationOne.firstName);
      }
    });

    test("should correctly apply filter on nameOrEmail field with last name", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: applicationOne.lastName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        expect(result.lastName).toEqual(applicationOne.lastName);
      }
    });

    test("should correctly apply filter on nameOrEmail field with email", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: applicationOne.email })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        expect(result.email).toEqual(applicationOne.email);
      }
    });

    test("should correctly apply filter on officer field for only one officer", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertOfficers([officerOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ officer: [`${officerOne._id}`] })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(applicationOne._id.toHexString());
    });

    test("should correctly apply filter on officer field for more than one officer", async () => {
      applicationTwo.officer = officerOne._id;
      await insertApplications([applicationOne, applicationTwo]);
      await insertOfficers([officerOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ officer: [`${admin._id}`, `${officerOne._id}`] })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(applicationOne._id.toHexString());
      expect(res.body.results[1].id).toBe(applicationTwo._id.toHexString());
    });

    test("should correctly apply filter on status field for one status", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne, applicationTwo]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ status: [applicationOne.status] })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      for (const result of res.body.results) {
        expect(result.status).toEqual(applicationOne.status);
      }

      applicationTwo.officer = admin._id;
    });

    test("should correctly apply filter on status field for more than one status", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne, applicationTwo]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ status: [applicationOne.status, applicationTwo.status] })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(applicationOne._id.toHexString());
      expect(res.body.results[1].id).toBe(applicationTwo._id.toHexString());
    });

    test("should correctly apply filter on dateApplied field for startDate", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne, applicationTwo]);
      const startDate = applicationOne.dateApplied;
      startDate.setDate(date.getDate() + 2);
      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ startDate: startDate.toDateString() })
        .send()
        .expect(httpStatus.OK);
      for (const result of res.body.results) {
        expect(new Date(result.dateApplied).getTime() > startDate.getTime()).toBeTruthy();
      }

      applicationTwo.officer = admin._id;
    });

    test("should correctly apply filter on dateApplied field for endDate", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne, applicationTwo]);
      const endDate = applicationTwo.dateApplied;
      endDate.setDate(date.getDate() - 10);
      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ endDate: endDate.toDateString() })
        .send()
        .expect(httpStatus.OK);
      for (const result of res.body.results) {
        expect(new Date(result.dateApplied).getTime() < endDate.getTime()).toBeTruthy();
      }

      applicationTwo.officer = admin._id;
    });

    test("should correctly apply filter on dateApplied field for startDate and endDate", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne, applicationTwo]);
      const endDate = applicationTwo.dateApplied;
      endDate.setDate(date.getDate() - 10);
      const startDate = applicationOne.dateApplied;
      startDate.setDate(date.getDate());
      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ startDate: startDate.toDateString(), endDate: endDate.toDateString() })
        .send()
        .expect(httpStatus.OK);

      for (const result of res.body.results) {
        expect(new Date(result.dateApplied).getTime() < endDate.getTime()).toBeTruthy();
        expect(new Date(result.dateApplied).getTime() > startDate.getTime()).toBeTruthy();
      }
    });
    test("should correctly apply filter when displayAll is true", async () => {
      applicationOne.status = "Active Client";
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin, userOne]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ displayAll: true })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);
    });

    test("should correctly apply filter when displayAll is false", async () => {
      applicationOne.status = "Archived";
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin, userOne]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ displayAll: false })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(applicationTwo._id.toHexString());
      for (const result of res.body.results) {
        expect(result.status).not.toEqual("Archived");
      }
      applicationOne.status = "Received";
    });

    test("should correctly apply filter on user field with user id", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin, userOne]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ user: `${userOne._id}` })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(applicationOne._id.toHexString());
    });

    test("should return 404 if no applications are found", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: "notAValidSearchName" })
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "No applications found" });
    });

    test("should correctly sort the returned array if ascending sort param is specified", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ sortBy: "role:asc" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(applicationOne._id.toHexString());
      expect(res.body.results[1].id).toBe(applicationTwo._id.toHexString());
    });

    test("should correctly sort the returned array if multiple sorting criteria are specified", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ sortBy: "name:desc,status:asc" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);

      const expectedOrder = [applicationOne, applicationTwo].sort((a, b) => {
        if (a.name < b.name) {
          return 1;
        }
        if (a.name > b.name) {
          return -1;
        }
        return a.status < b.status ? -1 : 1;
      });

      expect(res.body.results[0].id).toBe(expectedOrder[0]._id.toHexString());
      expect(res.body.results[1].id).toBe(expectedOrder[1]._id.toHexString());
    });

    test("should correctly limit returned array if limit param is specified", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(applicationOne._id.toHexString());
    });

    test("should return the correct page if page and limit params are specified", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/applications")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ page: 2, limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 2,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(applicationTwo._id.toHexString());
    });
  });

  describe("GET /api/v1/applications/:applicationId", () => {
    test("should return 200 and the application object if user is trying to get their own application", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([userOne]);

      const res = await request(app)
        .get(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual(expectedApplication(applicationOne));
    });

    test("should return 200 and the application object if admin is trying to get another application", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne]);

      await request(app)
        .get(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);
    });

    test("should return 400 if clientID is not a valid mongo id", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin]);

      await request(app)
        .get("/api/v1/applications/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 401 error if access token is missing", async () => {
      await request(app).get(`/api/v1/applications/${applicationOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is trying to get another application", async () => {
      await insertUsers([pendingUser]);
      await insertApplications([applicationTwo]);

      await request(app)
        .get(`/api/v1/applications/${applicationTwo._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 404 if application is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("DELETE /api/v1/applications/:applicationId", () => {
    test("should return 204 if and successfully delete application if data is ok", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin]);

      await request(app)
        .delete(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbApplication = await Application.findById(admin._id);
      expect(dbApplication).toBeNull();
    });

    test("should return 400 if applicationId is not a valid mongo id", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete("/api/v1/applications/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 401 if access token is missing", async () => {
      await insertApplications([applicationOne]);
      await request(app).delete(`/api/v1/applications/${applicationOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 404 if application is not found", async () => {
      await insertUsers([admin]);

      const res = await request(app)
        .delete(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Application not found" });
    });
  });

  describe("PATCH /api/v1/applications/:applicationId", () => {
    test("should return 200 and successfully update application if data is ok", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin]);

      const updateBody = {
        loanAmountRequested: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        ...expectedApplication(applicationOne),
        loanAmountRequested: updateBody.loanAmountRequested
      });

      const dbApplication = await Application.findById(applicationOne._id);
      expect(dbApplication).toBeDefined();

      let expectedApp = {
        ...expectedApplication(applicationOne),
        dateOfBirth: date,
        dateApplied: applicationOne.dateApplied,
        loanAmountRequested: updateBody.loanAmountRequested,
        officer: applicationOne.officer,
        user: applicationOne.user
      };

      delete expectedApp.id;

      expect(dbApplication.toJSON()).toMatchObject(expectedApp);
    });

    test("should return 200 and successfully update application user", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin, userTwo]);

      const updateBody = { user: userTwo._id };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        ...expectedApplication(applicationOne),
        user: userTwo._id.toHexString()
      });
    });

    test("should return 200 and successfully update application officer", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin, officerUserTwo]);
      await insertOfficers([officerTwo]);

      const updateBody = { officer: officerTwo._id };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        ...expectedApplication(applicationOne),
        officer: officerTwo._id.toHexString()
      });
    });

    test("should return 200 and successfully update application status", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin, officerUserTwo]);
      await insertOfficers([officerTwo]);

      const updateBody = { status: "Accepted" };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        ...expectedApplication(applicationOne),
        status: "Accepted"
      });
    });

    test("should return 200 and successfully create client when application status is updated to Active Client", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin]);
      await insertOfficers([officerOne]);
      contractOne.status = "Active";
      await insertContracts([contractOne]);

      const updateBody = { status: "Active Client" };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expectedApp = expectedApplication(applicationOne);
      expectedApp.validStatuses = Object.values(ClientStatuses);
      expect(res.body).toEqual({
        ...expectedApp,
        status: "Active Client"
      });

      const dbClient = await Client.findOne({ application: applicationOne._id });
      expect(dbClient).toBeDefined();
      expect(dbClient).toMatchObject({
        dateSigned: expect.anything(),
        officer: applicationOne.officer,
        application: applicationOne._id,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 400 if applicationId is not a valid mongo id", async () => {
      await insertUsers([admin]);

      const updateBody = {
        loanAmountRequested: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };

      await request(app)
        .patch(`/api/v1/applications/invalidId`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if officer is invalid", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne]);

      const updateBody = { officer: "invalidofficer" };

      await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if user is invalid", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne]);

      const updateBody = { user: "60577adb72009a0024d2f7c2" };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "User not found" });
    });

    test("should return 404 if officer is invalid", async () => {
      await insertUsers([admin]);
      await insertApplications([applicationOne]);

      const updateBody = { officer: "60577adb72009a0024d2f7c2" };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Officer not found" });
    });

    test("should return 404 if application is not found", async () => {
      await insertUsers([admin]);
      const updateBody = { officer: "60577adb72009a0024d2f7c2" };

      const res = await request(app)
        .patch("/api/v1/applications/60577adb72009a0024d2f7c2")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "Application not found" });
    });

    test("should return 401 if access token is missing", async () => {
      await insertApplications([applicationOne]);
      await insertUsers([admin]);

      const updateBody = {
        loanAmountRequested: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };

      await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .send(updateBody)
        .expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is updating another application", async () => {
      await insertUsers([pendingUser]);
      await insertApplications([applicationTwo]);

      const updateBody = {
        loanAmount: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };

      await request(app)
        .patch(`/api/v1/applications/${applicationTwo._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 404 if admin is updating an application that is not found", async () => {
      await insertUsers([admin]);

      const updateBody = {
        loanAmountRequested: faker.datatype.number({
          min: 1000,
          max: 9999
        })
      };

      await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);
    });

    test("should return 400 if changing the status of an application with no loan officer", async () => {
      await insertUsers([admin]);
      delete applicationOne.officer;
      await insertApplications([applicationOne]);
      const updateBody = {
        status: "Waitlist"
      };

      const res = await request(app)
        .patch(`/api/v1/applications/${applicationOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({ message: "Cannot update a status with no assigned officer" });
    });
  });
});
