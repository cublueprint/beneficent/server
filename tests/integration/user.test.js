const request = require("supertest");
const faker = require("faker");
const httpStatus = require("http-status");
const app = require("../../src/app");
const setupTestDB = require("../utils/setupTestDB");
const { User, Officer, Application, Client } = require("../../src/models");
const { userOne, userTwo, pendingUser, admin, insertUsers, officerUserOne } = require("../fixtures/user.fixture");
const { userOneAccessToken, pendingUserAccessToken, adminAccessToken } = require("../fixtures/token.fixture");
const getRandomPhoneNumber = require("../fixtures/phoneNumber.fixture");
const { insertOfficers, officerOne } = require("../fixtures/officer.fixture");
const { insertApplications, applicationOne } = require("../fixtures/application.fixture");
const { clientOne, insertClients } = require("../fixtures/client.fixture");

setupTestDB();

describe("User routes", () => {
  describe("POST /api/v1/users", () => {
    let newUser;

    beforeEach(() => {
      newUser = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        phoneNumber: getRandomPhoneNumber(),
        address: faker.address.streetAddress(),
        country: "Canada",
        email: faker.internet.email().toLowerCase(),
        password: "password1",
        role: "user",
        isLoanOfficer: false
      };
    });

    test("should return 201 and successfully create new user if data is ok", async () => {
      await insertUsers([admin]);

      const res = await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.CREATED);

      expect(res.body).not.toHaveProperty("password");
      expect(res.body).toEqual({
        id: expect.anything(),
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        phoneNumber: newUser.phoneNumber,
        address: newUser.address,
        country: "Canada",
        email: newUser.email,
        role: newUser.role,
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbUser = await User.findById(res.body.id);
      expect(dbUser).toBeDefined();
      expect(dbUser.password).not.toBe(newUser.password);
      expect(dbUser.toJSON()).toMatchObject({
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        phoneNumber: newUser.phoneNumber,
        address: newUser.address,
        country: "Canada",
        email: newUser.email,
        role: newUser.role,
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should be able to create an admin as well", async () => {
      await insertUsers([admin]);
      newUser.role = "admin";

      const res = await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.CREATED);

      expect(res.body.role).toBe("admin");

      const dbUser = await User.findById(res.body.id);
      expect(dbUser.role).toBe("admin");
    });

    test("should return 401 if access token is missing", async () => {
      await request(app).post("/api/v1/users").send(newUser).expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if logged in user is not admin", async () => {
      await insertUsers([pendingUser]);

      await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send(newUser)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 400 if email is invalid", async () => {
      await insertUsers([admin]);
      newUser.email = "invalidEmail";

      await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if email is already used", async () => {
      await insertUsers([admin, userOne]);
      newUser.email = userOne.email;

      await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if password length is less than 8 characters", async () => {
      await insertUsers([admin]);
      newUser.password = "passwo1";

      await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if password does not contain both letters and numbers", async () => {
      await insertUsers([admin]);
      newUser.password = "password";

      await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.BAD_REQUEST);

      newUser.password = "1111111";

      await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if role is neither user nor admin", async () => {
      await insertUsers([admin]);
      newUser.role = "invalid";

      await request(app)
        .post("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.BAD_REQUEST);
    });
  });

  describe("GET /api/v1/users", () => {
    test("should return 200 and apply the default query options", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 3
      });
      expect(res.body.results).toHaveLength(3);
      expect(res.body.results[0]).toEqual({
        id: userOne._id.toHexString(),
        firstName: userOne.firstName,
        lastName: userOne.lastName,
        phoneNumber: userOne.phoneNumber,
        address: userOne.address,
        country: "Canada",
        email: userOne.email,
        role: userOne.role,
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([userOne, userTwo, admin]);

      await request(app).get("/api/v1/users").send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to access all users", async () => {
      await insertUsers([pendingUser, userTwo, admin]);

      await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should correctly apply filter on phoneNumber field", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({
          phoneNumber: userOne.phoneNumber
        })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    });

    test("should correctly apply filter on address field", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({
          address: userOne.address
        })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    });

    test("should correctly apply filter on country field", async () => {
      userTwo.country = "Not a Country";
      admin.country = "Not a Country";

      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({
          country: userOne.country
        })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    });

    test("should correctly apply filter on nameOrEmail field with first name", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: userOne.firstName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    });

    test("should correctly apply filter on nameOrEmail field with last name", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: userOne.lastName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    });

    test("should correctly apply filter on nameOrEmail field with both first and last name", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: userOne.firstName + " " + userOne.lastName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    });

    test("should correctly apply filter on nameOrEmail field with email", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: userOne.email })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    });

    test("should correctly apply filter on role field", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ role: "user" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
      expect(res.body.results[1].id).toBe(userTwo._id.toHexString());
    });

    test("should return 404 if no users are found", async () => {
      await insertUsers([admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ nameOrEmail: "invalidemail@example.com" })
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "No users found" });
    });

    test("should correctly sort the returned array if descending sort param is specified", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ sortBy: "role:desc" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 3
      });
      expect(res.body.results).toHaveLength(3);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
      expect(res.body.results[1].id).toBe(userTwo._id.toHexString());
      expect(res.body.results[2].id).toBe(admin._id.toHexString());
    });

    test("should correctly sort the returned array if ascending sort param is specified", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ sortBy: "role:asc" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 3
      });
      expect(res.body.results).toHaveLength(3);
      expect(res.body.results[0].id).toBe(admin._id.toHexString());
      expect(res.body.results[1].id).toBe(userOne._id.toHexString());
      expect(res.body.results[2].id).toBe(userTwo._id.toHexString());
    });

    test("should correctly sort the returned array if multiple sorting criteria are specified", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ sortBy: "role:desc,firstName:asc" })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 3
      });
      expect(res.body.results).toHaveLength(3);

      const expectedOrder = [userOne, userTwo, admin].sort((a, b) => {
        if (a.role < b.role) {
          return 1;
        }
        if (a.role > b.role) {
          return -1;
        }
        return a.firstName < b.firstName ? -1 : 1;
      });

      expectedOrder.forEach((user, index) => {
        expect(res.body.results[index].id).toBe(user._id.toHexString());
      });
    });

    test("should limit returned array if limit param is specified", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ limit: 2 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 2,
        totalPages: 2,
        totalResults: 3
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(userOne._id.toHexString());
      expect(res.body.results[1].id).toBe(userTwo._id.toHexString());
    });

    test("should return the correct page if page and limit params are specified", async () => {
      await insertUsers([userOne, userTwo, admin]);

      const res = await request(app)
        .get("/api/v1/users")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .query({ page: 2, limit: 2 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 2,
        limit: 2,
        totalPages: 2,
        totalResults: 3
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(admin._id.toHexString());
    });
  });

  describe("GET /api/v1/users/:userId", () => {
    test("should return 200 and the user object if data is ok", async () => {
      await insertUsers([userOne]);

      const res = await request(app)
        .get(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty("password");
      expect(res.body).toEqual({
        id: userOne._id.toHexString(),
        firstName: userOne.firstName,
        lastName: userOne.lastName,
        phoneNumber: userOne.phoneNumber,
        address: userOne.address,
        country: "Canada",
        email: userOne.email,
        role: userOne.role,
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([userOne]);

      await request(app).get(`/api/v1/users/${userOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is trying to get another user", async () => {
      await insertUsers([pendingUser, userTwo]);

      await request(app)
        .get(`/api/v1/users/${userTwo._id}`)
        .set("Authorization", `Bearer ${pendingUserAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 and the user object if admin is trying to get another user", async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .get(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);
    });

    test("should return 400 if userId is not a valid mongo id", async () => {
      await insertUsers([admin]);

      await request(app)
        .get("/api/v1/users/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if user is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("DELETE /api/v1/users/:userId", () => {
    test("should return 204 if data is ok", async () => {
      await insertUsers([admin]);
      await insertUsers([userOne]);

      await request(app)
        .delete(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbUser = await User.findById(userOne._id);
      expect(dbUser).toBeNull();
    });

    test("should cascade delete loanOfficer information", async () => {
      await insertUsers([officerUserOne, admin]);
      await insertOfficers([officerOne]);
      await insertApplications([{ ...applicationOne, officer: officerOne }]);
      await insertClients([{ ...clientOne, officer: officerOne }]);

      const updateBody = { isLoanOfficer: false };

      await request(app)
        .delete(`/api/v1/users/${officerUserOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NO_CONTENT);

      const dbOfficers = await Officer.find({ user: officerUserOne._id });
      expect(dbOfficers.length).toBe(0);

      //should unassign officer in their applications + clients

      const dbApplication = await Application.findById(applicationOne._id);
      expect(dbApplication.officer).toBe(undefined);

      const dbClient = await Client.findById(clientOne._id);
      expect(dbClient.officer).toBe(undefined);
    });
    test("should return 401 if access token is missing", async () => {
      await insertUsers([userOne]);

      await request(app).delete(`/api/v1/users/${userOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is trying to delete self", async () => {
      await insertUsers([userOne]);

      await request(app)
        .delete(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 403 if user is trying to delete another user", async () => {
      await insertUsers([userOne, userTwo]);

      await request(app)
        .delete(`/api/v1/users/${userTwo._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 204 if admin is trying to delete another user", async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .delete(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);
    });

    test("should return 400 if userId is not a valid mongo id", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete("/api/v1/users/invalidId")
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if user already is not found", async () => {
      await insertUsers([admin]);

      await request(app)
        .delete(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("PATCH /api/v1/users/:userId", () => {
    test("should return 200 and successfully update user if data is ok", async () => {
      await insertUsers([userOne]);
      const updateBody = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        phoneNumber: getRandomPhoneNumber(),
        address: faker.address.streetAddress(),
        country: "Canada",
        password: "newPassword1",
        currentPassword: "password1"
      };

      const res = await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty("password");
      expect(res.body).toEqual({
        id: userOne._id.toHexString(),
        firstName: updateBody.firstName,
        lastName: updateBody.lastName,
        phoneNumber: updateBody.phoneNumber,
        address: updateBody.address,
        country: "Canada",
        email: expect.anything(),
        role: "user",
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbUser = await User.findById(userOne._id);
      expect(dbUser).toBeDefined();
      expect(dbUser.password).not.toBe(updateBody.password);
      expect(dbUser.toJSON()).toMatchObject({
        firstName: updateBody.firstName,
        lastName: updateBody.lastName,
        phoneNumber: updateBody.phoneNumber,
        address: updateBody.address,
        country: "Canada",
        email: expect.anything(),
        role: "user",
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 200 and revert user role to pending if email was changed", async () => {
      await insertUsers([userOne]);
      const updateBody = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        phoneNumber: getRandomPhoneNumber(),
        address: faker.address.streetAddress(),
        email: faker.internet.email().toLowerCase(),
        country: "Canada",
        password: "newPassword1",
        currentPassword: "password1"
      };

      const res = await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty("password");
      expect(res.body).toEqual({
        id: userOne._id.toHexString(),
        firstName: updateBody.firstName,
        lastName: updateBody.lastName,
        phoneNumber: updateBody.phoneNumber,
        address: updateBody.address,
        country: "Canada",
        email: updateBody.email,
        role: "pendingUser",
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbUser = await User.findById(userOne._id);
      expect(dbUser).toBeDefined();
      expect(dbUser.password).not.toBe(updateBody.password);
      expect(dbUser.toJSON()).toMatchObject({
        firstName: updateBody.firstName,
        lastName: updateBody.lastName,
        phoneNumber: updateBody.phoneNumber,
        address: updateBody.address,
        country: "Canada",
        email: updateBody.email,
        role: "pendingUser",
        isEmailVerified: false,
        hasTempPassword: false,
        isLoanOfficer: false,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertUsers([userOne]);
      const updateBody = { firstName: faker.name.firstName() };

      await request(app).patch(`/api/v1/users/${userOne._id}`).send(updateBody).expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if user is updating another user", async () => {
      await insertUsers([userOne, userTwo]);
      const updateBody = { firstName: faker.name.firstName() };

      await request(app)
        .patch(`/api/v1/users/${userTwo._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 200 and successfully update user if admin is updating another user", async () => {
      await insertUsers([userOne, admin]);
      const updateBody = { firstName: faker.name.firstName() };

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);
    });

    test("should return 404 if admin is updating another user that is not found", async () => {
      await insertUsers([admin]);
      const updateBody = { firstName: faker.name.firstName() };

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);
    });

    test("should return 400 if userId is not a valid mongo id", async () => {
      await insertUsers([admin]);
      const updateBody = { firstName: faker.name.firstName() };

      await request(app)
        .patch(`/api/v1/users/invalidId`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if email is invalid", async () => {
      await insertUsers([userOne]);
      const updateBody = { email: "invalidEmail" };

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if email is already taken", async () => {
      await insertUsers([userOne, userTwo]);
      const updateBody = { email: userTwo.email };

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should not return 400 if email is my email", async () => {
      await insertUsers([userOne]);
      const updateBody = { email: userOne.email };

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);
    });

    test("should return 400 if password length is less than 8 characters", async () => {
      await insertUsers([userOne]);
      const updateBody = { password: "passwo1" };

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 400 if password does not contain both letters and numbers", async () => {
      await insertUsers([userOne]);
      const updateBody = { password: "password" };

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);

      updateBody.password = "11111111";

      await request(app)
        .patch(`/api/v1/users/${userOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should create a loanOfficer entity isLoanOfficer is set to true", async () => {
      await insertUsers([{ ...officerUserOne, isLoanOfficer: false }, admin]);
      const updateBody = { isLoanOfficer: true };

      await request(app)
        .patch(`/api/v1/users/${officerUserOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      const dbOfficers = await Officer.find({ user: officerUserOne._id });
      expect(dbOfficers.length).toBe(1);
    });

    test("should not create a loanOfficer entity isLoanOfficer is set to true, but it is already true", async () => {
      await insertUsers([officerUserOne, admin]);
      const updateBody = { isLoanOfficer: true };

      await request(app)
        .patch(`/api/v1/users/${officerUserOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      const dbOfficers = await Officer.find({ user: officerUserOne._id });

      //realistically should be one, but loan officer is not created since user is inserted as db insert
      expect(dbOfficers.length).toBe(0);
    });

    test("should delete loanOfficer entity isLoanOfficer is set to false", async () => {
      await insertUsers([officerUserOne, admin]);
      await insertOfficers([officerOne]);
      await insertApplications([{ ...applicationOne, officer: officerOne }]);
      await insertClients([{ ...clientOne, officer: officerOne }]);

      const updateBody = { isLoanOfficer: false };

      await request(app)
        .patch(`/api/v1/users/${officerUserOne._id}`)
        .set("Authorization", `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      const dbOfficers = await Officer.find({ user: officerUserOne._id });
      expect(dbOfficers.length).toBe(0);

      //should unassign officer in their applications + clients

      const dbApplication = await Application.findById(applicationOne._id);
      expect(dbApplication.officer).toBe(undefined);

      const dbClient = await Client.findById(clientOne._id);
      expect(dbClient.officer).toBe(undefined);
    });
  });
});
