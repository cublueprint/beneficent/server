module.exports = {
  testEnvironment: "node",
  testEnvironmentOptions: {
    NODE_ENV: "test"
  },
  restoreMocks: true,
  coveragePathIgnorePatterns: [".yarn", "src/config", "src/app.js", "tests"],
  coverageReporters: ["text", "lcov", "clover", "html", "cobertura"],
  reporters: ["default", ["jest-junit", { outputName: "coverage/junit.xml" }]]
};
